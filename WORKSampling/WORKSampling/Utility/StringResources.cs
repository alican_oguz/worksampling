﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using System.Security.Cryptography;
using System.Text;

namespace _2DMap.Utility
{
    public class StringResources
    {
        public static Int16 RES_LABEL = 1;
        public static Int16 RES_MSG = 2;
        public static Int16 RES_ERR = 3;
        public static Int16 RES_PAGE_TITLE = 4;
        public static Int16 RES_PAGE_HEADER = 5;
        public static Int16 RES_MENU_ITEMS = 6;
        public static Int16 RES_LIST_ITEMS = 7;

        public static Int16 LIST_LIFT_TYPE = 1;
        public static Int16 LIST_LIFT_STATUS = 2;
        public static Int16 LIST_SLOPE_TYPE = 3;
        public static Int16 LIST_SLOPE_STATUS = 4;
        public static Int16 LIST_FUN_TYPE = 5;
        public static Int16 LIST_FUN_STATUS = 6;
        public static Int16 LIST_INF_TYPE = 7;
        public static Int16 LIST_INF_STATUS = 8;

        private const string AllowedChars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz#@$!";

        public static string getString(Int16 resType, string key, string lang)
        {
            String ret = "!" + key + "_" + lang + "!";

            if (resType == RES_LABEL)
            {
                try
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
                    if (HttpContext.GetGlobalResourceObject("Labels", key, culture) != null)
                        ret = (String)HttpContext.GetGlobalResourceObject("Labels", key, culture);
                }
                catch (Exception ex)
                {
                    return ret;
                }

            }
            else if (resType == RES_MSG)
            {
                try
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
                    if (HttpContext.GetGlobalResourceObject("Messages", key, culture) != null)
                        ret = (String)HttpContext.GetGlobalResourceObject("Messages", key, culture);
                }
                catch (Exception ex)
                {
                    return ret;
                }
            }
            else if (resType == RES_ERR)
            {
                try
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
                    if (HttpContext.GetGlobalResourceObject("Errors", key, culture) != null)
                        ret = (String)HttpContext.GetGlobalResourceObject("Errors", key, culture);
                }
                catch (Exception ex)
                {
                    return ret;
                }
            }
            else if (resType == RES_PAGE_TITLE)
            {
                try
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
                    if (HttpContext.GetGlobalResourceObject("PageTitles", key, culture) != null)
                        ret = (String)HttpContext.GetGlobalResourceObject("PageTitles", key, culture);
                }
                catch (Exception ex)
                {
                    return ret;
                }
            }
            else if (resType == RES_PAGE_HEADER)
            {
                try
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
                    if (HttpContext.GetGlobalResourceObject("PageHeaders", key, culture) != null)
                        ret = (String)HttpContext.GetGlobalResourceObject("PageHeaders", key, culture);
                }
                catch (Exception ex)
                {
                    return ret;
                }
            }
            else if (resType == RES_MENU_ITEMS)
            {
                try
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
                    if (HttpContext.GetGlobalResourceObject("MenuItems", key, culture) != null)
                        ret = (String)HttpContext.GetGlobalResourceObject("MenuItems", key, culture);
                }
                catch (Exception ex)
                {
                    return ret;
                }
            }
            else if (resType == RES_LIST_ITEMS)
            {
                try
                {
                    CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
                    if (HttpContext.GetGlobalResourceObject("ListItems", key, culture) != null)
                        ret = (String)HttpContext.GetGlobalResourceObject("ListItems", key, culture);
                }
                catch (Exception ex)
                {
                    return ret;
                }
            }
            return ret;
        }

        public static string encodeString(String rawString)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(rawString);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        public static string decodeString(String encodedString)
        {
            var base64EncodedBytes = System.Convert.FromBase64String(encodedString);
            return System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
        }

        public static IEnumerable<string> RandomStrings(int minLength, int maxLength, int count, Random rng)
        {
            char[] chars = new char[maxLength];
            int setLength = AllowedChars.Length;

            while (count-- > 0)
            {
                int length = rng.Next(minLength, maxLength + 1);

                for (int i = 0; i < length; ++i)
                {
                    chars[i] = AllowedChars[rng.Next(setLength)];
                }

                yield return new string(chars, 0, length);
            }
        }

        public static String MD5Hash(String input)
        {
            MD5 md5 = System.Security.Cryptography.MD5.Create();
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public static Int64 GetLongCurrentDateTime()
        {
            DateTime dtNow = DateTime.Now;
            Int64 lDtNow = -1;
            String strDtNow = dtNow.ToString("yyyyMMddHHmmss");
            Int64.TryParse(strDtNow, out lDtNow);
            return lDtNow;
        }

        public static List<KeyValuePair<String, String>> GetStaticList(Int16 listType, String lang)
        {
            List<KeyValuePair<String, String>> retKV = new List<KeyValuePair<string, string>>();
            String dictKeyPrefix = "";

            if (listType == LIST_LIFT_TYPE)
                dictKeyPrefix = "LIFT_TYPE_";
            else if (listType == LIST_LIFT_STATUS)
                dictKeyPrefix = "LIFT_STATUS_";
            else if (listType == LIST_SLOPE_TYPE)
                dictKeyPrefix = "SLOPE_TYPE_";
            else if (listType == LIST_SLOPE_STATUS)
                dictKeyPrefix = "SLOPE_STATUS_";
            else if (listType == LIST_FUN_TYPE)
                dictKeyPrefix = "FUN_SPORTS_TYPE_";
            else if (listType == LIST_FUN_STATUS)
                dictKeyPrefix = "FUN_SPORTS_STATUS_";
            else if (listType == LIST_INF_TYPE)
                dictKeyPrefix = "INF_TYPE_";
            else if (listType == LIST_INF_STATUS)
                dictKeyPrefix = "INF_STATUS_";

            if (!dictKeyPrefix.Equals(""))
            {
                for (int i = 1; i < 100; i++)
                {
                    String dictKey = dictKeyPrefix + i.ToString();
                    String tmpVal = getString(RES_LIST_ITEMS, dictKey, lang);
                    if (tmpVal.StartsWith("!" + dictKey))
                        return retKV;
                    retKV.Add(new KeyValuePair<String, String>(i.ToString(), tmpVal));
                }
            }

            return retKV;
        }

    }
}