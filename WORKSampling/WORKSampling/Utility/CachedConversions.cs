﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Models;

namespace _2DMap.Utility
{
    public class CachedConversions
    {
        public static Int16 CACHED_DICT_ALL = -1;
        public static Int16 CACHED_DICT_COMPANIES = 1;
        public static Int16 CACHED_DICT_DEPARTMENTS = 2;
        public static Int16 CACHED_DICT_WORKERS = 3;
        public static Int16 CACHED_DICT_OPERATORS = 4;
        public static Int16 CACHED_DICT_PTYPE = 5;
        public static Int16 CACHED_DICT_OBCLASS = 6;
        public static Int16 CACHED_DICT_OBSERVATION = 7;
        public static Int16 CACHED_DICT_DEPSETTINGS = 8;

        private static String CACHED_DICT_COMPANIES_KEY = "CACHED_DICT_COMPANIES";
        private static String CACHED_DICT_DEPARTMENTS_KEY = "CACHED_DICT_DEPARTMENTS";
        private static String CACHED_DICT_WORKERS_KEY = "CACHED_DICT_WORKERS";
        private static String CACHED_DICT_OPERATORS_KEY = "CACHED_DICT_OPERATORS";
        private static String CACHED_DICT_PTYPE_KEY = "CACHED_DICT_PTYPE";
        private static String CACHED_DICT_OBCLASS_KEY = "CACHED_DICT_OBCLASS";
        private static String CACHED_DICT_OBSERVATION_KEY = "CACHED_DICT_OBSERVATION";
        private static String CACHED_DICT_DEPSETTINGS_KEY = "CACHED_DICT_DEPSETTINGS";

        public static void GetCachedKeyValues(HttpContext cont, Int16 dictType, out Dictionary<String, String> kvDict)
        {
            kvDict = new Dictionary<String, String>();

            if (dictType == CACHED_DICT_COMPANIES)
            {
                if (cont.Application[CACHED_DICT_COMPANIES_KEY] == null)
                {
                    CompanyItems items = new CompanyItems();
                    items.Load();
                    if (items.CompanyItemsList != null)
                    {
                        foreach (CompanyItem item in items.CompanyItemsList)
                        {
                            if (!kvDict.ContainsKey(item.Id.ToString()))
                                kvDict.Add(item.Id.ToString(), item.Name);
                        }
                    }
                    cont.Application[CACHED_DICT_COMPANIES_KEY] = kvDict;
                }
                else
                {
                    kvDict = (Dictionary<String, String>)cont.Application[CACHED_DICT_COMPANIES_KEY];
                }
            }
            else if (dictType == CACHED_DICT_DEPARTMENTS)
            {
                 if (cont.Application[CACHED_DICT_DEPARTMENTS_KEY] == null)
                {
                    DepartmentItems items = new DepartmentItems();
                    items.Load();
                    if (items.DepartmentItemsList != null)
                    {
                        foreach (DepartmentItem item in items.DepartmentItemsList)
                        {
                            if (!kvDict.ContainsKey(item.Id.ToString()))
                                kvDict.Add(item.Id.ToString(), item.Name);
                        }
                    }
                    cont.Application[CACHED_DICT_DEPARTMENTS_KEY] = kvDict;
                }
                else
                {
                    kvDict = (Dictionary<String, String>)cont.Application[CACHED_DICT_DEPARTMENTS_KEY];
                }
            }

            else if (dictType == CACHED_DICT_OPERATORS)
            {
                if (cont.Application[CACHED_DICT_OPERATORS_KEY] == null)
                {
                    OperatorItems items = new OperatorItems();
                    items.Load();
                    if (items.OperatorItemsList != null)
                    {
                        
                    }
                    cont.Application[CACHED_DICT_OPERATORS_KEY] = kvDict;
                }
                else
                {
                    kvDict = (Dictionary<String, String>)cont.Application[CACHED_DICT_OPERATORS_KEY];
                }
            }

            else if (dictType == CACHED_DICT_OBCLASS)
            {
                if (cont.Application[CACHED_DICT_OBCLASS_KEY] == null)
                {
                    ObclassItems items = new ObclassItems();
                    items.Load();
                    if (items.ObclassItemsList != null)
                    {
                        foreach (ObclassItem item in items.ObclassItemsList)
                        {
                            if (!kvDict.ContainsKey(item.Id.ToString()))
                                kvDict.Add(item.Id.ToString(), item.Name);
                        }
                    }
                    cont.Application[CACHED_DICT_OBCLASS_KEY] = kvDict;
                }
                else
                {
                    kvDict = (Dictionary<String, String>)cont.Application[CACHED_DICT_OBCLASS_KEY];
                }
            }

            else if (dictType == CACHED_DICT_WORKERS)
            {
                if (cont.Application[CACHED_DICT_WORKERS_KEY] == null)
                {
                    WorkerItems items = new WorkerItems();
                    items.Load();
                    if (items.WorkerItemsList != null)
                    {
                        foreach (WorkerItem item in items.WorkerItemsList)
                        {
                            if (!kvDict.ContainsKey(item.Id.ToString()))
                                kvDict.Add(item.Id.ToString(), item.Name);
                        }
                    }
                    cont.Application[CACHED_DICT_WORKERS_KEY] = kvDict;
                }
                else
                {
                    kvDict = (Dictionary<String, String>)cont.Application[CACHED_DICT_WORKERS_KEY];
                }
            }

            else if (dictType == CACHED_DICT_DEPSETTINGS)
            {
                if (cont.Application[CACHED_DICT_DEPSETTINGS_KEY] == null)
                {
                    DepsettingItems items = new DepsettingItems();
                    items.Load();
                    if (items.DepsettingItemsList != null)
                    {
                        foreach (DepsettingItem item in items.DepsettingItemsList)
                        {
                            if (!kvDict.ContainsKey(item.Id.ToString()))
                                kvDict.Add(item.Id.ToString(), item.Id.ToString());
                        }
                    }
                    cont.Application[CACHED_DICT_DEPSETTINGS_KEY] = kvDict;
                }
                else
                {
                    kvDict = (Dictionary<String, String>)cont.Application[CACHED_DICT_DEPSETTINGS_KEY];
                }
            }

            else if (dictType == CACHED_DICT_OBSERVATION)
            {
                if (cont.Application[CACHED_DICT_OBSERVATION_KEY] == null)
                {
                    ObservationItems items = new ObservationItems();
                    items.Load();
                    if (items.ObservationItemsList != null)
                    {

                    }
                    cont.Application[CACHED_DICT_OBSERVATION_KEY] = kvDict;
                }
                else
                {
                    kvDict = (Dictionary<String, String>)cont.Application[CACHED_DICT_OBSERVATION_KEY];
                }
            }

            return;
        }


       
     
        public static void GetCachedDepartmentItems(HttpContext cont, out List<DepartmentItem> listItems)
        {
            listItems = new List<DepartmentItem>();

            if (cont.Application[CACHED_DICT_DEPARTMENTS_KEY] == null)
            {
                DepartmentItems items = new DepartmentItems();
                items.Load();
                if (items.DepartmentItemsList != null)
                {
                    foreach (DepartmentItem item in items.DepartmentItemsList)
                    {
                        listItems.Add(item);
                    }
                }
                cont.Application[CACHED_DICT_DEPARTMENTS_KEY] = listItems;
            }
            else
            {
                listItems = (List<DepartmentItem>)cont.Application[CACHED_DICT_DEPARTMENTS_KEY];
            }

            return;
        }

        public static void GetCachedWorkerItems(HttpContext cont, out List<WorkerItem> listItems)
        {
            listItems = new List<WorkerItem>();

            if (cont.Application[CACHED_DICT_WORKERS_KEY] == null)
            {
                WorkerItems items = new WorkerItems();
                items.Load();
                if (items.WorkerItemsList != null)
                {
                    foreach (WorkerItem item in items.WorkerItemsList)
                    {
                        listItems.Add(item);
                    }
                }
                cont.Application[CACHED_DICT_WORKERS_KEY] = listItems;
            }
            else
            {
                listItems = (List<WorkerItem>)cont.Application[CACHED_DICT_WORKERS_KEY];
            }

            return;
        }


        public static void GetCachedDepsettingItems(HttpContext cont, out List<DepsettingItem> listItems)
        {
            listItems = new List<DepsettingItem>();

            if (cont.Application[CACHED_DICT_DEPSETTINGS_KEY] == null)
            {
                DepsettingItems items = new DepsettingItems();
                items.Load();
                if (items.DepsettingItemsList != null)
                {
                    foreach (DepsettingItem item in items.DepsettingItemsList)
                    {
                        listItems.Add(item);
                    }
                }
                cont.Application[CACHED_DICT_DEPSETTINGS_KEY] = listItems;
            }
            else
            {
                listItems = (List<DepsettingItem>)cont.Application[CACHED_DICT_DEPSETTINGS_KEY];
            }

            return;
        }


        public static void GetCachedObclassItems(HttpContext cont, out List<ObclassItem> listItems)
        {
            listItems = new List<ObclassItem>();

            if (cont.Application[CACHED_DICT_OBCLASS_KEY] == null)
            {
                ObclassItems items = new ObclassItems();
                items.Load();
                if (items.ObclassItemsList != null)
                {
                    foreach (ObclassItem item in items.ObclassItemsList)
                    {
                        listItems.Add(item);
                    }
                }
                cont.Application[CACHED_DICT_OBCLASS_KEY] = listItems;
            }
            else
            {
                listItems = (List<ObclassItem>)cont.Application[CACHED_DICT_OBCLASS_KEY];
            }

            return;
        }

        public static void GetCachedObservationItems(HttpContext cont, out List<ObservationItem> listItems)
        {
            listItems = new List<ObservationItem>();

            if (cont.Application[CACHED_DICT_OBSERVATION_KEY] == null)
            {
                ObservationItems items = new ObservationItems();
                items.Load();
                if (items.ObservationItemsList != null)
                {
                    foreach (ObservationItem item in items.ObservationItemsList)
                    {
                        listItems.Add(item);
                    }
                }
                cont.Application[CACHED_DICT_OBSERVATION_KEY] = listItems;
            }
            else
            {
                listItems = (List<ObservationItem>)cont.Application[CACHED_DICT_OBSERVATION_KEY];
            }

            return;
        }
       
        public static void ClearCachedKeyValues(HttpContext cont, Int16 dictType)
        {
            if (dictType == CACHED_DICT_COMPANIES || dictType == CACHED_DICT_ALL)
                cont.Application.Remove(CACHED_DICT_COMPANIES_KEY);
           
            if (dictType == CACHED_DICT_DEPARTMENTS || dictType == CACHED_DICT_ALL)
                cont.Application.Remove(CACHED_DICT_DEPARTMENTS_KEY);
          
        }
    }
}