USE [TDMAP]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_GAST_SKIC]') AND parent_object_id = OBJECT_ID(N'[dbo].[GASTRONOMIES]'))
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [FK_GAST_SKIC]
GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GASTRONOMIES_GAST_INT_CODE]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [DF_GASTRONOMIES_GAST_INT_CODE]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GASTRONOMIES_GAST_DESC]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [DF_GASTRONOMIES_GAST_DESC]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GASTRONOMIES_GAST_IMG]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [DF_GASTRONOMIES_GAST_IMG]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GASTRONOMIES_GAST_STATUS]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [DF_GASTRONOMIES_GAST_STATUS]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GASTRONOMIES_GAST_INSERT_USER]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [DF_GASTRONOMIES_GAST_INSERT_USER]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GASTRONOMIES_GAST_INSERT_DATE_TIME]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [DF_GASTRONOMIES_GAST_INSERT_DATE_TIME]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GASTRONOMIES_GAST_UPDATE_USER]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [DF_GASTRONOMIES_GAST_UPDATE_USER]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GASTRONOMIES_GAST_UPDATE_DATE_TIME]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [DF_GASTRONOMIES_GAST_UPDATE_DATE_TIME]
END

GO

IF  EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[DF_GASTRONOMIES_GAST_DELETED]') AND type = 'D')
BEGIN
ALTER TABLE [dbo].[GASTRONOMIES] DROP CONSTRAINT [DF_GASTRONOMIES_GAST_DELETED]
END

GO

USE [TDMAP]
GO

/****** Object:  Table [dbo].[GASTRONOMIES]    Script Date: 08/25/2015 11:04:14 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[GASTRONOMIES]') AND type in (N'U'))
DROP TABLE [dbo].[GASTRONOMIES]
GO

USE [TDMAP]
GO

/****** Object:  Table [dbo].[GASTRONOMIES]    Script Date: 08/25/2015 11:04:14 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[GASTRONOMIES](
	[GAST_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SKIC_ID] [bigint] NOT NULL,
	[GAST_NAME] [varchar](256) NOT NULL,
	[GAST_INT_CODE] [varchar](32) NULL,
	[GAST_DESC] [varchar](2048) NULL,
	[GAST_IMG] [varchar](256) NULL,
	[GAST_STATUS] [smallint] NULL,
	[GAST_INSERT_USER] [varchar](50) NULL,
	[GAST_INSERT_DATE_TIME] [bigint] NULL,
	[GAST_UPDATE_USER] [varchar](50) NULL,
	[GAST_UPDATE_DATE_TIME] [bigint] NULL,
	[GAST_DELETED] [smallint] NULL,
 CONSTRAINT [PK_GASTRONOMIES] PRIMARY KEY CLUSTERED 
(
	[GAST_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[GASTRONOMIES]  WITH CHECK ADD  CONSTRAINT [FK_GAST_SKIC] FOREIGN KEY([SKIC_ID])
REFERENCES [dbo].[SKI_CENTERS] ([SKIC_ID])
GO

ALTER TABLE [dbo].[GASTRONOMIES] CHECK CONSTRAINT [FK_GAST_SKIC]
GO

ALTER TABLE [dbo].[GASTRONOMIES] ADD  CONSTRAINT [DF_GASTRONOMIES_GAST_INT_CODE]  DEFAULT ('') FOR [GAST_INT_CODE]
GO

ALTER TABLE [dbo].[GASTRONOMIES] ADD  CONSTRAINT [DF_GASTRONOMIES_GAST_DESC]  DEFAULT ('') FOR [GAST_DESC]
GO

ALTER TABLE [dbo].[GASTRONOMIES] ADD  CONSTRAINT [DF_GASTRONOMIES_GAST_IMG]  DEFAULT ('') FOR [GAST_IMG]
GO

ALTER TABLE [dbo].[GASTRONOMIES] ADD  CONSTRAINT [DF_GASTRONOMIES_GAST_STATUS]  DEFAULT ((0)) FOR [GAST_STATUS]
GO

ALTER TABLE [dbo].[GASTRONOMIES] ADD  CONSTRAINT [DF_GASTRONOMIES_GAST_INSERT_USER]  DEFAULT ('') FOR [GAST_INSERT_USER]
GO

ALTER TABLE [dbo].[GASTRONOMIES] ADD  CONSTRAINT [DF_GASTRONOMIES_GAST_INSERT_DATE_TIME]  DEFAULT ((0)) FOR [GAST_INSERT_DATE_TIME]
GO

ALTER TABLE [dbo].[GASTRONOMIES] ADD  CONSTRAINT [DF_GASTRONOMIES_GAST_UPDATE_USER]  DEFAULT ('') FOR [GAST_UPDATE_USER]
GO

ALTER TABLE [dbo].[GASTRONOMIES] ADD  CONSTRAINT [DF_GASTRONOMIES_GAST_UPDATE_DATE_TIME]  DEFAULT ((0)) FOR [GAST_UPDATE_DATE_TIME]
GO

ALTER TABLE [dbo].[GASTRONOMIES] ADD  CONSTRAINT [DF_GASTRONOMIES_GAST_DELETED]  DEFAULT ((0)) FOR [GAST_DELETED]
GO

