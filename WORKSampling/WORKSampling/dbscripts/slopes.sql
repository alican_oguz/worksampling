USE [TDMAP]
GO

/****** Object:  Table [dbo].[SLOPES]    Script Date: 08/24/2015 12:29:21 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[SLOPES](
	[SLP_ID] [bigint] IDENTITY(1,1) NOT NULL,
	[SKIC_ID] [bigint] NOT NULL,
	[SLP_SHORT_NAME] [varchar](32) NOT NULL,
	[SLP_NAME] [varchar](256) NOT NULL,
	[SLP_INT_CODE] [varchar](32) NOT NULL,
	[SLP_STATUS] [smallint] NULL,
	[SLP_TYPE] [smallint] NULL,
	[SLP_INSERT_USER] [varchar](50) NULL,
	[SLP_INSERT_DATE_TIME] [bigint] NULL,
	[SLP_UPDATE_USER] [varchar](50) NULL,
	[SLP_UPDATE_DATE_TIME] [bigint] NULL,
	[SLP_DELETED] [smallint] NULL,
 CONSTRAINT [PK_SLOPES] PRIMARY KEY CLUSTERED 
(
	[SLP_ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[SLOPES]  WITH CHECK ADD  CONSTRAINT [FK_SLP_SKIC] FOREIGN KEY([SKIC_ID])
REFERENCES [dbo].[SKI_CENTERS] ([SKIC_ID])
GO

ALTER TABLE [dbo].[SLOPES] CHECK CONSTRAINT [FK_SLP_SKIC]
GO

ALTER TABLE [dbo].[SLOPES] ADD  CONSTRAINT [DF_SLOPES_SLP_STATUS]  DEFAULT ((0)) FOR [SLP_STATUS]
GO

ALTER TABLE [dbo].[SLOPES] ADD  CONSTRAINT [DF_SLOPES_SLP_TYPE]  DEFAULT ((0)) FOR [SLP_TYPE]
GO

ALTER TABLE [dbo].[SLOPES] ADD  CONSTRAINT [DF_SLOPES_SLP_INSERT_USER]  DEFAULT ('') FOR [SLP_INSERT_USER]
GO

ALTER TABLE [dbo].[SLOPES] ADD  CONSTRAINT [DF_SLOPES_SLP_INSERT_DATE_TIME]  DEFAULT ((0)) FOR [SLP_INSERT_DATE_TIME]
GO

ALTER TABLE [dbo].[SLOPES] ADD  CONSTRAINT [DF_SLOPES_SLP_UPDATE_USER]  DEFAULT ('') FOR [SLP_UPDATE_USER]
GO

ALTER TABLE [dbo].[SLOPES] ADD  CONSTRAINT [DF_SLOPES_SLP_UPDATE_DATE_TIME]  DEFAULT ((0)) FOR [SLP_UPDATE_DATE_TIME]
GO

ALTER TABLE [dbo].[SLOPES] ADD  CONSTRAINT [DF_SLOPES_SLP_DELETED]  DEFAULT ((0)) FOR [SLP_DELETED]
GO

