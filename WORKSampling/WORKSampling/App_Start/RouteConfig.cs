﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace _2DMap
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            #region LoginController

            routes.MapRoute(
                name: "Login",
                url: "login",
                defaults: new { controller = "Login", action = "LoginPage" }
            );

            routes.MapRoute(
               name: "mustchangepwd",
               url: "mustchangepwd",
               defaults: new { controller = "Login", action = "UserMustChangePwdPage" }
           );
            
            routes.MapRoute(
                name: "RememberPwd",
                url: "pwdre",
                defaults: new { controller = "Login", action = "RememberPwdPage" }
            );

            #endregion

            #region users management

            routes.MapRoute(
                name: "UserList",
                url: "usr/list",
                defaults: new { controller = "UserMng", action = "UserList" }
            );

            routes.MapRoute(
                name: "UserEdit",
                url: "usr/edit/{refcode}",
                defaults: new { controller = "UserMng", action = "UserEdit" }
            );

            routes.MapRoute(
                name: "UserEditGet",
                url: "edit/usr/get",
                defaults: new { controller = "UserMng", action = "DoGetUserEdit" }
            );

            routes.MapRoute(
                name: "UserEditProcess",
                url: "process/usr/edit",
                defaults: new { controller = "UserMng", action = "DoUserEdit" }
            );

            routes.MapRoute(
                name: "UserAdd",
                url: "usr/add",
                defaults: new { controller = "UserMng", action = "UserAdd" }
            );

            routes.MapRoute(
               name: "UserDelete",
               url: "process/usr/delete/{refcode}",
               defaults: new { controller = "UserMng", action = "DeleteUser" }
            );

            routes.MapRoute(
               name: "UserBlockLogin",
               url: "process/usr/block/{refcode}",
               defaults: new { controller = "UserMng", action = "BlockUser" }
            );

            routes.MapRoute(
               name: "UserUnBlockLogin",
               url: "process/usr/unblock/{refcode}",
               defaults: new { controller = "UserMng", action = "UnBlockUser" }
            );

            routes.MapRoute(
               name: "UserRenewPed",
               url: "process/usr/renewpwd/{refcode}",
               defaults: new { controller = "UserMng", action = "RenewPwd" }
            );

            #endregion

            #region data management

            routes.MapRoute(
              name: "OperatorsListList",
              url: "data/ope",
              defaults: new { controller = "DataMng", action = "OperatorsList" }
          );

            routes.MapRoute(
                name: "OperatorsAdd",
                url: "data/ope/add",
                defaults: new { controller = "DataMng", action = "OperatorsAdd" }
           );

            routes.MapRoute(
                name: "OperatorsAddProcess",
                url: "process/data/ope/add",
                defaults: new { controller = "DataMng", action = "DoOperatorsAdd" }
            );

            routes.MapRoute(
                name: "OperatorsEdit",
                url: "data/ope/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "OperatorsEdit" }
            );

            routes.MapRoute(
                name: "OperatorsEditProcess",
                url: "process/data/ope/edit",
                defaults: new { controller = "DataMng", action = "DoOperatorsEdit" }
            );

            routes.MapRoute(
                name: "OperatorsEditGet",
                url: "edit/data/ope/get",
                defaults: new { controller = "DataMng", action = "DoGetOperatorsEdit" }
            );

            routes.MapRoute(
                name: "OperatorsDelete",
                url: "process/data/ope/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoOperatorsDelete" }
            );


            routes.MapRoute(
               name: "WorkersListList",
               url: "data/worker",
               defaults: new { controller = "DataMng", action = "WorkersList" }
           );

            routes.MapRoute(
                name: "WorkersAdd",
                url: "data/worker/add",
                defaults: new { controller = "DataMng", action = "WorkersAdd" }
           );

            routes.MapRoute(
                name: "WorkersAddProcess",
                url: "process/data/worker/add",
                defaults: new { controller = "DataMng", action = "DoWorkersAdd" }
            );

            routes.MapRoute(
                name: "WorkersEdit",
                url: "data/worker/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "WorkersEdit" }
            );

            routes.MapRoute(
                name: "WorkersEditProcess",
                url: "process/data/worker/edit",
                defaults: new { controller = "DataMng", action = "DoWorkersEdit" }
            );

            routes.MapRoute(
                name: "WorkersEditGet",
                url: "edit/data/worker/get",
                defaults: new { controller = "DataMng", action = "DoGetWorkersEdit" }
            );

            routes.MapRoute(
                name: "WorkersDelete",
                url: "process/data/worker/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoWorkersDelete" }
            );



            routes.MapRoute(
              name: "DepsettingsListList",
              url: "data/depsetting",
              defaults: new { controller = "DataMng", action = "DepsettingsList" }
          );

            routes.MapRoute(
                name: "DepsettingsAdd",
                url: "data/depsetting/add",
                defaults: new { controller = "DataMng", action = "DepsettingsAdd" }
           );

            routes.MapRoute(
                name: "DepsettingsAddProcess",
                url: "process/data/depsetting/add",
                defaults: new { controller = "DataMng", action = "DoDepsettingsAdd" }
            );

            routes.MapRoute(
                name: "DepsettingsEdit",
                url: "data/depsetting/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "DepsettingsEdit" }
            );

            routes.MapRoute(
                name: "DepsettingsEditProcess",
                url: "process/data/depsetting/edit",
                defaults: new { controller = "DataMng", action = "DoDepsettingsEdit" }
            );

            routes.MapRoute(
                name: "DepsettingsEditGet",
                url: "edit/data/depsetting/get",
                defaults: new { controller = "DataMng", action = "DoGetDepsettingsEdit" }
            );

            routes.MapRoute(
                name: "DepsettingsDelete",
                url: "process/data/depsetting/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoDepsettingsDelete" }
            );


            routes.MapRoute(
              name: "ObclassListList",
              url: "data/obclass",
              defaults: new { controller = "DataMng", action = "ObclassList" }
          );

            routes.MapRoute(
                name: "ObclassAdd",
                url: "data/obclass/add",
                defaults: new { controller = "DataMng", action = "ObclassAdd" }
           );

            routes.MapRoute(
                name: "ObclassAddProcess",
                url: "process/data/obclass/add",
                defaults: new { controller = "DataMng", action = "DoObclassAdd" }
            );

            routes.MapRoute(
                name: "ObclassEdit",
                url: "data/obclass/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "ObclassEdit" }
            );

            routes.MapRoute(
                name: "ObclassEditProcess",
                url: "process/data/obclass/edit",
                defaults: new { controller = "DataMng", action = "DoObclassEdit" }
            );

            routes.MapRoute(
                name: "ObclassEditGet",
                url: "edit/data/obclass/get",
                defaults: new { controller = "DataMng", action = "DoGetObclassEdit" }
            );

            routes.MapRoute(
                name: "ObclassDelete",
                url: "process/data/obclass/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoObclassDelete" }
            );



            routes.MapRoute(
              name: "ObservationListList",
              url: "data/observation",
              defaults: new { controller = "DataMng", action = "ObservationList" }
          );

            routes.MapRoute(
                name: "ObservationAdd",
                url: "data/observation/add",
                defaults: new { controller = "DataMng", action = "ObservationAdd" }
           );

            routes.MapRoute(
                name: "ObservationAddProcess",
                url: "process/data/observation/add",
                defaults: new { controller = "DataMng", action = "DoObservationAdd" }
            );

            routes.MapRoute(
                name: "ObservationEdit",
                url: "data/observation/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "ObservationEdit" }
            );

            routes.MapRoute(
                name: "ObservationEditProcess",
                url: "process/data/observation/edit",
                defaults: new { controller = "DataMng", action = "DoObservationEdit" }
            );

            routes.MapRoute(
                name: "ObservationEditGet",
                url: "edit/data/observation/get",
                defaults: new { controller = "DataMng", action = "DoGetObservationEdit" }
            );

            routes.MapRoute(
                name: "ObservationDelete",
                url: "process/data/observation/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoObservationDelete" }
            );


            routes.MapRoute(
               name: "DepartmentsListList",
               url: "data/dept",
               defaults: new { controller = "DataMng", action = "DepartmentsList" }
           );

            routes.MapRoute(
                name: "DepartmentsAdd",
                url: "data/dept/add",
                defaults: new { controller = "DataMng", action = "DepartmentsAdd" }
           );

            routes.MapRoute(
                name: "DepartmentsAddProcess",
                url: "process/data/dept/add",
                defaults: new { controller = "DataMng", action = "DoDepartmentsAdd" }
            );

            routes.MapRoute(
                name: "DepartmentsEdit",
                url: "data/dept/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "DepartmentsEdit" }
            );

            routes.MapRoute(
                name: "DepartmentsEditProcess",
                url: "process/data/dept/edit",
                defaults: new { controller = "DataMng", action = "DoDepartmentsEdit" }
            );

            routes.MapRoute(
                name: "DepartmentsEditGet",
                url: "edit/data/dept/get",
                defaults: new { controller = "DataMng", action = "DoGetDepartmentsEdit" }
            );

            routes.MapRoute(
                name: "DepartmentsDelete",
                url: "process/data/dept/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoDepartmentsDelete" }
            );



            routes.MapRoute(
                name: "CompaniesList",
                url: "data/comp",
                defaults: new { controller = "DataMng", action = "CompaniesList" }
            );

            routes.MapRoute(
                name: "CompaniesAdd",
                url: "data/comp/add",
                defaults: new { controller = "DataMng", action = "CompaniesAdd" }
            );

            routes.MapRoute(
                name: "CompaniesAddProcess",
                url: "process/data/comp/add",
                defaults: new { controller = "DataMng", action = "DoCompaniesAdd" }
            );

            routes.MapRoute(
                name: "CompaniesEdit",
                url: "data/comp/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "CompaniesEdit" }
            );

            routes.MapRoute(
                name: "CompaniesEditProcess",
                url: "process/data/comp/edit",
                defaults: new { controller = "DataMng", action = "DoCompaniesEdit" }
            );

            routes.MapRoute(
                name: "CompaniesEditGet",
                url: "edit/data/comp/get",
                defaults: new { controller = "DataMng", action = "DoGetCompaniesEdit" }
            );

            routes.MapRoute(
                name: "CompaniesDelete",
                url: "process/data/comp/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoCompaniesDelete" }
            );

            routes.MapRoute(
                name: "SkiCentersList",
                url: "data/ski",
                defaults: new { controller = "DataMng", action = "SkiCentersList" }
            );

            routes.MapRoute(
                name: "SkiCentersAdd",
                url: "data/ski/add",
                defaults: new { controller = "DataMng", action = "SkiCentersAdd" }
            );

            routes.MapRoute(
                name: "SkiCentersAddProcess",
                url: "process/data/ski/add",
                defaults: new { controller = "DataMng", action = "DoSkiCentersAdd" }
            );

            routes.MapRoute(
                name: "SkiCentersEdit",
                url: "data/ski/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "SkiCentersEdit" }
            );

            routes.MapRoute(
                name: "SkiCentersEditProcess",
                url: "process/data/ski/edit",
                defaults: new { controller = "DataMng", action = "DoSkiCentersEdit" }
            );

            routes.MapRoute(
                name: "SkiCentersEditGet",
                url: "edit/data/ski/get",
                defaults: new { controller = "DataMng", action = "DoGetSkiCentersEdit" }
            );

            routes.MapRoute(
                name: "SkiCentersDelete",
                url: "process/data/ski/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoSkiCentersDelete" }
            );


            routes.MapRoute(
                name: "LiftsList",
                url: "data/lifts",
                defaults: new { controller = "DataMng", action = "LiftsList" }
            );

            routes.MapRoute(
                name: "LiftsAdd",
                url: "data/lifts/add",
                defaults: new { controller = "DataMng", action = "LiftsAdd" }
            );

            routes.MapRoute(
                name: "LiftsAddProcess",
                url: "process/data/lifts/add",
                defaults: new { controller = "DataMng", action = "DoLiftsAdd" }
            );

            routes.MapRoute(
                name: "LiftsEdit",
                url: "data/lifts/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "LiftsEdit" }
            );

            routes.MapRoute(
                name: "LiftsEditProcess",
                url: "process/data/lifts/edit",
                defaults: new { controller = "DataMng", action = "DoLiftsEdit" }
            );

            routes.MapRoute(
                name: "LiftsEditGet",
                url: "edit/data/lifts/get",
                defaults: new { controller = "DataMng", action = "DoGetLiftsEdit" }
            );

            routes.MapRoute(
                name: "LiftsDelete",
                url: "process/data/lifts/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoLiftsDelete" }
            );
            routes.MapRoute(
                name: "SlopesListList",
                url: "data/slopes",
                defaults: new { controller = "DataMng", action = "SlopesList" }
            );

            routes.MapRoute(
                name: "SlopesAdd",
                url: "data/slopes/add",
                defaults: new { controller = "DataMng", action = "SlopesAdd" }
           );

            routes.MapRoute(
                name: "SlopesAddProcess",
                url: "process/data/slopes/add",
                defaults: new { controller = "DataMng", action = "DoSlopesAdd" }
            );

            routes.MapRoute(
                name: "SlopesEdit",
                url: "data/slopes/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "SlopesEdit" }
            );

            routes.MapRoute(
                name: "SlopesEditProcess",
                url: "process/data/slopes/edit",
                defaults: new { controller = "DataMng", action = "DoSlopesEdit" }
            );

            routes.MapRoute(
                name: "SlopesEditGet",
                url: "edit/data/slopes/get",
                defaults: new { controller = "DataMng", action = "DoGetSlopesEdit" }
            );

            routes.MapRoute(
                name: "SlopesDelete",
                url: "process/data/slopes/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoSlopesDelete" }
            );

            routes.MapRoute(
                name: "LiveCamsList",
                url: "data/cams",
                defaults: new { controller = "DataMng", action = "LiveCamsList" }
            );

            routes.MapRoute(
                name: "LiveCamsAdd",
                url: "data/cams/add",
                defaults: new { controller = "DataMng", action = "LiveCamsAdd" }
            );

            routes.MapRoute(
                name: "LiveCamsAddProcess",
                url: "process/data/cams/add",
                defaults: new { controller = "DataMng", action = "DoLiveCamsAdd" }
            );

            routes.MapRoute(
                name: "LiveCamsEdit",
                url: "data/cams/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "LiveCamsEdit" }
            );

            routes.MapRoute(
                name: "LiveCamsEditProcess",
                url: "process/data/cams/edit",
                defaults: new { controller = "DataMng", action = "DoLiveCamsEdit" }
            );

            routes.MapRoute(
                name: "LiveCamsEditGet",
                url: "edit/data/cams/get",
                defaults: new { controller = "DataMng", action = "DoGetLiveCamsEdit" }
            );

            routes.MapRoute(
                name: "LiveCamsDelete",
                url: "process/data/cams/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoLiveCamsDelete" }
            );

            routes.MapRoute(
                name: "InfrastructureList",
                url: "data/inf",
                defaults: new { controller = "DataMng", action = "InfrastructureList" }
            );

            routes.MapRoute(
                name: "InfrastructureAdd",
                url: "data/inf/add",
                defaults: new { controller = "DataMng", action = "InfrastructureAdd" }
            );

            routes.MapRoute(
                name: "InfrastructureAddProcess",
                url: "process/data/inf/add",
                defaults: new { controller = "DataMng", action = "DoInfrastructureAdd" }
            );

            routes.MapRoute(
                name: "InfrastructureEdit",
                url: "data/inf/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "InfrastructureEdit" }
            );

            routes.MapRoute(
                name: "InfrastructureEditProcess",
                url: "process/data/inf/edit",
                defaults: new { controller = "DataMng", action = "DoInfrastructureEdit" }
            );

            routes.MapRoute(
                name: "InfrastructureEditGet",
                url: "edit/data/inf/get",
                defaults: new { controller = "DataMng", action = "DoGetInfrastructureEdit" }
            );

            routes.MapRoute(
                name: "InfrastructureDelete",
                url: "process/data/inf/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoInfrastructureDelete" }
            );

            routes.MapRoute(
                name: "GastronomyList",
                url: "data/gast",
                defaults: new { controller = "DataMng", action = "GastronomyList" }
            );
            routes.MapRoute(
                name: "GastronomyAdd",
                url: "data/gast/add",
                defaults: new { controller = "DataMng", action = "GastronomyAdd" }
            );

            routes.MapRoute(
                name: "GastronomyEdit",
                url: "data/gast/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "GastronomyEdit" }
            );

            routes.MapRoute(
                name: "GastronomyEditProcess",
                url: "process/data/gast/edit",
                defaults: new { controller = "DataMng", action = "DoGastronomyEdit" }
            );

            routes.MapRoute(
                name: "GastronomyEditGet",
                url: "edit/data/gast/get",
                defaults: new { controller = "DataMng", action = "DoGetGastronomyEdit" }
            );

            routes.MapRoute(
                name: "GastronomyDelete",
                url: "process/data/gast/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoGastronomyDelete" }
            );

            routes.MapRoute(
                name: "DoGastronomyAddProcess",
                url: "process/data/gast/add",
                defaults: new { controller = "DataMng", action = "DoGastronomyAdd" }
            );

            routes.MapRoute(
                name: "SportsFunList",
                url: "data/sfun",
                defaults: new { controller = "DataMng", action = "SportsFunList" }
            );

            routes.MapRoute(
                name: "SportsFunAdd",
                url: "data/sfun/add",
                defaults: new { controller = "DataMng", action = "SportsFunAdd" }
            );

            routes.MapRoute(
                name: "SportsFunAddProcess",
                url: "process/data/sfun/add",
                defaults: new { controller = "DataMng", action = "DoSportsFunAdd" }
            );

            routes.MapRoute(
                name: "SportsFunEdit",
                url: "data/sfun/edit/{refcode}",
                defaults: new { controller = "DataMng", action = "SportsFunEdit" }
            );

            routes.MapRoute(
                name: "SportsFunEditProcess",
                url: "process/data/sfun/edit",
                defaults: new { controller = "DataMng", action = "DoSportsFunEdit" }
            );

            routes.MapRoute(
                name: "SportsFunEditGet",
                url: "edit/data/sfun/get",
                defaults: new { controller = "DataMng", action = "DoGetSportsFunEdit" }
            );

            routes.MapRoute(
                name: "SportsFunDelete",
                url: "process/data/sfun/delete/{refcode}",
                defaults: new { controller = "DataMng", action = "DoSportsFunDelete" }
            );

            #endregion

            #region status update

            routes.MapRoute(
                name: "DataUpdateStatus",
                url: "data/updstatus",
                defaults: new { controller = "DataMng", action = "UpdateStatus" }
            );

            #endregion

            #region maps management

            routes.MapRoute(
                name: "DraftMapsList",
                url: "map/draft",
                defaults: new { controller = "Map", action = "DraftMapsList" }
            );

            routes.MapRoute(
                name: "DraftMapsAdd",
                url: "map/draft/add",
                defaults: new { controller = "Map", action = "DraftMapsAdd" }
            );

            routes.MapRoute(
                name: "LiveMapsList",
                url: "map/live",
                defaults: new { controller = "Map", action = "LiveMapsList" }
            );

            #endregion

            #region testmng

            routes.MapRoute(
             name: "TestObservationList",
             url: "test/observation/list",
             defaults: new { controller = "TestMng", action = "ObservationList" }
         );

            routes.MapRoute(
                name: "TestObservationAdd",
                url: "test/observation/add",
                defaults: new { controller = "TestMng", action = "ObservationAdd" }
           );

            routes.MapRoute(
                name: "TestObservationReportAdd",
                url: "test/observation/report/{refcode}",
                defaults: new { controller = "TestMng", action = "ObservationReportAdd" }
           );

            routes.MapRoute(
                name: "TestObservationAddProcess",
                url: "process/test/observation/add",
                defaults: new { controller = "TestMng", action = "DoObservationAdd" }
            );

            routes.MapRoute(
                name: "TestObservationDelete",
                url: "process/test/observation/delete/{refcode}",
                defaults: new { controller = "TestMng", action = "DoObservationDelete" }
            );

            routes.MapRoute(
                name: "TestObservationReportAddProcess",
                url: "process/test/observation/report/add",
                defaults: new { controller = "TestMng", action = "DoObservationReportAdd" }
            );

            #endregion

            #region parametermng

            routes.MapRoute(
             name: "ParameterList",
             url: "parameter/list",
             defaults: new { controller = "Parameter", action = "ParameterList" }
         );

            routes.MapRoute(
                name: "ParameterAdd",
                url: "parameter/add",
                defaults: new { controller = "Parameter", action = "ParameterAdd" }
           );

            routes.MapRoute(
                name: "ParameterProcess",
                url: "process/parameter/add",
                defaults: new { controller = "Parameter", action = "DoParameterAdd" }
            );

            routes.MapRoute(
                name: "ParameterDelete",
                url: "process/parameter/delete/{refcode}",
                defaults: new { controller = "Parameter", action = "DoParameterDelete" }
            );
            #endregion

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}