﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace _2DMap
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Common/script/tools/jquery-1.11.3.min.js",
                "~/Common/script/tools/jquery.validate.min.js",
                "~/Common/script/tools/messages_tr.js",
                "~/Common/script/tools/additional-methods.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                "~/Common/bootstrap/js/bootstrap.min.js",
                "~/Common/script/tools/bootstrap-datetimepicker.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/adminlte").Include(
                "~/Common/adminlte/js/app.min.js",
                "~/Common/plugins/slimScroll/jquery.slimscroll.min.js",
                "~/Common/plugins/fastclick/fastclick.min.js",
                "~/Common/adminlte/js/demo.js"));

            bundles.Add(new ScriptBundle("~/bundles/tools").Include(
                "~/Common/script/tools/delete.js",
                "~/Common/script/tools/tools.js"));

            bundles.Add(new ScriptBundle("~/bundles/ckfinder").Include(
                "~/Common/plugins/ckfinder/ckfinder.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Common/bootstrap/css/bootstrap.min.css",
                "~/Common/style/font-awesome.min.css",
                "~/Common/adminlte/css/AdminLTE.min.css",
                "~/Common/adminlte/css/skins/skin-blue.min.css",
                "~/Common/style/ionicons.min.css",
                "~/Common/style/bootstrap-datetimepicker.min.css",
                "~/Common/style/layout/site.css"));

            #region plugins

            bundles.Add(new StyleBundle("~/plugins/css/iCheck").Include(
                "~/Common/plugins/iCheck/square/blue.css"));

            bundles.Add(new ScriptBundle("~/plugins/js/iCheck").Include(
                "~/Common/plugins/iCheck/iCheck.min.js"));

            bundles.Add(new StyleBundle("~/plugins/css/datatables").Include(
                "~/Common/plugins/datatables/dataTables.bootstrap.css"));

            bundles.Add(new ScriptBundle("~/plugins/js/dataTables").Include(
                "~/Common/plugins/datatables/jquery.dataTables.min.js",
                "~/Common/plugins/datatables/dataTables.bootstrap.min.js"));

            bundles.Add(new StyleBundle("~/plugins/css/btWYSIHTML").Include(
                "~/Common/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css"));

            bundles.Add(new ScriptBundle("~/plugins/js/btWYSIHTML").Include(
                "~/Common/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"));

            #endregion

            #region Pages

            bundles.Add(new ScriptBundle("~/pages/js/TestObservationReportAdd").Include(
                "~/Common/script/pages/TestObservationReportAdd.js"));


            bundles.Add(new ScriptBundle("~/pages/js/ParameterAdd").Include(
                "~/Common/script/pages/ParameterAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/TestObservationAdd").Include(
             "~/Common/script/pages/TestObservationAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/OperatorsAdd").Include(
             "~/Common/script/pages/OperatorsAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/OperatorsEdit").Include(
                "~/Common/script/pages/OperatorsEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/WorkersAdd").Include(
             "~/Common/script/pages/WorkersAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/WorkersEdit").Include(
                "~/Common/script/pages/WorkersEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/DepsettingsAdd").Include(
             "~/Common/script/pages/DepsettingsAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/DepsettingsEdit").Include(
                "~/Common/script/pages/DepsettingsEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/ObclassAdd").Include(
            "~/Common/script/pages/ObclassAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/ObclassEdit").Include(
                "~/Common/script/pages/ObclassEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/ObservationAdd").Include(
          "~/Common/script/pages/ObservationAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/ObservationEdit").Include(
                "~/Common/script/pages/ObservationEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/DepartmentsAdd").Include(
                "~/Common/script/pages/DepartmentsAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/DepartmentsEdit").Include(
                "~/Common/script/pages/DepartmentsEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/CompaniesAdd").Include(
               "~/Common/script/pages/CompaniesAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/CompaniesEdit").Include(
                "~/Common/script/pages/CompaniesEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/LoginPage").Include(
                "~/Common/script/pages/LoginPage.js"));

            bundles.Add(new ScriptBundle("~/pages/js/RememberPwdPage").Include(
                "~/Common/script/pages/RememberPwdPage.js"));

            bundles.Add(new ScriptBundle("~/pages/js/UserMustChangePwdPage").Include(
                "~/Common/script/pages/UserMustChangePwdPage.js"));

            bundles.Add(new ScriptBundle("~/pages/js/UserAdd").Include(
                "~/Common/script/pages/UserAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/UserEdit").Include(
                "~/Common/script/pages/UserEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/SkiCentersAdd").Include(
                "~/Common/script/pages/SkiCentersAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/SkiCentersEdit").Include(
                "~/Common/script/pages/SkiCentersEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/LiftsAdd").Include(
                "~/Common/script/pages/LiftsAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/LiftsEdit").Include(
                "~/Common/script/pages/LiftsEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/SlopesAdd").Include(
               "~/Common/script/pages/SlopesAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/SlopesEdit").Include(
                "~/Common/script/pages/SlopesEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/LiveCamsAdd").Include(
               "~/Common/script/pages/LiveCamsAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/LiveCamsEdit").Include(
               "~/Common/script/pages/LiveCamsEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/GastAdd").Include(
               "~/Common/script/pages/GastAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/GastEdit").Include(
              "~/Common/script/pages/GastEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/SportsFunAdd").Include(
               "~/Common/script/pages/SportsFunAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/SportsFunEdit").Include(
                "~/Common/script/pages/SportsFunEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/InfrastructureAdd").Include(
              "~/Common/script/pages/InfrastructureAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/InfrastructureEdit").Include(
                "~/Common/script/pages/InfrastructureEdit.js"));

            bundles.Add(new ScriptBundle("~/pages/js/ShowChangePassword").Include(
                "~/Common/script/pages/ShowChangePassword.js"));

            bundles.Add(new ScriptBundle("~/pages/js/UpdateStatus").Include(
               "~/Common/script/pages/UpdateStatus.js"));

            bundles.Add(new ScriptBundle("~/pages/js/DraftMapsAdd").Include(
              "~/Common/script/pages/DraftMapsAdd.js"));

            bundles.Add(new ScriptBundle("~/pages/js/DraftMapsEdit").Include(
                "~/Common/script/pages/DraftMapsEdit.js"));
            #endregion

        }
    }
}