﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class TestObservationDataItem
    {
        public Int64 Id { get; set; }
        public Int64 ObservationId { get; set; }
        public Int64 Parameter { get; set; }
        public Int64 WorkerId { get; set; }
        public Int64 StartDateTime { get; set; }
        public string InsertUser { get; set; }
        public Int64 InsertDateTime { get; set; }
        public string UpdateUser { get; set; }
        public Int64 UpdateDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }
        public string ObservationName { get; set; }

        public TestObservationDataItem()
        {
            this.Id = -1;
            this.ObservationId = -1;
            this.StartDateTime = -1;
            this.Parameter = -1;
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
            this.ObservationName = "";
        }

        public TestObservationDataItem(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.Id = valuesDict.ContainsKey("DataId") && valuesDict["DataId"] != null ? Int64.Parse(valuesDict["DataId"].ToString()) : -1;
            this.Parameter = valuesDict.ContainsKey("ParameterId") && valuesDict["ParameterId"] != null ? Int64.Parse(valuesDict["ParameterId"].ToString()) : -1; 
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;

        }

        public String[] Update()
        {
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Id <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationDataId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                OBSERVATION_DATA dbItem =
                    tdmapEntities.OBSERVATION_DATA
                        .Where(item => item.ID == this.Id && item.DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {
                    dbItem.UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.UPDATE_USER = CommonConfiguration.SessionUserName();
                    dbItem.PARAMETER = this.Parameter;
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "ACTIVITIES", this.Id.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }


    }
}