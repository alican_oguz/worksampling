﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class CompanyItems
    {
          public List<CompanyItem> CompanyItemsList { get; set; }

        public CompanyItems()
        {
            this.CompanyItemsList = new List<CompanyItem>();
        }

        public String[] Load()
        {
            String[] result = { "true", "", "" };

            try
            {
                List<COMPANIES> coMpanies = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.CompanyItemsList = new List<CompanyItem>();

                coMpanies = 
                    tdmapEntities.COMPANIES.Where(
                        company => company.COMP_DELETED != 1
                    ).OrderBy(company => company.COMP_NAME).ToList<COMPANIES>();
                
                if (coMpanies != null)
                {
                    foreach (COMPANIES company in coMpanies)
                    {
                        CompanyItem item = new CompanyItem();
                        item.Id = company.COMP_ID;
                        item.Name = company.COMP_NAME;
                        item.Manager = company.MANAGER;
                        item.CompanyCategory = company.COMP_CATEGORY;
                        item.CompanyEmail = company.COMP_EMAIL;
                        item.CompanyWorkerNumber = Int64.Parse(company.COMP_WORKERS_NMBR.ToString());
                        item.InsertUser = company.COMP_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(company.COMP_INSERT_DATETIME.ToString());
                        item.UpdateUser = company.COMP_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(company.COMP_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(company.COMP_DELETED.ToString());
                        this.CompanyItemsList.Add(item);
                    }
                }

            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

       
    }
}