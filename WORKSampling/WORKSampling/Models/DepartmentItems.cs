﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class DepartmentItems
    {
         public List<DepartmentItem> DepartmentItemsList { get; set; }

        public DepartmentItems()
        {
            this.DepartmentItemsList = new List<DepartmentItem>();
        }

        public String[] Load()
        {
            String[] result = { "true", "", "" };

            try
            {
                List<DEPARTMENTS> departments = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.DepartmentItemsList = new List<DepartmentItem>();
                String username = CommonConfiguration.SessionUserName();
                departments =
                    tdmapEntities.DEPARTMENTS.Where(
                        lift => lift.DEPT_DELETED != 1 && lift.DEPT_INSERT_USER.Equals(username)
                    ).OrderBy(lift => lift.DEPT_NAME).ToList<DEPARTMENTS>();

                if (departments != null)
                {
                    foreach (DEPARTMENTS department in departments)
                    {
                        DepartmentItem item = new DepartmentItem();
                        item.Id = department.DEPT_ID;
                        item.CompanyId = department.COMP_ID;
                        item.Name = department.DEPT_NAME;
                        item.InsertUser = department.DEPT_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(department.DEPT_INSERT_DATETIME.ToString());
                        item.UpdateUser = department.DEPT_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(department.DEPT_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(department.DEPT_DELETED.ToString());
                        item.LoadDisplayData();
                        this.DepartmentItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] LoadByCompanyId(Int64 coMpanyId)
        {
            String[] result = { "true", "", "" };

            try
            {
                List<DEPARTMENTS> departments = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.DepartmentItemsList = new List<DepartmentItem>();

                departments =
                    tdmapEntities.DEPARTMENTS.Where(
                        lift => lift.DEPT_DELETED != 1
                            && lift.COMP_ID == coMpanyId
                    ).OrderBy(lift => lift.DEPT_NAME).ToList<DEPARTMENTS>();

                if (departments != null)
                {
                    foreach (DEPARTMENTS department in departments)
                    {
                        DepartmentItem item = new DepartmentItem();
                        item.Id = department.DEPT_ID;
                        item.CompanyId = department.COMP_ID;
                        item.Name = department.DEPT_NAME;
                        item.InsertUser = department.DEPT_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(department.DEPT_INSERT_DATETIME.ToString());
                        item.UpdateUser = department.DEPT_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(department.DEPT_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(department.DEPT_DELETED.ToString());
                        item.LoadDisplayData();
                        this.DepartmentItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

 

    }
}