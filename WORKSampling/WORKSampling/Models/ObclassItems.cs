﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class ObclassItems
    {

        public List<ObclassItem> ObclassItemsList { get; set; }

        public ObclassItems()
        {
            this.ObclassItemsList = new List<ObclassItem>();
        }

        public String[] Load()
        {
            String[] result = { "true", "", "" };

            try
            {
                List<ACTIVITY_TYPES> obclasses = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.ObclassItemsList = new List<ObclassItem>();
                String username = CommonConfiguration.SessionUserName();
                obclasses =
                    tdmapEntities.ACTIVITY_TYPES.Where(
                        lift => lift.OBS_CLASS_DELETED != 1 && lift.OBS_CLASS_INSERT_USER.Equals(username)
                    ).OrderBy(lift => lift.OBS_CLASS).ToList<ACTIVITY_TYPES>();

                if (obclasses != null)
                {
                    foreach (ACTIVITY_TYPES obclass in obclasses)
                    {
                        ObclassItem item = new ObclassItem();
                        item.Id = obclass.ACT_TYPE_ID;
                        item.DepartmentId = obclass.DEPT_ID;
                        item.Name = obclass.OBS_CLASS;

                        item.InsertUser = obclass.OBS_CLASS_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(obclass.OBS_CLASS_INSERT_DATETIME.ToString());
                        item.UpdateUser = obclass.OBS_CLASS_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(obclass.OBS_CLASS_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(obclass.OBS_CLASS_DELETED.ToString());
                        item.LoadDisplayData();
                        this.ObclassItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] LoadByDepartmentId(Int64 dePartmentId)
        {
            String[] result = { "true", "", "" };

            try
            {
                List<ACTIVITY_TYPES> obclasses = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.ObclassItemsList = new List<ObclassItem>();

                obclasses =
                    tdmapEntities.ACTIVITY_TYPES.Where(
                        lift => lift.OBS_CLASS_DELETED != 1
                            && lift.DEPT_ID == dePartmentId
                    ).OrderBy(lift => lift.OBS_CLASS).ToList<ACTIVITY_TYPES>();

                if (obclasses != null)
                {
                    foreach (ACTIVITY_TYPES obclass in obclasses)
                    {
                        ObclassItem item = new ObclassItem();
                        item.Id = obclass.ACT_TYPE_ID;
                        item.DepartmentId = obclass.DEPT_ID;
                        item.Name = obclass.OBS_CLASS;

                        item.InsertUser = obclass.OBS_CLASS_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(obclass.OBS_CLASS_INSERT_DATETIME.ToString());
                        item.UpdateUser = obclass.OBS_CLASS_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(obclass.OBS_CLASS_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(obclass.OBS_CLASS_DELETED.ToString());
                        item.LoadDisplayData();
                        this.ObclassItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }


    }
}