﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using _2DMap.Utility;


namespace _2DMap.Models
{
    public class DepartmentItem
    {
        
        public Int64 Id { get; set; }
        public Int64 CompanyId { get; set; }
        public String Name { get; set; }
        public string InsertUser { get; set; }
        public Int64 InsertDateTime { get; set; }
        public string UpdateUser { get; set; }
        public Int64 UpdateDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }
        private Dictionary<String, String> DisplayData;
        
        public DepartmentItem()
        {
            this.Id = -1;
            this.CompanyId = -1;
            this.Name = "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
        }

        public DepartmentItem(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.Id = -1;
            this.Name = valuesDict.ContainsKey("DepartmentName") && valuesDict["DepartmentName"] != null ? (string)valuesDict["DepartmentName"] : "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : "";
            String sCompanyId = valuesDict.ContainsKey("CompanyId") && valuesDict["CompanyId"] != null ? (string)valuesDict["CompanyId"] : "-1";

            Int64 iCompanyId = 0;

            Int64.TryParse(sCompanyId, out iCompanyId);


            this.CompanyId = iCompanyId;
        }

        public String[] Create()
        {
            try
            {
                String lang = CommonConfiguration.SiteLanguage();
                CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

                String[] result = { "true", "", "" };
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                DEPARTMENTS dbItem = null;

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Name.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentName");
                }
             
                if (this.CompanyId <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "CompanyId");
                }

             

                if (result[0].Equals("false"))
                    return result;


                if (dbItem != null)
                {
                    String strInvUserName = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_INT_CODE_INVALID", lang), this.Name);
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = strInvUserName;
                }
                else
                {
                    dbItem = new DEPARTMENTS();
                    dbItem.DEPT_NAME = this.Name;
                    
                   
                    dbItem.COMP_ID = this.CompanyId;
                    
                 
                    dbItem.DEPT_DELETED = 0;
                    dbItem.DEPT_INSERT_DATETIME = StringResources.GetLongCurrentDateTime();
                    dbItem.DEPT_INSERT_USER = CommonConfiguration.SessionUserName();
                    dbItem.DEPT_UPDATE_DATE_TIME = 0;
                    dbItem.DEPT_UPDATE_USER = "";

                    tdmapEntities.DEPARTMENTS.AddObject(dbItem);
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] LoadByKey(Int64 Id)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                DEPARTMENTS dbItem = tdmapEntities.DEPARTMENTS.Where(
                    item => item.DEPT_DELETED != 1 && item.DEPT_ID == Id).SingleOrDefault();

                if (dbItem != null)
                {
                    this.Id = dbItem.DEPT_ID;
                    this.CompanyId = dbItem.COMP_ID;
                    this.Name = dbItem.DEPT_NAME;
                    this.InsertUser = dbItem.DEPT_INSERT_USER;
                    this.InsertDateTime = Int64.Parse(dbItem.DEPT_INSERT_DATETIME.ToString());
                    this.UpdateUser = dbItem.DEPT_UPDATE_USER;
                    this.UpdateDateTime = Int64.Parse(dbItem.DEPT_UPDATE_DATE_TIME.ToString());
                    this.Deleted = Int16.Parse(dbItem.DEPT_DELETED.ToString());
                    this.LoadDisplayData();
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang), "DEPARTMENTS", Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] Update(String departmentId, String coMpanyId, DepartmentItem srcObj)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iDepartmentId = -1;
            Int64 iCompanyId = -1;

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (departmentId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentId");
                    return result;
                }

                Int64.TryParse(departmentId, out iDepartmentId);
                if (iDepartmentId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentId");
                    return result;
                }

                if (coMpanyId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "coMpanyId");
                    return result;
                }

                Int64.TryParse(coMpanyId, out iCompanyId);
                if (iCompanyId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "coMpanyId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
           
                DEPARTMENTS dbItem =
                    tdmapEntities.DEPARTMENTS
                        .Where(item => item.DEPT_ID == iDepartmentId && item.DEPT_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.DEPT_NAME = srcObj.Name;
                    dbItem.DEPT_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.DEPT_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "DEPARTMENTS", departmentId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] UpdateStatus(String departmentId, String coMpanyId)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iDepartmentId = -1;
            Int64 iCompanyId = -1;
          
            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (departmentId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentId");
                    return result;
                }

                Int64.TryParse(departmentId, out iDepartmentId);
                if (iDepartmentId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentId");
                    return result;
                }

                if (coMpanyId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "coMpanyId");
                    return result;
                }

                Int64.TryParse(coMpanyId, out iCompanyId);
                if (iCompanyId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "coMpanyId");
                    return result;
                }

              

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                DEPARTMENTS dbItem =
                    tdmapEntities.DEPARTMENTS
                        .Where(item => item.DEPT_ID == iDepartmentId && item.DEPT_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                   
                    dbItem.DEPT_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.DEPT_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "DEPARTMENTS", departmentId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Delete()
        {
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Id <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "liftId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
               DEPARTMENTS dbItem =
                    tdmapEntities.DEPARTMENTS
                        .Where(item => item.DEPT_ID == this.Id && item.DEPT_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.DEPT_DELETED = 1;
                    dbItem.DEPT_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.DEPT_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "DEPARTMENTS", this.Id.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("DepartmentItem.Id", this.Id.ToString());
            param += SessionParamsUtil.createParameter("DepartmentItem.CompanyId", this.CompanyId.ToString());
            param += SessionParamsUtil.createParameter("DepartmentItem.Name", this.Name);
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public void LoadDisplayData()
        {
            DisplayData = new Dictionary<String, String>();
            DisplayData["CompanyId"] = "";
       

            Dictionary<String, String> dataDict = new Dictionary<string, string>();
            CachedConversions.GetCachedKeyValues(HttpContext.Current, CachedConversions.CACHED_DICT_COMPANIES, out dataDict);

            if (dataDict != null && dataDict.ContainsKey(this.CompanyId.ToString()))
                DisplayData["CompanyId"] = dataDict[this.CompanyId.ToString()];
            else
                DisplayData["CompanyId"] = this.CompanyId.ToString();
        }

        public String GetDisplayData(String itemKey, String lang)
        {
            if (itemKey.Equals("CompanyId") && DisplayData != null && DisplayData.ContainsKey("CompanyId"))
                return DisplayData["CompanyId"];

            return "";
        }
    }
}