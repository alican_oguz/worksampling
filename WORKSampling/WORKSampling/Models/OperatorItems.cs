﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class OperatorItems
    {
        
        public List<OperatorItem> OperatorItemsList { get; set; }

        public OperatorItems()
        {
            this.OperatorItemsList = new List<OperatorItem>();
        }

        public String[] Load()
        {
            String[] result = { "true", "", "" };

            try
            {
                List<OPERATORS> oPerators = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.OperatorItemsList = new List<OperatorItem>();

                oPerators = 
                    tdmapEntities.OPERATORS.Where(
                        operatorr => operatorr.OPERATOR_DELETED != 1
                    ).OrderBy(operatorr => operatorr.OPERATOR_NAME).ToList<OPERATORS>();

                if (oPerators != null)
                {
                    foreach (OPERATORS oPerator in oPerators)
                    {
                        OperatorItem item = new OperatorItem();
                        item.Id = oPerator.OPERATOR_ID;
                        item.Name = oPerator.OPERATOR_NAME;
                        item.Pwd = oPerator.OPERETOR_PWD;
                        item.Email = oPerator.OPERATOR_EMAIL;
                        item.Username = oPerator.OPERATOR_USR_NAME;
                        item.InsertUser = oPerator.OPERATOR_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(oPerator.OPERATOR_INSERT_DATETIME.ToString());
                        item.UpdateUser = oPerator.OPERATOR_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(oPerator.OPERATOR_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(oPerator.OPERATOR_DELETED.ToString());
                        this.OperatorItemsList.Add(item);
                    }
                }

            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

    }
}