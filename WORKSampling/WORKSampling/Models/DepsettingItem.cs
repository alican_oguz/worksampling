﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class DepsettingItem
    {

        public Int64 Id { get; set; }
        public Int64 DepartmentId { get; set; }
        public Int64 MinObserNumber { get; set; }
        public string StartShiftTime { get; set; }
        public string EndShiftTime { get; set; }
        public string StartBreakTime { get; set; }
        public string EndBreakTime { get; set; }
        public string InsertUser { get; set; }
        public Int64 InsertDateTime { get; set; }
        public string UpdateUser { get; set; }
        public Int64 UpdateDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }
        private Dictionary<String, String> DisplayData;

        public DepsettingItem()
        {
            this.Id = -1;
            this.DepartmentId = -1;
            this.MinObserNumber = 0;
            this.StartShiftTime = "";
            this.EndShiftTime = "";
            this.StartBreakTime = "";
            this.EndBreakTime = "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
        }

        public DepsettingItem(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.Id = -1;
            this.MinObserNumber = valuesDict.ContainsKey("MinObserNumber") && valuesDict["MinObserNumber"] != null ? Int64.Parse((string)valuesDict["MinObserNumber"]) : 0;
            String ssTime = valuesDict.ContainsKey("StartShiftTime") && valuesDict["StartShiftTime"] != null ? (string)valuesDict["StartShiftTime"] : "";
            ssTime = ssTime.Replace(":", "");   
            this.StartShiftTime = ssTime;
            String esTime = valuesDict.ContainsKey("EndShiftTime") && valuesDict["EndShiftTime"] != null ? (string)valuesDict["EndShiftTime"] : "";
            esTime = esTime.Replace(":", "");
            this.EndShiftTime = esTime;
            String sbTime = valuesDict.ContainsKey("StartBreakTime") && valuesDict["StartBreakTime"] != null ? (string)valuesDict["StartBreakTime"] : "";
            sbTime = sbTime.Replace(":", "");
            this.StartBreakTime = sbTime;
            String ebTime = valuesDict.ContainsKey("EndBreakTime") && valuesDict["EndBreakTime"] != null ? (string)valuesDict["EndBreakTime"] : "";
            ebTime = ebTime.Replace(":", "");
            this.EndBreakTime = ebTime;
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : "";



            String sDepartmentId = valuesDict.ContainsKey("DepartmentId") && valuesDict["DepartmentId"] != null ? (string)valuesDict["DepartmentId"] : "-1";
            Int64 iDepartmentId = 0;
            Int64.TryParse(sDepartmentId, out iDepartmentId);

            this.DepartmentId = iDepartmentId;
        }

        public String[] Create()
        {
            try
            {
                String lang = CommonConfiguration.SiteLanguage();
                CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

                String[] result = { "true", "", "" };
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                DEPSETTINGS dbItem = null;

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.MinObserNumber.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "MinObserNumber");
                }
                if (this.StartShiftTime.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "StartShiftTime");
                }
                if (this.EndShiftTime.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "EndShiftTime");
                }
                if (this.StartBreakTime.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "StartBreakTime");
                }
                if (this.EndBreakTime.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "EndBreakTime");
                }
                if (this.DepartmentId <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentId");
                }



                if (result[0].Equals("false"))
                    return result;

                if (dbItem != null)
                {
                    String sInvIntCode = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_INT_CODE_INVALID", lang), this.MinObserNumber);
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = sInvIntCode;
                }
                else
                {
                    dbItem = new DEPSETTINGS();
                    dbItem.MIN_OBSERVATION_NUMBER = this.MinObserNumber;
                    dbItem.START_SHIFT_TIME = this.StartShiftTime;
                    dbItem.END_SHIFT_TIME = this.EndShiftTime;
                    dbItem.START_BREAK_TIME = this.StartBreakTime;
                    dbItem.END_BREAK_TIME = this.EndBreakTime;
                    dbItem.DEPT_ID = this.DepartmentId;

                    dbItem.DEPSETTINGS_DELETED = 0;
                    dbItem.DEPSETTINGS_INSERT_DATETIME = StringResources.GetLongCurrentDateTime();
                    dbItem.DEPSETTINGS_INSERT_USER = CommonConfiguration.SessionUserName();
                    dbItem.DEPSETTINGS_UPDATE_DATE_TIME = 0;
                    dbItem.DEPSETTINGS_UPDATE_USER = "";

                    tdmapEntities.DEPSETTINGS.AddObject(dbItem);
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPSETTINGS);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] LoadByKey(Int64 Id)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                DEPSETTINGS dbItem = tdmapEntities.DEPSETTINGS.Where(
                    item => item.DEPSETTINGS_DELETED != 1 && item.DEPSETTINGS_ID == Id).SingleOrDefault();

                if (dbItem != null)
                {
                    this.Id = dbItem.DEPSETTINGS_ID;
                    this.DepartmentId = dbItem.DEPT_ID;
                    this.MinObserNumber = Int64.Parse(dbItem.MIN_OBSERVATION_NUMBER.ToString());
                    this.StartShiftTime = dbItem.START_SHIFT_TIME;
                    this.EndShiftTime = dbItem.END_SHIFT_TIME;
                    this.StartBreakTime = dbItem.START_BREAK_TIME;
                    this.EndBreakTime = dbItem.END_BREAK_TIME;
                    this.InsertUser = dbItem.DEPSETTINGS_INSERT_USER;
                    this.InsertDateTime = Int64.Parse(dbItem.DEPSETTINGS_INSERT_DATETIME.ToString());
                    this.UpdateUser = dbItem.DEPSETTINGS_UPDATE_USER;
                    this.UpdateDateTime = Int64.Parse(dbItem.DEPSETTINGS_UPDATE_DATE_TIME.ToString());
                    this.Deleted = Int16.Parse(dbItem.DEPSETTINGS_DELETED.ToString());
                    this.LoadDisplayData();
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang), "DEPSETTINGS", Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] Update(String depsettingId, String dePartmentId, DepsettingItem srcObj)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iDepsettingId = -1;
            Int64 iDepartmentId = -1;

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (depsettingId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepsettingId");
                    return result;
                }

                Int64.TryParse(depsettingId, out iDepsettingId);
                if (iDepsettingId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepsettingId");
                    return result;
                }

                if (dePartmentId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                Int64.TryParse(dePartmentId, out iDepartmentId);
                if (iDepartmentId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();


                DEPSETTINGS dbItem =
                    tdmapEntities.DEPSETTINGS
                        .Where(item => item.DEPSETTINGS_ID == iDepsettingId && item.DEPSETTINGS_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.MIN_OBSERVATION_NUMBER = srcObj.MinObserNumber;
                    dbItem.START_SHIFT_TIME = srcObj.StartShiftTime;
                    dbItem.END_SHIFT_TIME = srcObj.EndShiftTime;
                    dbItem.START_BREAK_TIME = srcObj.StartBreakTime;
                    dbItem.END_BREAK_TIME = srcObj.EndBreakTime;
                    dbItem.DEPT_ID = srcObj.DepartmentId;
                    dbItem.DEPSETTINGS_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.DEPSETTINGS_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPSETTINGS);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "SLOPES", depsettingId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] UpdateStatus(String depsettingId, String dePartmentId, String dataStatus)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iDepsettingId = -1;
            Int64 iDepartmentId = -1;
            Int16 iStatus = -1;
            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (depsettingId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "SlopeId");
                    return result;
                }

                Int64.TryParse(depsettingId, out iDepsettingId);
                if (iDepsettingId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "SlopeId");
                    return result;
                }

                if (dePartmentId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                Int64.TryParse(dePartmentId, out iDepartmentId);
                if (iDepartmentId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                Int16.TryParse(dataStatus, out iStatus);
                if (iStatus < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dataStatus");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                DEPSETTINGS dbItem =
                    tdmapEntities.DEPSETTINGS
                        .Where(item => item.DEPSETTINGS_ID == iDepsettingId && item.DEPSETTINGS_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {


                    dbItem.DEPSETTINGS_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.DEPSETTINGS_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPSETTINGS);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "WORKERS", depsettingId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Delete()
        {
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Id <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "liftId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                DEPSETTINGS dbItem =
                    tdmapEntities.DEPSETTINGS
                        .Where(item => item.DEPSETTINGS_ID == this.Id && item.DEPSETTINGS_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.DEPSETTINGS_DELETED = 1;
                    dbItem.DEPSETTINGS_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.DEPSETTINGS_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPSETTINGS);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "DEPSETTINGS", this.Id.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("DepsettingItem.Id", this.Id.ToString());
            param += SessionParamsUtil.createParameter("DepsettingItem.DepartmentId", this.DepartmentId.ToString());
            param += SessionParamsUtil.createParameter("DepsettingItem.MinObserNumber", this.MinObserNumber.ToString());
            param += SessionParamsUtil.createParameter("DepsettingItem.StartShiftTime", this.StartShiftTime.ToString());
            param += SessionParamsUtil.createParameter("DepsettingItem.EndShiftTime", this.EndShiftTime.ToString());
            param += SessionParamsUtil.createParameter("DepsettingItem.StartBreakTime", this.StartBreakTime.ToString());
            param += SessionParamsUtil.createParameter("DepsettingItem.EndBreakTime", this.EndBreakTime.ToString());
            
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public void LoadDisplayData()
        {
            DisplayData = new Dictionary<String, String>();
            DisplayData["DepartmentId"] = "";

            Dictionary<String, String> dataDict = new Dictionary<string, string>();
            CachedConversions.GetCachedKeyValues(HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dataDict);

            if (dataDict != null && dataDict.ContainsKey(this.DepartmentId.ToString()))
                DisplayData["DepartmentId"] = dataDict[this.DepartmentId.ToString()];
            else
                DisplayData["DepartmentId"] = this.DepartmentId.ToString();
        }

        public String GetDisplayData(String itemKey, String lang)
        {
            if (itemKey.Equals("DepartmentId") && DisplayData != null && DisplayData.ContainsKey("DepartmentId"))
                return DisplayData["DepartmentId"];

            return "";
        }
    }
}