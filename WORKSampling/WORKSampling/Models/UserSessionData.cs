﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class UserSessionData
    {
        public string Username { get; set; }
        public string Name { get; set; }
        public string MidName { get; set; }
        public string Surname { get; set; }
        public string Email { get; set; }
        public string ProfileImage { get; set; }
        public string Status { get; set; }
        public string AuthToken { get; set; }

        public static string GetUserStatusDisp(String userStatus)
        {
            String lang = CommonConfiguration.SiteLanguage();
            if (userStatus.Equals(USER_STATUS_ACTIVE))
            {
                return StringResources.getString(StringResources.RES_LABEL, "LBL_ACTIVE", lang);
            }
            else if (userStatus.Equals(USER_STATUS_BLOCKED))
            {
                return StringResources.getString(StringResources.RES_LABEL, "LBL_USER_BLOCKED", lang);
            }
            else if (userStatus.Equals(USER_STATUS_PWD_BLOCKED))
            {
                return StringResources.getString(StringResources.RES_LABEL, "LBL_USER_PWD_BLOCKED", lang);
            }
            else if (userStatus.Equals(USER_STATUS_MUST_CHANGE_PWD))
            {
                return StringResources.getString(StringResources.RES_LABEL, "LBL_USER_MUST_CHANGE_PWD", lang);
            }
            else
            {
                return "N/A";
            }
        }

        public static string USER_STATUS_ACTIVE = "1";
        public static string USER_STATUS_BLOCKED = "2";
        public static string USER_STATUS_PWD_BLOCKED = "3";
        public static string USER_STATUS_MUST_CHANGE_PWD = "4";

        public UserSessionData()
        {
            this.Username = "";
            this.Name = "";
            this.MidName = "";
            this.Status = "";
            this.Surname = "";
            this.Email = "";
            this.AuthToken = "";
            this.ProfileImage = "";
        }
    }


}