﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class TestObservationItem
    {

        public Int64 Id { get; set; }
        public Int64 DepartmentId { get; set; }
        public String Name { get; set; }
        public Int64 StartDateTime { get; set; }
        public Int64 Interval { get; set; }
        public Int64 SampleCount { get; set; }
        public string InsertUser { get; set; }
        public Int64 InsertDateTime { get; set; }
        public string UpdateUser { get; set; }
        public Int64 UpdateDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }
        private Dictionary<String, String> DisplayData;

        public TestObservationItem()
        {
            this.Id = -1;
            this.DepartmentId = -1;
            this.StartDateTime = -1;
            this.Interval = -1;
            this.Name = "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
            this.SampleCount = 0;
        }

        public TestObservationItem(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.Id = -1;
            this.Name = valuesDict.ContainsKey("ObservationName") && valuesDict["ObservationName"] != null ? (string)valuesDict["ObservationName"] : "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : "";



            String sDepartmentId = valuesDict.ContainsKey("DepartmentId") && valuesDict["DepartmentId"] != null ? (string)valuesDict["DepartmentId"] : "-1";
            Int64 iDepartmentId = 0;
            Int64.TryParse(sDepartmentId, out iDepartmentId);

            this.DepartmentId = iDepartmentId;

            String sPStartDate = valuesDict.ContainsKey("ProcessStartDate") && valuesDict["ProcessStartDate"] != null ? (string)valuesDict["ProcessStartDate"] : "-1";
            Int64 lPSDate = 0;
            Int64.TryParse(sPStartDate, out lPSDate);

            this.StartDateTime = lPSDate;

            String sInterval = valuesDict.ContainsKey("ProcessInterval") && valuesDict["ProcessInterval"] != null ? (string)valuesDict["ProcessInterval"] : "-1";
            Int64 lInterval = 0;
            Int64.TryParse(sInterval, out lInterval);

            this.Interval = lInterval;

            String sSampleCount = valuesDict.ContainsKey("SampleCount") && valuesDict["SampleCount"] != null ? (string)valuesDict["SampleCount"] : "-1";
            Int64 lSmapleCount = 0;
            Int64.TryParse(sSampleCount, out lSmapleCount);

            this.SampleCount = lSmapleCount;

        }

        public TestObservationItem(OBSERVATION dbObj)
        {
            this.Id = (Int64)dbObj.ID;
            this.DepartmentId = (Int64)dbObj.DEPT_ID;
            this.StartDateTime = (Int64)dbObj.STARTDATETIME;
            this.Interval = (Int64)dbObj.INTERVAL;
            this.Name = dbObj.NAME;
            this.InsertUser = dbObj.INSERT_USER;
            this.InsertDateTime = (Int64)dbObj.INSERT_DATETIME;
            this.UpdateUser = dbObj.UPDATE_USER;
            this.UpdateDateTime = (Int64)dbObj.UPDATE_DATE_TIME;
            this.Deleted = (Int16)dbObj.DELETED;
            this.SessionParam = "";
            this.SampleCount = (Int64)dbObj.SAMPLE_COUNT;
        }

        public String[] Create()
        {
            try
            {
                String lang = CommonConfiguration.SiteLanguage();
                CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

                String[] result = { "true", "", "" };
                WorkSamplingEntities dbEntities = new WorkSamplingEntities();
                OBSERVATION dbItem = null;

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Name.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationName");
                }

                if (this.DepartmentId <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentId");
                }

                if (result[0].Equals("false"))
                    return result;

                dbItem = new OBSERVATION();
                dbItem.NAME = this.Name;
                dbItem.DEPT_ID = this.DepartmentId;
                dbItem.STARTDATETIME = this.StartDateTime;
                dbItem.INTERVAL = this.Interval;
                dbItem.SAMPLE_COUNT = this.SampleCount;
                dbItem.DELETED = 0;
                dbItem.INSERT_DATETIME = StringResources.GetLongCurrentDateTime();
                dbItem.INSERT_USER = CommonConfiguration.SessionUserName();
                dbItem.UPDATE_DATE_TIME = 0;
                dbItem.UPDATE_USER = "";

                dbEntities.OBSERVATION.AddObject(dbItem);
                dbEntities.SaveChanges();
                this.CreateSampleData(dbItem.ID);
                result[0] = "true";
                result[1] = "";
                result[2] = "";


                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] CreateSampleData(Int64 observationId)
        {
            try
            {
                String lang = CommonConfiguration.SiteLanguage();
                CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

                String[] result = { "true", "", "" };
                WorkSamplingEntities dbEntities = new WorkSamplingEntities();
                OBSERVATION_DATA dbItem = null;

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Name.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationName");
                }

                if (this.DepartmentId <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentId");
                }

                if (result[0].Equals("false"))
                    return result;

                WorkerItems itemSet = new WorkerItems();
                itemSet.LoadByDepartmentId(this.DepartmentId);
                List<WorkerItem> itemList = itemSet.WorkerItemsList;
                Int64 interval = 0;
                Int64 checkTime = this.StartDateTime + interval;
                Int64 willBeCheckDateTime = Int64.Parse("20160511" + checkTime.ToString());
                for (int i = 0; i < this.SampleCount; i++)
                {
                    Random random = new Random();
                    Int64 randVal = random.Next(1, itemList.Count);
                    WorkerItem workerItem = itemList.Where(item => item.Id == randVal).SingleOrDefault();
                   
                    if (workerItem != null)
                    {

                        Int64 startDateTime = CheckTime(willBeCheckDateTime);
                        dbItem = new OBSERVATION_DATA();
                        dbItem.WORKER_ID = randVal;
                        dbItem.OBSERVATION_ID = observationId;
                        dbItem.STARTDATETIME = startDateTime;
                        dbItem.DELETED = 0;
                        dbItem.INSERT_DATETIME = StringResources.GetLongCurrentDateTime();
                        dbItem.UPDATE_DATE_TIME = 0;
                        dbItem.UPDATE_USER = "";
                        dbItem.PARAMETER = 0;
                        dbEntities.OBSERVATION_DATA.AddObject(dbItem);
                        String sNext = startDateTime.ToString().Substring(8, 6);
                        Int64 lNext = Int64.Parse(sNext);
                        lNext += this.Interval;
                        sNext = "20160511" + lNext.ToString();
                        willBeCheckDateTime = Int64.Parse(sNext);

                        dbEntities.SaveChanges();
                    }

                }
 
                result[0] = "true";
                result[1] = "";
                result[2] = "";


                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public Int64 CheckTime(Int64 time)
        {
            Int64 startDateTime = 0;
            String sTime = time.ToString();
            String sHour = sTime.Substring(8, 2);
            String sMin = sTime.Substring(10, 2);
            String sSec = sTime.Substring(12, 2);
            Int32 iMin = Int32.Parse(sMin);
            Int32 iHour = Int32.Parse(sHour);
            Int32 iSec = Int32.Parse(sSec);

            if (iSec >= 60)
            {
                iSec = iSec - 60;
                iMin++;
            }

            if (iMin >= 60)
            {
                iMin = iMin - 60;
                iHour++;
            }
            if (iHour >= 24)
            {
                iHour = iHour - 24;
            }

            if (iSec < 10)
            {
                sSec = "0" + iSec.ToString();
            }
            else
            {
                sSec = iSec.ToString();
            }
            if (iHour < 10)
            {
                sHour = "0" + iHour.ToString();
            }
            else
            {
                sHour = iHour.ToString();
            }
            if (iMin < 10)
            {
                sMin = "0" + iMin.ToString();
            }
            else
            {
                sMin = iMin.ToString();
            }

            String sStartDateTime = "20160505" + sHour + sMin + sSec;
            Int64.TryParse(sStartDateTime, out startDateTime);
            return startDateTime;
        
        }

        public String[] LoadByKey(Int64 Id)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                OBSERVATION dbItem = tdmapEntities.OBSERVATION.Where(
                    item => item.DELETED != 1 && item.ID == Id).SingleOrDefault();

                if (dbItem != null)
                {
                    new TestObservationItem(dbItem);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang), "ACTIVITIES", Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] Delete()
        {
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Id <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                OBSERVATION dbItem =
                    tdmapEntities.OBSERVATION
                        .Where(item => item.ID == this.Id && item.DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {
                    dbItem.UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.UPDATE_USER = CommonConfiguration.SessionUserName();
                    dbItem.DELETED = 1;
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "ACTIVITIES", this.Id.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Load(out List<TestObservationItem> resultList)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();
            resultList = new List<TestObservationItem>();
            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                List<OBSERVATION> dbItem = tdmapEntities.OBSERVATION.Where(
                    item => item.DELETED != 1).ToList<OBSERVATION>();

                if (dbItem != null)
                {
                    foreach (var item in dbItem)
                    {
                        resultList.Add(new TestObservationItem(item));
                    }

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang), "ACTIVITIES", Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] LoadData(Int64 ObservationId, out List<TestObservationDataItem> resultList)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();
            resultList = new List<TestObservationDataItem>();
            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                OBSERVATION dbItemObs = tdmapEntities.OBSERVATION.Where(
                    item => item.DELETED != 1 && item.ID == ObservationId).SingleOrDefault();

                List<OBSERVATION_DATA> dbItem = tdmapEntities.OBSERVATION_DATA.Where(
                    item => item.PARAMETER == 0 && item.OBSERVATION_ID == ObservationId).OrderBy(item=>item.ID).ToList<OBSERVATION_DATA>();

                if (dbItem != null)
                {
                    foreach (var item in dbItem)
                    {
                        TestObservationDataItem testObj = new TestObservationDataItem();
                        testObj.Id = item.ID;
                        testObj.ObservationId = item.OBSERVATION_ID;
                        testObj.StartDateTime = item.STARTDATETIME;
                        testObj.ObservationName = dbItemObs.NAME;
                        resultList.Add(testObj);
                    }

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang), "ACTIVITIES", Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("TestObservationItem.Id", this.Id.ToString());
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public void LoadDisplayData()
        {
            DisplayData = new Dictionary<String, String>();
            DisplayData["DepartmentId"] = "";

            Dictionary<String, String> dataDict = new Dictionary<string, string>();
            CachedConversions.GetCachedKeyValues(HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dataDict);

            if (dataDict != null && dataDict.ContainsKey(this.DepartmentId.ToString()))
                DisplayData["DepartmentId"] = dataDict[this.DepartmentId.ToString()];
            else
                DisplayData["DepartmentId"] = this.DepartmentId.ToString();

        }

        public String GetDisplayData(String itemKey, String lang)
        {
            if (itemKey.Equals("DepartmentId") && DisplayData != null && DisplayData.ContainsKey("DepartmentId"))
                return DisplayData["DepartmentId"];

            return "";
        }
    }
}