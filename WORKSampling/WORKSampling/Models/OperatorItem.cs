﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class OperatorItem
    {
        
        public Int64 Id { get; set; }
        public String Name { get; set; }
        public String Pwd { get; set; }
        public String Email { get; set; }
        public String Username { get; set; }
        public string InsertUser { get; set; }
        public Int64 InsertDateTime { get; set; }
        public string UpdateUser { get; set; }
        public Int64 UpdateDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }

        public OperatorItem()
        {
            this.Id = -1;
            this.Name = "";
            this.Pwd = "";
            this.Email = "";
            this.Username = "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
        }

        public OperatorItem(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.Id = -1;
            this.Name = valuesDict.ContainsKey("OperatorName") && valuesDict["OperatorName"] != null ? (string)valuesDict["OperatorName"] : "";
            this.Pwd = valuesDict.ContainsKey("OperatorPwd") && valuesDict["OperatorPwd"] != null ? (string)valuesDict["OperatorPwd"] : ""; ;
            this.Email = valuesDict.ContainsKey("OperatorEmail") && valuesDict["OperatorEmail"] != null ? (string)valuesDict["OperatorEmail"] : "";
            this.Username = valuesDict.ContainsKey("OperatorUsername") && valuesDict["OperatorUsername"] != null ? (string)valuesDict["OperatorUsername"] : "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : ""; ;
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("OperatorItem.Id", this.Id.ToString());
            param += SessionParamsUtil.createParameter("OperatorItem.Name", this.Name);
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public String[] Create()
        {
            try
            {
                String lang = CommonConfiguration.SiteLanguage();
                CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

                String[] result = { "true", "", "" };
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                OPERATORS dbItem = null;

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Name.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "Name");
                }
                if (result[0].Equals("false"))
                    return result;


                if (dbItem != null)
                {
                    String strInvUserName = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_INT_CODE_INVALID", lang), this.Name);
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = strInvUserName;
                }
                else
                {
                    dbItem = new OPERATORS();
                    dbItem.OPERATOR_NAME = this.Name;
                    dbItem.OPERETOR_PWD = this.Pwd;
                    dbItem.OPERATOR_EMAIL = this.Email;
                    dbItem.OPERATOR_USR_NAME = this.Username;
                    dbItem.OPERATOR_DELETED = 0;
                    dbItem.OPERATOR_INSERT_DATETIME = StringResources.GetLongCurrentDateTime();
                    dbItem.OPERATOR_INSERT_USER = CommonConfiguration.SessionUserName();
                    dbItem.OPERATOR_UPDATE_DATE_TIME = 0;
                    dbItem.OPERATOR_UPDATE_USER = "";

                    tdmapEntities.OPERATORS.AddObject(dbItem);
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OPERATORS);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] LoadByKey(Int64 Id)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                OPERATORS dbItem = tdmapEntities.OPERATORS.Where(
                    item => item.OPERATOR_DELETED != 1 && item.OPERATOR_ID == Id).SingleOrDefault();

                if (dbItem != null)
                {
                    this.Id = dbItem.OPERATOR_ID;
                    this.Name = dbItem.OPERATOR_NAME;
                    this.Pwd = dbItem.OPERETOR_PWD;
                    this.Email = dbItem.OPERATOR_EMAIL;
                    this.Username = dbItem.OPERATOR_USR_NAME;
                    this.InsertUser = dbItem.OPERATOR_INSERT_USER;
                    this.InsertDateTime = Int64.Parse(dbItem.OPERATOR_INSERT_DATETIME.ToString());
                    this.UpdateUser = dbItem.OPERATOR_UPDATE_USER;
                    this.UpdateDateTime = Int64.Parse(dbItem.OPERATOR_UPDATE_DATE_TIME.ToString());
                    this.Deleted = Int16.Parse(dbItem.OPERATOR_DELETED.ToString());
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang), "OPERATORS", Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] Update(String oPeratorId, OperatorItem srcObj)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iOperatorId = -1;

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (oPeratorId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "oPeratorId");
                    return result;
                }

                Int64.TryParse(oPeratorId, out iOperatorId);
                if ( iOperatorId < 0 )
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "oPeratorId");
                    return result;
                }


                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
              
                OPERATORS dbItem =
                    tdmapEntities.OPERATORS
                        .Where(item => item.OPERATOR_ID == iOperatorId && item.OPERATOR_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.OPERATOR_NAME = srcObj.Name;
                    dbItem.OPERETOR_PWD = srcObj.Pwd;
                    dbItem.OPERATOR_EMAIL = srcObj.Email;
                    dbItem.OPERATOR_USR_NAME = srcObj.Username;
                    dbItem.OPERATOR_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.OPERATOR_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OPERATORS);

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "OPERATORS", oPeratorId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Delete()
        {
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Id <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "skiCenterId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                OPERATORS dbItem =
                    tdmapEntities.OPERATORS
                        .Where(item => item.OPERATOR_ID == this.Id && item.OPERATOR_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.OPERATOR_DELETED = 1;
                    dbItem.OPERATOR_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.OPERATOR_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OPERATORS);

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "SKI_CENTERS", this.Id.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }
    }
}