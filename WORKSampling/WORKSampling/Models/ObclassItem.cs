﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class ObclassItem
    {
        
        public Int64 Id { get; set; }
        public Int64 DepartmentId { get; set; }
        public String Name { get; set; }
        public string InsertUser { get; set; }
        public Int64 InsertDateTime { get; set; }
        public string UpdateUser { get; set; }
        public Int64 UpdateDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }
        private Dictionary<String, String> DisplayData;
        
        public ObclassItem()
        {
            this.Id = -1;
            this.DepartmentId = -1;
            this.Name = "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
        }

        public ObclassItem(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.Id = -1;
            this.Name = valuesDict.ContainsKey("ObclassName") && valuesDict["ObclassName"] != null ? (string)valuesDict["ObclassName"] : "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : "";



            String sDepartmentId = valuesDict.ContainsKey("DepartmentId") && valuesDict["DepartmentId"] != null ? (string)valuesDict["DepartmentId"] : "-1";
            Int64 iDepartmentId = 0;
            Int64.TryParse(sDepartmentId, out iDepartmentId);

            this.DepartmentId = iDepartmentId;
        }

        public String[] Create()
        {
            try
            {
                String lang = CommonConfiguration.SiteLanguage();
                CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

                String[] result = { "true", "", "" };
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                ACTIVITY_TYPES dbItem = null;

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Name.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObclassName");
                }
                
                if (this.DepartmentId <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentId");
                }

               

                if (result[0].Equals("false"))
                    return result;

                if (dbItem != null)
                {
                    String sInvIntCode = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_INT_CODE_INVALID", lang), this.Name);
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = sInvIntCode;
                }
                else
                {
                    dbItem = new ACTIVITY_TYPES();
                    dbItem.OBS_CLASS = this.Name;
                    dbItem.DEPT_ID = this.DepartmentId;

                    dbItem.OBS_CLASS_DELETED = 0;
                    dbItem.OBS_CLASS_INSERT_DATETIME= StringResources.GetLongCurrentDateTime();
                    dbItem.OBS_CLASS_INSERT_USER = CommonConfiguration.SessionUserName();
                    dbItem.OBS_CLASS_UPDATE_DATE_TIME= 0;
                    dbItem.OBS_CLASS_UPDATE_USER = "";
                 

                    tdmapEntities.ACTIVITY_TYPES.AddObject(dbItem);
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_WORKERS);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] LoadByKey(Int64 Id)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                ACTIVITY_TYPES dbItem = tdmapEntities.ACTIVITY_TYPES.Where(
                    item => item.OBS_CLASS_DELETED != 1 && item.ACT_TYPE_ID == Id).SingleOrDefault();

                if (dbItem != null)
                {
                    this.Id = dbItem.ACT_TYPE_ID;
                    this.DepartmentId = dbItem.DEPT_ID;
                    this.Name = dbItem.OBS_CLASS;
                    this.InsertUser = dbItem.OBS_CLASS_INSERT_USER;
                    this.InsertDateTime = Int64.Parse(dbItem.OBS_CLASS_INSERT_DATETIME.ToString());
                    this.UpdateUser = dbItem.OBS_CLASS_UPDATE_USER;
                    this.UpdateDateTime = Int64.Parse(dbItem.OBS_CLASS_UPDATE_DATE_TIME.ToString());
                    this.Deleted = Int16.Parse(dbItem.OBS_CLASS_DELETED.ToString());
                    this.LoadDisplayData();
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang), "WORKERS", Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] Update(String obclassId, String dePartmentId, ObclassItem srcObj)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iObclassId = -1;
            Int64 iDepartmentId = -1;

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (obclassId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObclassId");
                    return result;
                }

                Int64.TryParse(obclassId, out iObclassId);
                if (iObclassId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObclassId");
                    return result;
                }

                if (dePartmentId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                Int64.TryParse(dePartmentId, out iDepartmentId);
                if (iDepartmentId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
              

                ACTIVITY_TYPES dbItem =
                    tdmapEntities.ACTIVITY_TYPES
                        .Where(item => item.ACT_TYPE_ID == iObclassId && item.OBS_CLASS_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.OBS_CLASS = srcObj.Name;
                   
                    dbItem.DEPT_ID = srcObj.DepartmentId;
                    dbItem.OBS_CLASS_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.OBS_CLASS_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OBCLASS);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "SLOPES", obclassId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] UpdateStatus(String obclassId, String dePartmentId, String dataStatus)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iObclassId = -1;
            Int64 iDepartmentId = -1;
            Int16 iStatus = -1;
            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (obclassId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObclassId");
                    return result;
                }

                Int64.TryParse(obclassId, out iObclassId);
                if (iObclassId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObclassId");
                    return result;
                }

                if (dePartmentId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                Int64.TryParse(dePartmentId, out iDepartmentId);
                if (iDepartmentId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                Int16.TryParse(dataStatus, out iStatus);
                if (iStatus < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dataStatus");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                ACTIVITY_TYPES dbItem =
                    tdmapEntities.ACTIVITY_TYPES
                        .Where(item => item.ACT_TYPE_ID == iObclassId && item.OBS_CLASS_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {
                    dbItem.OBS_CLASS_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.OBS_CLASS_UPDATE_USER= CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OBCLASS);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "WORKERS", obclassId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Delete()
        {
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Id <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "liftId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                ACTIVITY_TYPES dbItem =
                    tdmapEntities.ACTIVITY_TYPES
                        .Where(item => item.ACT_TYPE_ID == this.Id && item.OBS_CLASS_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {
                    dbItem.OBS_CLASS_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.OBS_CLASS_UPDATE_USER = CommonConfiguration.SessionUserName();
                    dbItem.OBS_CLASS_DELETED = 1;
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OBCLASS);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "ACTIVITY_TYPES", this.Id.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("ObclassItem.Id", this.Id.ToString());
            param += SessionParamsUtil.createParameter("ObclassItem.DepartmentId", this.DepartmentId.ToString());
            param += SessionParamsUtil.createParameter("ObclassItem.Name", this.Name);
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public void LoadDisplayData()
        {
            DisplayData = new Dictionary<String, String>();
            DisplayData["DepartmentId"] = "";
          
            Dictionary<String, String> dataDict = new Dictionary<string, string>();
            CachedConversions.GetCachedKeyValues(HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dataDict);

            if (dataDict != null && dataDict.ContainsKey(this.DepartmentId.ToString()))
                DisplayData["DepartmentId"] = dataDict[this.DepartmentId.ToString()];
            else
                DisplayData["DepartmentId"] = this.DepartmentId.ToString();
        }

        public String GetDisplayData(String itemKey, String lang)
        {
            if (itemKey.Equals("DepartmentId") && DisplayData != null && DisplayData.ContainsKey("DepartmentId"))
                return DisplayData["DepartmentId"];

            return "";
        }
    }
}