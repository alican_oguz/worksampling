﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class ParameterItem
    {
        public Int64 Id { get; set; }
        public Int64 DeptId { get; set; }
        public String Parameter { get; set; }
        public string InsertUser { get; set; }
        public Int64 InsertDateTime { get; set; }
        public string UpdateUser { get; set; }
        public Int64 UpdateDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }
        private Dictionary<String, String> DisplayData;

        public ParameterItem()
        {
            this.Id = -1;
            this.DeptId = -1;
            this.Parameter = "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
            this.DisplayData = new Dictionary<string, string>();
        }

        public ParameterItem(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.Id = -1;
            this.Parameter = valuesDict.ContainsKey("Parameter") && valuesDict["Parameter"] != null ? (string)valuesDict["Parameter"] : "";
            this.DeptId = valuesDict.ContainsKey("DeptId") && valuesDict["DeptId"] != null ? Int64.Parse(valuesDict["DeptId"].ToString()) : -1;
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : ""; ;
            this.DisplayData = new Dictionary<string, string>();
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("ParameterItem.Id", this.Id.ToString());
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public String[] Create()
        {
            try
            {
                String lang = CommonConfiguration.SiteLanguage();
                CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

                String[] result = { "true", "", "" };
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                PARAMETERS dbItem = null;

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Parameter.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "Parameter");
                }
                if (result[0].Equals("false"))
                    return result;

                dbItem = new PARAMETERS();
                dbItem.PARAMETER = this.Parameter;
                dbItem.DEPT_ID = this.DeptId;
                dbItem.PARAMETER_DELETED = 0;
                dbItem.PARAMETER_INSERT_DATETIME = StringResources.GetLongCurrentDateTime();
                dbItem.PARAMETER_INSERT_USER = CommonConfiguration.SessionUserName();
                dbItem.PARAMETER_UPDATE_DATE_TIME = 0;
                dbItem.PARAMETER_UPDATE_USER = "";

                tdmapEntities.PARAMETERS.AddObject(dbItem);
                tdmapEntities.SaveChanges();

                result[0] = "true";
                result[1] = "";
                result[2] = "";

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] LoadByKey(Int64 Id)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                PARAMETERS dbItem = tdmapEntities.PARAMETERS.Where(
                    item => item.PARAMETER_DELETED != 1 && item.PARAMETER_ID == Id).SingleOrDefault();

                if (dbItem != null)
                {
                    this.Id = dbItem.PARAMETER_ID;
                    this.Parameter = dbItem.PARAMETER;
                    this.DeptId = dbItem.DEPT_ID;
                    this.InsertUser = dbItem.PARAMETER_INSERT_USER;
                    this.InsertDateTime = Int64.Parse(dbItem.PARAMETER_INSERT_DATETIME.ToString());
                    this.UpdateUser = dbItem.PARAMETER_UPDATE_USER;
                    this.UpdateDateTime = Int64.Parse(dbItem.PARAMETER_UPDATE_DATE_TIME.ToString());
                    this.Deleted = Int16.Parse(dbItem.PARAMETER_DELETED.ToString());
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang), "PARAMETERS", Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] Update(String parameterId, ParameterItem srcObj)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iParameterId = -1;

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (parameterId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "parameterId");
                    return result;
                }

                Int64.TryParse(parameterId, out iParameterId);
                if (iParameterId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "parameterId");
                    return result;
                }


                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                PARAMETERS dbItem =
                    tdmapEntities.PARAMETERS
                        .Where(item => item.PARAMETER_ID == iParameterId && item.PARAMETER_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.PARAMETER = srcObj.Parameter;
                    dbItem.DEPT_ID = srcObj.DeptId;
                    dbItem.PARAMETER_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.PARAMETER_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "PARAMETERS", parameterId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Delete()
        {
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Id <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ParameterId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                PARAMETERS dbItem =
                    tdmapEntities.PARAMETERS
                        .Where(item => item.PARAMETER_ID == this.Id && item.PARAMETER_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.PARAMETER_DELETED = 1;
                    dbItem.PARAMETER_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.PARAMETER_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "PARAMETERS", this.Id.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public void LoadDisplayData()
        {
            DisplayData = new Dictionary<String, String>();
            DisplayData["DepartmentId"] = "";

            Dictionary<String, String> dataDict = new Dictionary<string, string>();
            CachedConversions.GetCachedKeyValues(HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dataDict);

            if (dataDict != null && dataDict.ContainsKey(this.DeptId.ToString()))
                DisplayData["DepartmentId"] = dataDict[this.DeptId.ToString()];
            else
                DisplayData["DepartmentId"] = this.DeptId.ToString();

        }

        public String GetDisplayData(String itemKey, String lang)
        {
            if (itemKey.Equals("DepartmentId") && DisplayData != null && DisplayData.ContainsKey("DepartmentId"))
                return DisplayData["DepartmentId"];

            return "";
        }


    }
}