﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class SideBarMenuData
    {
        public string MenuHeader;
        public List<SideBarMenuNode> MenuNodes {get; set;}

        public SideBarMenuData()
        {
            String lang = CommonConfiguration.SiteLanguage();
            this.MenuHeader = StringResources.getString(StringResources.RES_LABEL, "SIDEBAR_MENU_HEADER", lang);
            this.MenuNodes = new List<SideBarMenuNode>();
        }

        public static SideBarMenuData GenerateMenu()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SideBarMenuData menu = new SideBarMenuData();

            String role = (String)HttpContext.Current.Session["UserSessionRole"];

            /*
            SideBarMenuNode node1 = SideBarMenuNode.Create("Node 1","");
            SideBarMenuItem node1_item1 = SideBarMenuItem.CreateLeaf("Leaf 1/1", "", "/home");
            SideBarMenuItem node1_item2 = SideBarMenuItem.Create("Leaf 1/2", "");
            SideBarMenuItem node1_item2_leaf_1 = SideBarMenuItem.CreateLeaf("Leaf 1/2/1", "", "/leaf121");
            SideBarMenuItem node1_item2_leaf_2 = SideBarMenuItem.Create("Leaf 1/2/2", "");
            SideBarMenuItem node1_item2_leaf_2_leaf_1 = SideBarMenuItem.Create("Leaf 1/2/2/1", "");
            SideBarMenuItem node1_item2_leaf_2_leaf_1_leaf_1 = SideBarMenuItem.CreateLeaf("Leaf 1/2/2/1/1", "", "/leaf12211");
            SideBarMenuItem node1_item2_leaf_2_leaf_1_leaf_2 = SideBarMenuItem.CreateLeaf("Leaf 1/2/2/1/2", "", "/leaf12212");
            SideBarMenuItem node1_item2_leaf_2_leaf_1_leaf_3 = SideBarMenuItem.CreateLeaf("Leaf 1/2/2/1/3", "", "/leaf12213");
            SideBarMenuItem node1_item2_leaf_2_leaf_2 = SideBarMenuItem.CreateLeaf("Leaf 1/2/2/2", "", "/leaf1222");
            SideBarMenuItem node1_item2_leaf_2_leaf_3 = SideBarMenuItem.CreateLeaf("Leaf 1/2/2/3", "", "/leaf1223");

            node1_item2_leaf_2_leaf_1.AddLeaf(node1_item2_leaf_2_leaf_1_leaf_1);
            node1_item2_leaf_2_leaf_1.AddLeaf(node1_item2_leaf_2_leaf_1_leaf_2);
            node1_item2_leaf_2_leaf_1.AddLeaf(node1_item2_leaf_2_leaf_1_leaf_3);
            node1_item2_leaf_2.AddLeaf(node1_item2_leaf_2_leaf_1);
            node1_item2_leaf_2.AddLeaf(node1_item2_leaf_2_leaf_2);
            node1_item2_leaf_2.AddLeaf(node1_item2_leaf_2_leaf_3);
            node1_item2.AddLeaf(node1_item2_leaf_1);
            node1_item2.AddLeaf(node1_item2_leaf_2);
            node1.AddNodeItem(node1_item1);
            node1.AddNodeItem(node1_item2);
            
            menu.MenuNodes.Add(node1);

            SideBarMenuNode node2 = SideBarMenuNode.Create("Node 2", "");
            SideBarMenuItem node2_item1 = SideBarMenuItem.CreateLeaf("Kullanıcılar", "", "/userlist");
            SideBarMenuItem node2_item2 = SideBarMenuItem.CreateLeaf("Pistler", "", "/tracklist");
            SideBarMenuItem node2_item3 = SideBarMenuItem.CreateLeaf("Liftler", "", "/liftlist");
            SideBarMenuItem node2_item4 = SideBarMenuItem.CreateLeaf("Kameralar", "", "/cameralist");
            SideBarMenuItem node2_item5 = SideBarMenuItem.CreateLeaf("Restorantlar", "", "/restaurantlist");
            node2.AddNodeItem(node2_item1);
            node2.AddNodeItem(node2_item2);
            node2.AddNodeItem(node2_item3);
            node2.AddNodeItem(node2_item4);
            node2.AddNodeItem(node2_item5);
            menu.MenuNodes.Add(node2);
            */

            /* Veri Girişi */

            if (role.Equals("M"))
            {/*
                SideBarMenuNode nodeDataOperation = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("DATA_OPERATION", lang), "fa fa-database");
                SideBarMenuItem nodeDataOperation_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DATA_SKI_CENTERS", lang), "fa fa-ticket", "/data/ski");
                SideBarMenuItem nodeDataOperation_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DATA_SKI_LIFTS", lang), "fa fa-subway", "/data/lifts");
                SideBarMenuItem nodeDataOperation_item3 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DATA_SKI_SLOPES", lang), "fa fa-random", "/data/slopes");
                SideBarMenuItem nodeDataOperation_item4 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DATA_LIVE_CAMS", lang), "fa fa-video-camera", "/data/cams");
                SideBarMenuItem nodeDataOperation_item5 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DATA_SPORTS_FUN", lang), "fa fa-bicycle", "/data/sfun");
                SideBarMenuItem nodeDataOperation_item6 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DATA_GASTRONOMY", lang), "fa fa-cutlery", "/data/gast");
                SideBarMenuItem nodeDataOperation_item7 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DATA_INFRASTRUCTURE", lang), "fa fa-info", "/data/inf");
                nodeDataOperation.AddNodeItem(nodeDataOperation_item1);
                nodeDataOperation.AddNodeItem(nodeDataOperation_item2);
                nodeDataOperation.AddNodeItem(nodeDataOperation_item3);
                nodeDataOperation.AddNodeItem(nodeDataOperation_item4);
                nodeDataOperation.AddNodeItem(nodeDataOperation_item5);
                nodeDataOperation.AddNodeItem(nodeDataOperation_item6);
                nodeDataOperation.AddNodeItem(nodeDataOperation_item7);

                menu.MenuNodes.Add(nodeDataOperation);*/
            }

            /* Şirket Ekleme */


            if (role.Equals("A"))
            {
                SideBarMenuNode nodeUserManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("COMPANIES_MANAGEMENT", lang), "fa fa-user");
                SideBarMenuItem nodeUserManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("COMPANIES_LIST", lang), "fa fa-users", "/data/comp");
                SideBarMenuItem nodeUserManagement_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("COMPANIES_NEW", lang), "fa fa-user-plus", "/data/comp/add");
                nodeUserManagement.AddNodeItem(nodeUserManagement_item1);
                nodeUserManagement.AddNodeItem(nodeUserManagement_item2);

                menu.MenuNodes.Add(nodeUserManagement);
            }

            /* Departmanlar */

            if (role.Equals("M"))
            {
                SideBarMenuNode nodeUserManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("DEPARTMENTS_MANAGEMENT", lang), "fa fa-user");
                SideBarMenuItem nodeUserManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DEPARTMENTS_LIST", lang), "fa fa-users", "/data/dept");
                SideBarMenuItem nodeUserManagement_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DEPARTMENTS_NEW", lang), "fa fa-user-plus", "/data/dept/add");
                SideBarMenuItem nodeUserManagement_item3 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DEPARTMENTS_SETTINGS_LİST", lang), "fa fa-users", "/data/depsetting");
                SideBarMenuItem nodeUserManagement_item4 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("DEPARTMENTS_SETTINGS_NEW", lang), "fa fa-user-plus", "/data/depsetting/add");
                nodeUserManagement.AddNodeItem(nodeUserManagement_item1);
                nodeUserManagement.AddNodeItem(nodeUserManagement_item2);
                nodeUserManagement.AddNodeItem(nodeUserManagement_item3);
                nodeUserManagement.AddNodeItem(nodeUserManagement_item4);

                menu.MenuNodes.Add(nodeUserManagement);
            }


            /* İşçiler */

            if (role.Equals("M"))
            {
                SideBarMenuNode nodeUserManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("WORKERS_MANAGEMENT", lang), "fa fa-user");
                SideBarMenuItem nodeUserManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("WORKERS_LIST", lang), "fa fa-users", "/data/worker");
                SideBarMenuItem nodeUserManagement_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("WORKERS_NEW", lang), "fa fa-user-plus", "/data/worker/add");
                nodeUserManagement.AddNodeItem(nodeUserManagement_item1);
                nodeUserManagement.AddNodeItem(nodeUserManagement_item2);

                menu.MenuNodes.Add(nodeUserManagement);
            }


            /* Operatörler */

            if (role.Equals("M"))
            {
                SideBarMenuNode nodeUserManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("OPERATORS_MANAGEMENT", lang), "fa fa-user");
                SideBarMenuItem nodeUserManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("OPERATORS_LIST", lang), "fa fa-users", "/data/ope");
                SideBarMenuItem nodeUserManagement_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("OPERATORS_NEW", lang), "fa fa-user-plus", "/data/ope/add");
                nodeUserManagement.AddNodeItem(nodeUserManagement_item1);
                nodeUserManagement.AddNodeItem(nodeUserManagement_item2);

                menu.MenuNodes.Add(nodeUserManagement);
            }

            /* Gözlem Sınıfı */

            if (role.Equals("M"))
            {
                SideBarMenuNode nodeUserManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("OBCLASS_MANAGEMENT", lang), "fa fa-user");
                SideBarMenuItem nodeUserManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("OBCLASS_LIST", lang), "fa fa-users", "/data/obclass");
                SideBarMenuItem nodeUserManagement_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("OBCLASS_NEW", lang), "fa fa-user-plus", "/data/obclass/add");
                nodeUserManagement.AddNodeItem(nodeUserManagement_item1);
                nodeUserManagement.AddNodeItem(nodeUserManagement_item2);

                menu.MenuNodes.Add(nodeUserManagement);
            }

            /* Gözlem  */

            if (role.Equals("M"))
            {
                SideBarMenuNode nodeUserManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("OBSERVATION_MANAGEMENT", lang), "fa fa-user");
                SideBarMenuItem nodeUserManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("OBSERVATION_LIST", lang), "fa fa-users", "/data/observation");
                SideBarMenuItem nodeUserManagement_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("OBSERVATION_NEW", lang), "fa fa-user-plus", "/data/observation/add");
                nodeUserManagement.AddNodeItem(nodeUserManagement_item1);
                nodeUserManagement.AddNodeItem(nodeUserManagement_item2);

                menu.MenuNodes.Add(nodeUserManagement);
            }


           

            

            /* Kullanıcı Yönetimi */

            if (role.Equals("A"))
            {
                SideBarMenuNode nodeUserManagement = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("USER_MANAGEMENT", lang), "fa fa-user");
                SideBarMenuItem nodeUserManagement_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("USERS_LIST", lang), "fa fa-users", "/usr/list");
                SideBarMenuItem nodeUserManagement_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("USER_NEW", lang), "fa fa-user-plus", "/usr/add");
                nodeUserManagement.AddNodeItem(nodeUserManagement_item1);
                nodeUserManagement.AddNodeItem(nodeUserManagement_item2);

                menu.MenuNodes.Add(nodeUserManagement);
            }

            if (role.Equals("M"))
            {
                SideBarMenuNode nodeObservationMng = SideBarMenuNode.Create(SideBarMenuData.GetMenuDisp("OBSERVATION_MNG", lang), "fa fa-user");
                SideBarMenuItem nodeObservationMng_item1 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("OBSERVATION_NEW", lang), "fa fa-users", "/test/observation/add");
                SideBarMenuItem nodeObservationMng_item2 = SideBarMenuItem.CreateLeaf(SideBarMenuData.GetMenuDisp("OBSERVATION_LIST", lang), "fa fa-user-plus", "/test/observation/list");
                nodeObservationMng.AddNodeItem(nodeObservationMng_item1);
                nodeObservationMng.AddNodeItem(nodeObservationMng_item2);

                menu.MenuNodes.Add(nodeObservationMng);
            }

            if (role.Equals("M"))
            {
                SideBarMenuNode nodeParameterMng = SideBarMenuNode.Create("Parametre Yönetimi", "fa fa-user");
                SideBarMenuItem nodeParameterMng_item1 = SideBarMenuItem.CreateLeaf("Parametre Ekleme", "fa fa-users", "/parameter/add");
                SideBarMenuItem nodeParameterMng_item2 = SideBarMenuItem.CreateLeaf("Parametre Listessi", "fa fa-user-plus", "/parameter/list");
                nodeParameterMng.AddNodeItem(nodeParameterMng_item1);
                nodeParameterMng.AddNodeItem(nodeParameterMng_item2);

                menu.MenuNodes.Add(nodeParameterMng);
            }

            return menu;
        }

        private static String GetMenuDisp(String key, String lang)
        {
            return StringResources.getString(StringResources.RES_MENU_ITEMS, key, lang);
        }
    }

    public class SideBarMenuNode
    {
        public string IconClass;
        public string ItemDisp;
        public List<SideBarMenuItem> NodeItems;

        public SideBarMenuNode()
        {
            this.IconClass = "fa fa-circle-o";
            this.ItemDisp = "N/A MenuNode";
            this.NodeItems = new List<SideBarMenuItem>();
        }

        public void AddNodeItem(SideBarMenuItem item)
        {
            this.NodeItems.Add(item);
        }

        public static SideBarMenuNode Create(string itemDisp, string iconClass)
        {
            SideBarMenuNode retItem = new SideBarMenuNode();

            retItem.ItemDisp = itemDisp;
            retItem.IconClass = iconClass.Equals("")?retItem.IconClass:iconClass;
            retItem.NodeItems = new List<SideBarMenuItem>();

            return retItem;
        }

    }

    public class SideBarMenuItem
    {
        public bool IsLeaf;
        public MenuLeafItem Item;
        public string IconClass;
        public string ItemDisp;
        public List<SideBarMenuItem> LeafItems;

        public SideBarMenuItem()
        {
            this.IsLeaf = false;
            this.Item = new MenuLeafItem();
            this.IconClass = "fa fa-circle-o";
            this.ItemDisp = "N/A MenuItem";
            this.LeafItems = new List<SideBarMenuItem>();
        }

        public void AddLeaf(SideBarMenuItem item)
        {
            this.LeafItems.Add(item);
        }

        public static SideBarMenuItem CreateLeaf(string itemDisp, string iconClass, string href)
        {
            SideBarMenuItem retItem = new SideBarMenuItem();

            retItem.Item = MenuLeafItem.Create(itemDisp, iconClass, href);
            retItem.IsLeaf = true;
            retItem.IconClass = "";
            retItem.ItemDisp = "";
            retItem.LeafItems = null;

            return retItem;
        }

        public static SideBarMenuItem Create(string itemDisp, string iconClass)
        {
            SideBarMenuItem retItem = new SideBarMenuItem();

            retItem.Item = null;
            retItem.IsLeaf = false;
            retItem.IconClass = iconClass.Equals("")?retItem.IconClass:iconClass;
            retItem.ItemDisp = itemDisp;
            retItem.LeafItems = new List<SideBarMenuItem>();

            return retItem;
        }
    }

    public class MenuLeafItem
    {
        public string Href;
        public string IconClass;
        public string ItemDisp;

        public MenuLeafItem()
        {
            this.Href = "javascript:void(0)";
            this.IconClass = "fa fa-circle-o";
            this.ItemDisp = "N/A LeafItem";
        }

        public static MenuLeafItem Create(string itemDisp, string iconClass, string href)
        {
            MenuLeafItem retItem = new MenuLeafItem();
            retItem.ItemDisp = itemDisp;
            retItem.IconClass = iconClass.Equals("") ? retItem.IconClass : iconClass;
            retItem.Href = href;
            return retItem;
        }
    }
}