﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class CompanyItem
    {
        
        public Int64 Id { get; set; }
        public String Name { get; set; }
        public String CompanyCategory { get; set; }
        public String CompanyEmail { get; set; }
        public Int64 CompanyWorkerNumber { get; set; }
        public String Manager { get; set; }
        public string InsertUser { get; set; }
        public Int64 InsertDateTime { get; set; }
        public string UpdateUser { get; set; }
        public Int64 UpdateDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }

        public CompanyItem()
        {
            this.Id = -1;
            this.Name = "";
            this.CompanyCategory = "";
            this.CompanyEmail = "";
            this.CompanyWorkerNumber=0;
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
        }

        public CompanyItem(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.Id = -1;
            this.Name = valuesDict.ContainsKey("CompanyName") && valuesDict["CompanyName"] != null ? (string)valuesDict["CompanyName"] : "";
            this.CompanyCategory = valuesDict.ContainsKey("CompanyCategory") && valuesDict["CompanyCategory"] != null ? (string)valuesDict["CompanyCategory"] : "";
            this.CompanyEmail = valuesDict.ContainsKey("CompanyEmail") && valuesDict["CompanyEmail"] != null ? (string)valuesDict["CompanyEmail"] : "";
            this.CompanyWorkerNumber = valuesDict.ContainsKey("CompanyWorkerNumber") && valuesDict["CompanyWorkerNumber"] != null ? Int64.Parse((string)valuesDict["CompanyWorkerNumber"]) : 0;
            this.Manager = valuesDict.ContainsKey("Manager") && valuesDict["Manager"] != null ? (string)valuesDict["Manager"] :"";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : ""; ;
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("CompanyItem.Id", this.Id.ToString());
            param += SessionParamsUtil.createParameter("CompanyItem.Name", this.Name);
            param += SessionParamsUtil.createParameter("CompanyItem.CompanyCategory", this.CompanyCategory);
            param += SessionParamsUtil.createParameter("CompanyItem.CompanyEmail", this.CompanyEmail);
            param += SessionParamsUtil.createParameter("CompanyItem.CompanyWorkerNumber", this.CompanyWorkerNumber.ToString());
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public String[] Create()
        {
            try
            {
                String lang = CommonConfiguration.SiteLanguage();
                CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

                String[] result = { "true", "", "" };
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                COMPANIES dbItem = null;

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Name.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "Name");
                }
                if (result[0].Equals("false"))
                    return result;

                if (dbItem != null)
                {
                    String strInvUserName = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_INT_CODE_INVALID", lang), this.Name);
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = strInvUserName;
                }
                else
                {
                    dbItem = new COMPANIES();
                    dbItem.COMP_NAME = this.Name;
                    dbItem.COMP_CATEGORY = this.CompanyCategory;
                    dbItem.COMP_EMAIL = this.CompanyEmail;
                    dbItem.COMP_WORKERS_NMBR = Int64.Parse(this.CompanyWorkerNumber.ToString());
                    dbItem.MANAGER = this.Manager;
                    dbItem.COMP_DELETED = 0;
                    dbItem.COMP_INSERT_DATETIME = StringResources.GetLongCurrentDateTime();
                    dbItem.COMP_INSERT_USER = CommonConfiguration.SessionUserName();
                    dbItem.COMP_UPDATE_DATE_TIME = 0;
                    dbItem.COMP_UPDATE_USER = "";

                    tdmapEntities.COMPANIES.AddObject(dbItem);
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                   
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] LoadByKey(Int64 Id)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                COMPANIES dbItem = tdmapEntities.COMPANIES.Where(
                    item => item.COMP_DELETED != 1 && item.COMP_ID == Id).SingleOrDefault();

                if (dbItem != null)
                {
                    this.Id = dbItem.COMP_ID;
                    this.Name = dbItem.COMP_NAME;
                    this.CompanyCategory = dbItem.COMP_CATEGORY;
                    this.CompanyEmail = dbItem.COMP_EMAIL;
                    this.CompanyWorkerNumber =Int64.Parse(dbItem.COMP_WORKERS_NMBR.ToString());
                    this.InsertUser = dbItem.COMP_INSERT_USER;
                    this.InsertDateTime = Int64.Parse(dbItem.COMP_INSERT_DATETIME.ToString());
                    this.UpdateUser = dbItem.COMP_UPDATE_USER;
                    this.UpdateDateTime = Int64.Parse(dbItem.COMP_UPDATE_DATE_TIME.ToString());
                    this.Deleted = Int16.Parse(dbItem.COMP_DELETED.ToString());
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang),"COMPANIES",Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] Update(String coMpanyId, CompanyItem srcObj)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iCompanyId = -1;

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (coMpanyId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "coMpanyId");
                    return result;
                }

                Int64.TryParse(coMpanyId, out iCompanyId);
                if ( iCompanyId < 0 )
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "coMpanyId");
                    return result;
                }


                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
               
                COMPANIES dbItem =
                    tdmapEntities.COMPANIES
                        .Where(item => item.COMP_ID == iCompanyId && item.COMP_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.COMP_NAME = srcObj.Name;
                    dbItem.COMP_CATEGORY = srcObj.CompanyCategory;
                    dbItem.COMP_EMAIL = srcObj.CompanyEmail;
                    dbItem.COMP_WORKERS_NMBR = Int64.Parse(srcObj.CompanyWorkerNumber.ToString());
                    dbItem.COMP_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.COMP_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "COMPANIES", coMpanyId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Delete()
        {
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Id <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "coMpanyId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                COMPANIES dbItem =
                    tdmapEntities.COMPANIES
                        .Where(item => item.COMP_ID == this.Id && item.COMP_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.COMP_DELETED = 1;
                    dbItem.COMP_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.COMP_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "COMPANIES", this.Id.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

    }
}