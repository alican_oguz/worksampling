﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Globalization;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class ObservationItem
    {

        public Int64 Id { get; set; }
        public Int64 DepartmentId { get; set; }
        public Int64 ObclassId { get; set; }
        public Int64 WorkerId { get; set; }
        public String Name { get; set; }
        public string InsertUser { get; set; }
        public Int64 InsertDateTime { get; set; }
        public string UpdateUser { get; set; }
        public Int64 UpdateDateTime { get; set; }
        public short Deleted { get; set; }
        public String SessionParam { get; set; }
        private Dictionary<String, String> DisplayData;

        public ObservationItem()
        {
            this.Id = -1;
            this.DepartmentId = -1;
            this.ObclassId = -1;
            this.WorkerId = -1;
            this.Name = "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = "";
        }

        public ObservationItem(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.Id = -1;
            this.Name = valuesDict.ContainsKey("ObservationName") && valuesDict["ObservationName"] != null ? (string)valuesDict["ObservationName"] : "";
            this.InsertUser = "";
            this.InsertDateTime = 0;
            this.UpdateUser = "";
            this.UpdateDateTime = 0;
            this.Deleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : "";



            String sDepartmentId = valuesDict.ContainsKey("DepartmentId") && valuesDict["DepartmentId"] != null ? (string)valuesDict["DepartmentId"] : "-1";
            Int64 iDepartmentId = 0;
            Int64.TryParse(sDepartmentId, out iDepartmentId);

            this.DepartmentId = iDepartmentId;

            String sObclassId = valuesDict.ContainsKey("ObclassId") && valuesDict["ObclassId"] != null ? (string)valuesDict["ObclassId"] : "-1";
            Int64 iObclassId = 0;
            Int64.TryParse(sObclassId, out iObclassId);

            this.ObclassId = iObclassId;

            String sWorkerId = valuesDict.ContainsKey("WorkerId") && valuesDict["WorkerId"] != null ? (string)valuesDict["WorkerId"] : "-1";
            Int64 iWorkerId = 0;
            Int64.TryParse(sWorkerId, out iWorkerId);

            this.WorkerId = iWorkerId;
        }

        public String[] Create()
        {
            try
            {
                String lang = CommonConfiguration.SiteLanguage();
                CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

                String[] result = { "true", "", "" };
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                ACTIVITIES dbItem = null;

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Name.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationName");
                }

                if (this.DepartmentId <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "DepartmentId");
                }

                if (this.ObclassId <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObclassId");
                }

                if (this.WorkerId <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "WorkerId");
                }



                if (result[0].Equals("false"))
                    return result;

                    dbItem = new ACTIVITIES();
                    dbItem.ACT_NAME = this.Name;
                    dbItem.DEPT_ID = this.DepartmentId;
                    dbItem.ACT_TYPE_ID = this.ObclassId;
                    dbItem.WORKERS_ID = this.WorkerId;

                    dbItem.ACT_DELETED = 0;
                    dbItem.ACT_INSERT_DATETIME = StringResources.GetLongCurrentDateTime();
                    dbItem.ACT_INSERT_USER = CommonConfiguration.SessionUserName();
                    dbItem.ACT_UPDATE_DATE_TIME = 0;
                    dbItem.ACT_UPDATE_USER = "";

                    tdmapEntities.ACTIVITIES.AddObject(dbItem);
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OBSERVATION);
                

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] LoadByKey(Int64 Id)
        {
            String[] result = { "true", "", "" };
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                ACTIVITIES dbItem = tdmapEntities.ACTIVITIES.Where(
                    item => item.ACT_DELETED != 1 && item.ACT_ID == Id).SingleOrDefault();

                if (dbItem != null)
                {
                    this.Id = dbItem.ACT_ID;
                    this.DepartmentId = dbItem.DEPT_ID;
                    this.ObclassId = dbItem.ACT_TYPE_ID;
                    this.WorkerId = dbItem.WORKERS_ID;
                    this.Name = dbItem.ACT_NAME;
                    this.InsertUser = dbItem.ACT_INSERT_USER;
                    this.InsertDateTime = Int64.Parse(dbItem.ACT_INSERT_DATETIME.ToString());
                    this.UpdateUser = dbItem.ACT_UPDATE_USER;
                    this.UpdateDateTime = Int64.Parse(dbItem.ACT_UPDATE_DATE_TIME.ToString());
                    this.Deleted = Int16.Parse(dbItem.ACT_DELETED.ToString());
                    this.LoadDisplayData();
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_RECORD_NOT_FOUND", lang), "ACTIVITIES", Id.ToString());
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] Update(String observationId, String dePartmentId, String obClassId, String worKerId, ObservationItem srcObj)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iObservationId = -1;
            Int64 iDepartmentId = -1;
            Int64 iObclassId = -1;
            Int64 iWorkerId = -1;

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (observationId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationId");
                    return result;
                }

                Int64.TryParse(observationId, out iObservationId);
                if (iObservationId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationId");
                    return result;
                }

                if (dePartmentId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                Int64.TryParse(dePartmentId, out iDepartmentId);
                if (iDepartmentId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }


                if (obClassId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "obClassId");
                    return result;
                }

                Int64.TryParse(obClassId, out iObclassId);
                if (iObclassId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "obClassId");
                    return result;
                }


                if (worKerId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "worKerId");
                    return result;
                }

                Int64.TryParse(worKerId, out iWorkerId);
                if (iWorkerId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "worKerId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();


                ACTIVITIES dbItem =
                    tdmapEntities.ACTIVITIES
                        .Where(item => item.ACT_ID== iObservationId && item.ACT_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {

                    dbItem.ACT_NAME = srcObj.Name;
                    dbItem.DEPT_ID = srcObj.DepartmentId;
                    dbItem.ACT_TYPE_ID = srcObj.ObclassId;
                    dbItem.WORKERS_ID = srcObj.WorkerId;
                    dbItem.ACT_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.ACT_UPDATE_USER= CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OBSERVATION);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "SLOPES", observationId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] UpdateStatus(String observationId, String dePartmentId, String obClassId, String worKerId, String dataStatus)
        {
            String lang = CommonConfiguration.SiteLanguage();
            CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);
            Int64 iObservationId = -1;
            Int64 iDepartmentId = -1;
            Int64 iObclassId = -1;
            Int64 iWorkerId = -1;
            Int16 iStatus = -1;
            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (observationId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationId");
                    return result;
                }

                Int64.TryParse(observationId, out iObservationId);
                if (iObservationId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationId");
                    return result;
                }

                if (dePartmentId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                Int64.TryParse(dePartmentId, out iDepartmentId);
                if (iDepartmentId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dePartmentId");
                    return result;
                }

                if (obClassId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "obClassId");
                    return result;
                }

                Int64.TryParse(obClassId, out iObclassId);
                if (iObclassId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "obClassId");
                    return result;
                }


                if (worKerId.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "worKerId");
                    return result;
                }

                Int64.TryParse(worKerId, out iWorkerId);
                if (iWorkerId < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "worKerId");
                    return result;
                }


                Int16.TryParse(dataStatus, out iStatus);
                if (iStatus < 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "dataStatus");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                ACTIVITIES dbItem =
                    tdmapEntities.ACTIVITIES
                        .Where(item => item.ACT_ID == iObservationId && item.ACT_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {
                    dbItem.ACT_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.ACT_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OBSERVATION);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "WORKERS", observationId);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Delete()
        {
            String lang = CommonConfiguration.SiteLanguage();

            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.Id <= 0)
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "ObservationId");
                    return result;
                }

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                ACTIVITIES dbItem =
                    tdmapEntities.ACTIVITIES
                        .Where(item => item.ACT_ID == this.Id && item.ACT_DELETED != 1).SingleOrDefault();

                if (dbItem != null)
                {
                    dbItem.ACT_UPDATE_DATE_TIME = StringResources.GetLongCurrentDateTime();
                    dbItem.ACT_UPDATE_USER = CommonConfiguration.SessionUserName();
                    dbItem.ACT_DELETED= 1;
                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";

                    CachedConversions.ClearCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OBSERVATION);
                }
                else
                {
                    result[0] = "false";
                    result[1] = CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    result[2] = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "ACTIVITIES", this.Id.ToString());
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
                return result;
            }
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("ObservationItem.Id", this.Id.ToString());
            param += SessionParamsUtil.createParameter("ObservationItem.DepartmentId", this.DepartmentId.ToString());
            param += SessionParamsUtil.createParameter("ObservationItem.ObclassId", this.ObclassId.ToString());
            param += SessionParamsUtil.createParameter("ObservationItem.WorkerId", this.WorkerId.ToString());
            param += SessionParamsUtil.createParameter("ObservationItem.Name", this.Name);
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public void LoadDisplayData()
        {
            DisplayData = new Dictionary<String, String>();
            DisplayData["DepartmentId"] = "";
            DisplayData["ObclassId"] = "";
            DisplayData["WorkerId"] = "";

            Dictionary<String, String> dataDict = new Dictionary<string, string>();
            CachedConversions.GetCachedKeyValues(HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dataDict);

            if (dataDict != null && dataDict.ContainsKey(this.DepartmentId.ToString()))
                DisplayData["DepartmentId"] = dataDict[this.DepartmentId.ToString()];
            else
                DisplayData["DepartmentId"] = this.DepartmentId.ToString();



            CachedConversions.GetCachedKeyValues(HttpContext.Current, CachedConversions.CACHED_DICT_OBCLASS, out dataDict);

            if (dataDict != null && dataDict.ContainsKey(this.ObclassId.ToString()))
                DisplayData["ObclassId"] = dataDict[this.ObclassId.ToString()];
            else
                DisplayData["ObclassId"] = this.ObclassId.ToString();


            CachedConversions.GetCachedKeyValues(HttpContext.Current, CachedConversions.CACHED_DICT_WORKERS, out dataDict);

            if (dataDict != null && dataDict.ContainsKey(this.WorkerId.ToString()))
                DisplayData["WorkerId"] = dataDict[this.WorkerId.ToString()];
            else
                DisplayData["WorkerId"] = this.WorkerId.ToString();



        }

        public String GetDisplayData(String itemKey, String lang)
        {
            if (itemKey.Equals("DepartmentId") && DisplayData != null && DisplayData.ContainsKey("DepartmentId"))
                return DisplayData["DepartmentId"];

            if (itemKey.Equals("ObclassId") && DisplayData != null && DisplayData.ContainsKey("ObclassId"))
                return DisplayData["ObclassId"];

            if (itemKey.Equals("WorkerId") && DisplayData != null && DisplayData.ContainsKey("WorkerId"))
                return DisplayData["WorkerId"];

            return "";
        }
    }
}