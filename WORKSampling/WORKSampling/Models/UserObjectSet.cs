﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _2DMap.Models
{
    public class UserObjectSet
    {
        public List<UserObject> UserObjectList { get; set; }

        public UserObjectSet()
        {
            UserObjectList = new List<UserObject>();
        }

        public UserObjectSet Load()
        {
            try
            {
                List<USERS> users = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                users = tdmapEntities.USERS.Where(
                        user => user.USR_DELETED != 1
                    ).OrderBy(user => user.USR_USERNAME).ToList<USERS>();

                UserObjectList = new List<UserObject>();

                if (users != null)
                {
                    foreach (USERS usrObj in users)
                    {
                        UserObject usrTmpObj = new UserObject();

                        usrTmpObj.UserDeleted = short.Parse(usrObj.USR_DELETED.ToString());
                        usrTmpObj.UserEMail = usrObj.USR_EMAIL;
                        usrTmpObj.UserFirstName = usrObj.USR_NAME;
                        usrTmpObj.UserInsertDateTime = Int64.Parse(usrObj.USR_INSERT_DATETIME.ToString());
                        usrTmpObj.UserInsertUser = usrObj.USR_INSERT_USER;
                        usrTmpObj.UserLastName = usrObj.USR_SURNAME;
                        usrTmpObj.UserMidName = usrObj.USR_MID_NAME;
                        usrTmpObj.UserMustChangePwd = short.Parse(usrObj.USR_MUST_CHANGE_PWD.ToString());
                        usrTmpObj.UserName = usrObj.USR_USERNAME;
                        usrTmpObj.UserPassword = usrObj.USR_PWD;
                        usrTmpObj.UserRetryPwdCount = short.Parse(usrObj.USR_PWD_RETRY_CNT.ToString());
                        usrTmpObj.UserStatus = short.Parse(usrObj.USR_STATUS.ToString());
                        usrTmpObj.UserUpdateDateTime = Int64.Parse(usrObj.USR_UPDATE_DATETIME.ToString());
                        usrTmpObj.UserLastLoginDateTime = Int64.Parse(usrObj.USR_LAST_LOGIN_DATETIME.ToString());
                        usrTmpObj.UserUpdateUser = usrObj.USR_UPDATE_USER;
                        usrTmpObj.UserProfileImage = usrObj.USR_PROFILE_IMG;
                        UserObjectList.Add(usrTmpObj);
                    }

                }


            }
            catch (Exception ex)
            {
                UserObjectList = new List<UserObject>();
            }

            return this;
        }

        public UserObjectSet LoadForManager()
        {
            try
            {
                List<USERS> users = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                users = tdmapEntities.USERS.Where(
                        user => user.USR_DELETED != 1 &&
                            user.USR_ROLE == "M"
                    ).OrderBy(user => user.USR_USERNAME).ToList<USERS>();

                UserObjectList = new List<UserObject>();

                if (users != null)
                {
                    foreach (USERS usrObj in users)
                    {
                        UserObject usrTmpObj = new UserObject();

                        usrTmpObj.UserId = Int32.Parse(usrObj.USR_ID.ToString());
                        usrTmpObj.UserDeleted = short.Parse(usrObj.USR_DELETED.ToString());
                        usrTmpObj.UserEMail = usrObj.USR_EMAIL;
                        usrTmpObj.UserFirstName = usrObj.USR_NAME;
                        usrTmpObj.UserInsertDateTime = Int64.Parse(usrObj.USR_INSERT_DATETIME.ToString());
                        usrTmpObj.UserInsertUser = usrObj.USR_INSERT_USER;
                        usrTmpObj.UserLastName = usrObj.USR_SURNAME;
                        usrTmpObj.UserMidName = usrObj.USR_MID_NAME;
                        usrTmpObj.UserMustChangePwd = short.Parse(usrObj.USR_MUST_CHANGE_PWD.ToString());
                        usrTmpObj.UserName = usrObj.USR_USERNAME;
                        usrTmpObj.UserPassword = usrObj.USR_PWD;
                        usrTmpObj.UserRetryPwdCount = short.Parse(usrObj.USR_PWD_RETRY_CNT.ToString());
                        usrTmpObj.UserStatus = short.Parse(usrObj.USR_STATUS.ToString());
                        usrTmpObj.UserUpdateDateTime = Int64.Parse(usrObj.USR_UPDATE_DATETIME.ToString());
                        usrTmpObj.UserLastLoginDateTime = Int64.Parse(usrObj.USR_LAST_LOGIN_DATETIME.ToString());
                        usrTmpObj.UserUpdateUser = usrObj.USR_UPDATE_USER;
                        usrTmpObj.UserProfileImage = usrObj.USR_PROFILE_IMG;
                        UserObjectList.Add(usrTmpObj);
                    }

                }


            }
            catch (Exception ex)
            {
                UserObjectList = new List<UserObject>();
            }

            return this;
        }

    }
}