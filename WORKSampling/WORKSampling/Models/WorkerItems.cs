﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class WorkerItems
    {
        
        public List<WorkerItem> WorkerItemsList { get; set; }

        public WorkerItems()
        {
            this.WorkerItemsList = new List<WorkerItem>();
        }

        public String[] Load()
        {
            String[] result = { "true", "", "" };

            try
            {
                List<WORKERS> workers = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.WorkerItemsList = new List<WorkerItem>();
                String username = CommonConfiguration.SessionUserName();


                workers =
                    tdmapEntities.WORKERS.Where(
                        lift => lift.WORKERS_DELETED != 1 && lift.WORKERS_INSERT_USER.Equals(username)
                    ).OrderBy(lift => lift.WORKERS_NAME).ToList<WORKERS>();

                if (workers != null)
                {
                    foreach (WORKERS worker in workers)
                    { 
                        WorkerItem item = new WorkerItem();
                        item.Id = worker.WORKERS_ID;
                        item.DepartmentId = worker.DEPT_ID;
                        item.Name = worker.WORKERS_NAME;
                        item.Surname = worker.WORKERS_SURNAME;
                        item.Tc = Int64.Parse(worker.WORKERS_TC.ToString());
                        item.InsertUser = worker.WORKERS_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(worker.WORKERS_INSERT_DATETIME.ToString());
                        item.UpdateUser = worker.WORKERS_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(worker.WORKERS_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(worker.WORKERS_DELETED.ToString());
                        item.LoadDisplayData();
                        this.WorkerItemsList.Add(item);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] LoadByDepartmentId(Int64 dePartmentId)
        {
            String[] result = { "true", "", "" };

            try
            {
                List<WORKERS> workers = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.WorkerItemsList = new List<WorkerItem>();

                workers =
                    tdmapEntities.WORKERS.Where(
                        lift => lift.WORKERS_DELETED != 1
                            && lift.DEPT_ID == dePartmentId
                    ).OrderBy(lift => lift.WORKERS_NAME).ToList<WORKERS>();

                if (workers != null)
                {
                    foreach (WORKERS worker in workers)
                    {
                        WorkerItem item = new WorkerItem();
                        item.Id = worker.WORKERS_ID;
                        item.DepartmentId = worker.DEPT_ID;
                        item.Name = worker.WORKERS_NAME;
                        item.Surname = worker.WORKERS_SURNAME;
                        item.Tc = Int64.Parse(worker.WORKERS_TC.ToString());
                        item.InsertUser = worker.WORKERS_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(worker.WORKERS_INSERT_DATETIME.ToString());
                        item.UpdateUser = worker.WORKERS_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(worker.WORKERS_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(worker.WORKERS_DELETED.ToString());
                        item.LoadDisplayData();
                        this.WorkerItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

      
    }
}