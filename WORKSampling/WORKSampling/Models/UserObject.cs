﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Globalization;
using System.Web;
using _2DMap.Utility;
using System.Net;
using System.Net.Mail;
using System.Web.Configuration;

namespace _2DMap.Models
{
    public class UserObject
    {
        public Int32 UserId { get; set; }
        public string UserName { get; set; }
        public string UserFirstName { get; set; }
        public string UserMidName { get; set; }
        public string UserLastName { get; set; }
        public string UserEMail { get; set; }
        public string UserPassword { get; set; }
        public string UserProfileImage { get; set; }
        public short UserMustChangePwd { get; set; }
        public short UserRetryPwdCount { get; set; }
        public short UserStatus { get; set; }
        public string UserInsertUser { get; set; }
        public Int64 UserInsertDateTime { get; set; }
        public string UserUpdateUser { get; set; }
        public Int64 UserUpdateDateTime { get; set; }
        public Int64 UserLastLoginDateTime { get; set; }
        public short UserDeleted { get; set; }
        public String UserRole { get; set; }
        public String SessionParam { get; set; }

        private static String lang = CommonConfiguration.SiteLanguage();
        CultureInfo culture = CultureInfo.CreateSpecificCulture(lang);

        public UserObject()
        {
            this.UserId = 0;
            this.UserName = "";
            this.UserFirstName = "";
            this.UserMidName = "";
            this.UserLastName = "";
            this.UserEMail = "";
            this.UserPassword = "";
            this.UserProfileImage = CommonConfiguration.UserDefaultProfileImg();
            this.UserMustChangePwd = 0;
            this.UserRetryPwdCount = 0;
            this.UserStatus = 0;
            this.UserInsertUser = "";
            this.UserInsertDateTime = 0;
            this.UserUpdateUser = "";
            this.UserUpdateDateTime = 0;
            this.UserLastLoginDateTime = 0;
            this.UserDeleted = 0;
            this.UserRole = "";
            this.SessionParam = "";
        }

        public UserObject(List<FormDataNameValue> formdatapair)
        {
            Dictionary<String, object> valuesDict = new Dictionary<string, object>();
            FormDataRetreiver.getFormData(formdatapair, out valuesDict);

            this.UserName = valuesDict.ContainsKey("Username") && valuesDict["Username"] != null ? (string)valuesDict["Username"] : "";
            this.UserFirstName = valuesDict.ContainsKey("FirstName") && valuesDict["FirstName"] != null ? (string)valuesDict["FirstName"] : "";
            this.UserMidName = valuesDict.ContainsKey("MidName") && valuesDict["MidName"] != null ? (string)valuesDict["MidName"] : "";
            this.UserLastName = valuesDict.ContainsKey("LastName") && valuesDict["LastName"] != null ? (string)valuesDict["LastName"] : "";
            this.UserEMail = valuesDict.ContainsKey("UserEmail") && valuesDict["UserEmail"] != null ? (string)valuesDict["UserEmail"] : "";
            this.UserPassword = valuesDict.ContainsKey("Password") && valuesDict["Password"] != null ? (string)valuesDict["Password"] : "";
            this.UserRole = valuesDict.ContainsKey("Role") && valuesDict["Role"] != null ? (string)valuesDict["Role"] : "";
            this.UserProfileImage = CommonConfiguration.UserDefaultProfileImg();
            this.UserMustChangePwd = 0;
            this.UserRetryPwdCount = 0;
            this.UserStatus = 0;
            this.UserInsertUser = "";
            this.UserInsertDateTime = 0;
            this.UserUpdateUser = "";
            this.UserUpdateDateTime = 0;
            this.UserLastLoginDateTime = 0;
            this.UserDeleted = 0;
            this.SessionParam = valuesDict.ContainsKey("refcode") ? (string)valuesDict["refcode"] : "";
        }

        public String[] Create()
        {
            try
            {
                String[] result = { "true", "", "" };
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                USERS userTmpObj = null;
                List<USERS> userObj = null;
                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (this.UserName.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserName");
                }
                else if (this.UserFirstName.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserFirstName");
                }
                else if (this.UserLastName.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserLastName");
                }
                else if (this.UserPassword.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserPassword");
                }
                else if (this.UserEMail.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserEMail");
                }

                if (result[0].Equals("false"))
                    return result;

                this.UserStatus = Int16.Parse(UserSessionData.USER_STATUS_ACTIVE);

                String userPassword = StringResources.MD5Hash(this.UserPassword);
                String upperUserName = this.UserName.ToUpper(culture);
                String lowerUserName = this.UserName.ToLower(culture);

                userObj = tdmapEntities.USERS.Where(
                            user => (
                                        user.USR_USERNAME.Equals(this.UserName) ||
                                        user.USR_USERNAME.Equals(upperUserName) ||
                                        user.USR_USERNAME.Equals(lowerUserName)
                                        
                                     )||
                                     user.USR_EMAIL.Equals(this.UserEMail)
                                        &&
                                        user.USR_DELETED != 1
                            ).ToList<USERS>();

                if (userObj.Count>0)
                {
                    foreach (var item in userObj)
                    {
                        if (item.USR_USERNAME==this.UserName)
                        {
                            String strInvUserName = String.Format(StringResources.getString(StringResources.RES_ERR, "USER_INV_USERNAME", lang), this.UserName);
                            result[0] = "false";
                            result[1] = "-1";
                            result[2] = strInvUserName;
                            return result;
                            
                        }
                        else  if (item.USR_EMAIL==this.UserEMail)
                        {
                          
                              String strInvEMail = String.Format(StringResources.getString(StringResources.RES_ERR, "USER_INV_EMAIL", lang), this.UserEMail);
                              result[0] = "false";
                              result[1] = "-1";
                              result[2] = strInvEMail;  
                        }
                    }
                                    

                }                          
            
                else
                {

                    userTmpObj = new USERS();
                    userTmpObj.USR_USERNAME = this.UserName;
                    userTmpObj.USR_NAME = this.UserFirstName;
                    userTmpObj.USR_MID_NAME = this.UserMidName;
                    userTmpObj.USR_SURNAME = this.UserLastName;
                    userTmpObj.USR_PWD = userPassword;
                    userTmpObj.USR_EMAIL = this.UserEMail;
                    userTmpObj.USR_DELETED = this.UserDeleted;
                    userTmpObj.USR_INSERT_DATETIME = StringResources.GetLongCurrentDateTime();
                    userTmpObj.USR_INSERT_USER = CommonConfiguration.SessionUserName();
                    userTmpObj.USR_UPDATE_DATETIME = 0;
                    userTmpObj.USR_UPDATE_USER = "";
                    userTmpObj.USR_LAST_LOGIN_DATETIME = 0;
                    userTmpObj.USR_PWD_RETRY_CNT = this.UserRetryPwdCount;
                    userTmpObj.USR_PROFILE_IMG = this.UserProfileImage;
                    userTmpObj.USR_STATUS = this.UserStatus;
                    userTmpObj.USR_MUST_CHANGE_PWD = this.UserMustChangePwd;
                    userTmpObj.USR_ROLE = this.UserRole;

                    tdmapEntities.USERS.AddObject(userTmpObj);
                        tdmapEntities.SaveChanges();

                        result[0] = "true";
                        result[1] = "";
                        result[2] = "";
                    
                   
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-2";
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] UpdateUserInfo(String userName, UserObject srcUserObj)
        {
            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (userName.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserName");
                    return result;
                }


                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                String upperUserName = userName.ToUpper(culture);
                String lowerUserName = userName.ToLower(culture);
                this.UserEMail = srcUserObj.UserEMail;
                USERS userObj =
                    tdmapEntities.USERS
                        .Where(
                            user => (
                                        user.USR_USERNAME.Equals(userName) ||
                                        user.USR_USERNAME.Equals(upperUserName) ||
                                        user.USR_USERNAME.Equals(lowerUserName) 
                                    ) &&
                                        user.USR_DELETED != 1
                        ).SingleOrDefault();

                if (userObj != null)
                {
                    USERS mailCheck =
                    tdmapEntities.USERS
                        .Where(
                            mail => (
                                mail.USR_USERNAME != userObj.USR_USERNAME && mail.USR_DELETED != 1 && mail.USR_EMAIL.Equals(this.UserEMail) && !mail.USR_EMAIL.Equals("")                            
                                    )
                        ).SingleOrDefault();
                    if (mailCheck != null)
                    {
                        
                        String strInvEMail = String.Format(StringResources.getString(StringResources.RES_ERR, "USER_INV_EMAIL", lang), srcUserObj.UserEMail);
                        result[0] = "false";
                        result[1] = "-1";
                        result[2] = strInvEMail;
                        return result;
                    }
                    
                        userObj.USR_NAME = srcUserObj.UserFirstName;
                        userObj.USR_MID_NAME = srcUserObj.UserMidName;
                        userObj.USR_SURNAME = srcUserObj.UserLastName;
                        userObj.USR_EMAIL = srcUserObj.UserEMail;
                        userObj.USR_UPDATE_DATETIME = StringResources.GetLongCurrentDateTime();
                        userObj.USR_UPDATE_USER = CommonConfiguration.SessionUserName();

                        tdmapEntities.SaveChanges();

                        result[0] = "true";
                        result[1] = "";
                        result[2] = "";                   
              
                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "USER_NOT_FOUND", lang);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-4";
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] ChangePassword(String oldPassword, String newPassword, String username )
        {
            try
            {
                String[] result = { "true", "", "" };

                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
                if (oldPassword.Equals(""))
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = String.Format(requiredErrMsg, "UserPassword");
                    return result;
                }


                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                String oldPwdHash = StringResources.MD5Hash(oldPassword);

                USERS userObj =
                    tdmapEntities.USERS
                        .Where(
                            user => (
                                        user.USR_PWD.Equals(oldPwdHash)&&
                                        user.USR_USERNAME.Equals(username)
                                    ) &&
                                        user.USR_DELETED != 1
                        ).SingleOrDefault();

                if (userObj != null)
                {
                    userObj.USR_PWD = StringResources.MD5Hash(newPassword);
                    
                    userObj.USR_UPDATE_DATETIME = StringResources.GetLongCurrentDateTime();
                    userObj.USR_UPDATE_USER = CommonConfiguration.SessionUserName();

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";
                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "USER_NOT_FOUND", lang);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-4";
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Delete()
        {
            try
            {
                String[] result = { "true", "", "" };

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                String upperUserName = this.UserName.ToUpper(culture);
                String lowerUserName = this.UserName.ToLower(culture);

                USERS userObj = tdmapEntities.USERS.Where(
                    user =>
                        (user.USR_USERNAME.Equals(this.UserName) ||
                            user.USR_USERNAME.Equals(upperUserName) ||
                            user.USR_USERNAME.Equals(lowerUserName)
                        ) && user.USR_DELETED != 1
                    ).SingleOrDefault();

                if (userObj != null)
                {
                    userObj.USR_DELETED = 1;
                    userObj.USR_UPDATE_DATETIME = StringResources.GetLongCurrentDateTime();
                    userObj.USR_UPDATE_USER = CommonConfiguration.SessionUserName();
                    tdmapEntities.SaveChanges();
                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";
                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "USER_NOT_FOUND", lang);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = { "false", "-2", ex.Message };
                return result;
            }
        }

        public String[] BlockUserLogin()
        {
            try
            {
                String[] result = { "true", "", "" };

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                String upperUserName = this.UserName.ToUpper(culture);
                String lowerUserName = this.UserName.ToLower(culture);

                USERS userObj = tdmapEntities.USERS.Where(
                    user =>
                        (user.USR_USERNAME.Equals(this.UserName) ||
                            user.USR_USERNAME.Equals(upperUserName) ||
                            user.USR_USERNAME.Equals(lowerUserName)
                        ) && user.USR_DELETED != 1
                    ).SingleOrDefault();

                if (userObj != null)
                {
                    userObj.USR_STATUS = Int16.Parse(UserSessionData.USER_STATUS_BLOCKED);
                    userObj.USR_UPDATE_DATETIME = StringResources.GetLongCurrentDateTime();
                    userObj.USR_UPDATE_USER = CommonConfiguration.SessionUserName();
                    tdmapEntities.SaveChanges();
                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";
                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "USER_NOT_FOUND", lang);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = { "false", "-2", ex.Message };
                return result;
            }
        }

        public String[] UnBlockUserLogin()
        {
            try
            {
                String[] result = { "true", "", "" };

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                String upperUserName = this.UserName.ToUpper(culture);
                String lowerUserName = this.UserName.ToLower(culture);

                USERS userObj = tdmapEntities.USERS.Where(
                    user =>
                        (user.USR_USERNAME.Equals(this.UserName) ||
                            user.USR_USERNAME.Equals(upperUserName) ||
                            user.USR_USERNAME.Equals(lowerUserName)
                        ) && user.USR_DELETED != 1
                    ).SingleOrDefault();

                if (userObj != null)
                {
                    userObj.USR_STATUS = Int16.Parse(UserSessionData.USER_STATUS_ACTIVE);
                    userObj.USR_UPDATE_DATETIME = StringResources.GetLongCurrentDateTime();
                    userObj.USR_UPDATE_USER = CommonConfiguration.SessionUserName();
                    tdmapEntities.SaveChanges();
                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";
                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "USER_NOT_FOUND", lang);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = { "false", "-2", ex.Message };
                return result;
            }
        }

        public String[] RenewPwd()
        {
            try
            {
                String[] result = { "true", "", "" };

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                String upperUserName = this.UserName.ToUpper(culture);
                String lowerUserName = this.UserName.ToLower(culture);

                USERS userObj = tdmapEntities.USERS.Where(
                    user =>
                        (user.USR_USERNAME.Equals(this.UserName) ||
                            user.USR_USERNAME.Equals(upperUserName) ||
                            user.USR_USERNAME.Equals(lowerUserName)
                        ) && user.USR_DELETED != 1
                    ).SingleOrDefault();

                if (userObj != null)
                {
                    Random rng = new Random();
                    String newPwd = StringResources.RandomStrings(8, 8, 1, rng).First<string>();
                    String hashedPwd = StringResources.MD5Hash(newPwd);

                    userObj.USR_PWD = hashedPwd;
                    userObj.USR_STATUS = Int16.Parse(UserSessionData.USER_STATUS_ACTIVE);
                    userObj.USR_PWD_RETRY_CNT = 0;
                    userObj.USR_UPDATE_DATETIME = StringResources.GetLongCurrentDateTime();
                    userObj.USR_UPDATE_USER = CommonConfiguration.SessionUserName();
                    userObj.USR_MUST_CHANGE_PWD = 1;
                    tdmapEntities.SaveChanges();
                    result[0] = "true";
                    result[1] = "";
                    result[2] = newPwd;
                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "USER_NOT_FOUND", lang);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = { "false", "-2", ex.Message };
                return result;
            }
        }

        public String[] SendPwd(String email)
        {
            try
            {
                String[] result = { "true", "", "" };

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                String UserEMail = this.UserEMail;
                

                USERS userObj = tdmapEntities.USERS.Where(
                    user =>
                        (user.USR_EMAIL.Equals(email)
                           
                        ) && user.USR_DELETED != 1
                    ).SingleOrDefault();

                if (userObj != null)
                {
                    Random rng = new Random();
                    String newPwd = StringResources.RandomStrings(8, 8, 1, rng).First<string>();
                    String hashedPwd = StringResources.MD5Hash(newPwd);
                  
                    String mailFrom = WebConfigurationManager.AppSettings["smtpUser"];
                    String mailPwd = WebConfigurationManager.AppSettings["smtpPass"];
                    String smtpServer = WebConfigurationManager.AppSettings["smtpServer"];
                    String smtpPort = WebConfigurationManager.AppSettings["smtpPort"];
                    
                    SmtpClient client = new SmtpClient(smtpServer, int.Parse(smtpPort));
                    MailMessage message = new MailMessage();
                    String body = String.Format(StringResources.getString(StringResources.RES_MSG, "MAIL_BODY_MSG", lang), userObj.USR_NAME, newPwd);
                    message.To.Add(email);
                    message.From = new MailAddress(mailFrom);
                    message.Subject = StringResources.getString(StringResources.RES_LABEL, "MAIL_SUBJECT", lang);
                    message.Body = body;
                    NetworkCredential security = new NetworkCredential(mailFrom, mailPwd);
                    client.Credentials = security;
                    client.EnableSsl = true;
                    client.Send(message);

                    userObj.USR_PWD = hashedPwd;
                    userObj.USR_STATUS = Int16.Parse(UserSessionData.USER_STATUS_ACTIVE);
                    userObj.USR_PWD_RETRY_CNT = 0;
                    userObj.USR_UPDATE_DATETIME = StringResources.GetLongCurrentDateTime();
                    userObj.USR_UPDATE_USER = CommonConfiguration.SessionUserName();
                    userObj.USR_MUST_CHANGE_PWD = 1;
                    tdmapEntities.SaveChanges();
                    result[0] = "true";
                    result[1] = "";
                    result[2] = newPwd;
                }
                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "REMEMBER_PWG_OK_MSG", lang);
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = { "false", "-2", ex.Message };
                return result;
            }
        
        }

        public String[] ChangePwd(String username,String oldpwd, String newpwd)
        {

            try
            {
                String[] result = { "true", "", "" };
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                USERS userObj = null;
                
                String requiredErrMsg = StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang);
          

                this.UserStatus = Int16.Parse(UserSessionData.USER_STATUS_ACTIVE);

                String userPassword = StringResources.MD5Hash(oldpwd);
                String upperUserName = username.ToUpper(culture);
                String lowerUserName = username.ToLower(culture);

                userObj = tdmapEntities.USERS.Where(
                            user => (
                                        user.USR_USERNAME.Equals(username) ||
                                        user.USR_USERNAME.Equals(upperUserName) ||
                                        user.USR_USERNAME.Equals(lowerUserName)

                                     ) &&
                                     user.USR_PWD.Equals(userPassword)
                                        &&
                                        user.USR_DELETED != 1
                            ).SingleOrDefault();

                if (userObj != null)
                {
                    newpwd = StringResources.MD5Hash(newpwd);
                    userObj.USR_PWD = newpwd;
                    userObj.USR_UPDATE_DATETIME = StringResources.GetLongCurrentDateTime();
                    userObj.USR_UPDATE_USER = CommonConfiguration.SessionUserName();
                    userObj.USR_MUST_CHANGE_PWD = 0;

                    tdmapEntities.SaveChanges();

                    result[0] = "true";
                    result[1] = "";
                    result[2] = "";
                }

                else
                {
                    result[0] = "false";
                    result[1] = "-1";
                    result[2] = StringResources.getString(StringResources.RES_ERR, "USER_INV_PWD", lang);

                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-2";
                result[2] = ex.Message;
                return result;
            }
        }

        public String[] Login(out UserObject loginUserObject)
        {
            try
            {
                int iUserStatusActive = Int16.Parse(UserSessionData.USER_STATUS_ACTIVE);

                String[] result = new String[3];

                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                String userPassword = StringResources.MD5Hash(this.UserPassword);
                String upperUserName = this.UserName.ToUpper(culture);
                String lowerUserName = this.UserName.ToLower(culture);
                USERS loginUser;

                loginUser = tdmapEntities.USERS.Where(
                    user => (
                        user.USR_USERNAME.Equals(this.UserName) ||
                        user.USR_USERNAME.Equals(upperUserName) ||
                        user.USR_USERNAME.Equals(lowerUserName))
                        &&
                        user.USR_PWD == userPassword &&
                        user.USR_DELETED == 0
                    ).FirstOrDefault();

                if (loginUser != null)
                {
                    loginUserObject = new UserObject();

                    if (loginUser.USR_STATUS == Int16.Parse(UserSessionData.USER_STATUS_BLOCKED))
                    {
                        result[0] = "false";
                        result[1] = "-1";
                        result[2] = StringResources.getString(StringResources.RES_ERR, "LOGIN_PAGE_INV_USER_STATUS_BLOCKED", lang);
                    }
                    else if (loginUser.USR_STATUS == Int16.Parse(UserSessionData.USER_STATUS_PWD_BLOCKED))
                    {
                        result[0] = "false";
                        result[1] = "-2";
                        result[2] = StringResources.getString(StringResources.RES_ERR, "LOGIN_PAGE_INV_USER_STATUS_PWD_BLOCKED", lang);
                    }
                    else if (loginUser.USR_STATUS == Int16.Parse(UserSessionData.USER_STATUS_ACTIVE))
                    {
                        loginUser.USR_PWD_RETRY_CNT = 0;
                        loginUser.USR_LAST_LOGIN_DATETIME = StringResources.GetLongCurrentDateTime();
                        tdmapEntities.SaveChanges();

                        loginUserObject = new UserObject();
                        loginUserObject.UserDeleted = short.Parse(loginUser.USR_DELETED.ToString());
                        loginUserObject.UserEMail = loginUser.USR_EMAIL;
                        loginUserObject.UserFirstName = loginUser.USR_NAME;
                        loginUserObject.UserInsertDateTime = Int64.Parse(loginUser.USR_INSERT_DATETIME.ToString());
                        loginUserObject.UserInsertUser = loginUser.USR_INSERT_USER;
                        loginUserObject.UserLastName = loginUser.USR_SURNAME;
                        loginUserObject.UserMidName = loginUser.USR_MID_NAME;
                        loginUserObject.UserMustChangePwd = short.Parse(loginUser.USR_MUST_CHANGE_PWD.ToString());
                        loginUserObject.UserName = loginUser.USR_USERNAME;
                        loginUserObject.UserPassword = loginUser.USR_PWD;
                        loginUserObject.UserRetryPwdCount = short.Parse(loginUser.USR_PWD_RETRY_CNT.ToString());
                        loginUserObject.UserStatus = short.Parse(loginUser.USR_STATUS.ToString());
                        loginUserObject.UserUpdateDateTime = Int64.Parse(loginUser.USR_UPDATE_DATETIME.ToString());
                        loginUserObject.UserLastLoginDateTime = Int64.Parse(loginUser.USR_LAST_LOGIN_DATETIME.ToString());
                        loginUserObject.UserUpdateUser = loginUser.USR_UPDATE_USER;
                        loginUserObject.UserProfileImage = loginUser.USR_PROFILE_IMG;
                        loginUserObject.UserRole = loginUser.USR_ROLE;

                        result[0] = "true";
                        result[1] = "";
                        result[2] = "";
                    }
                    else
                    {
                        result[0] = "false";
                        result[1] = "-3";
                        result[2] = StringResources.getString(StringResources.RES_ERR, "LOGIN_PAGE_INV_USER_STATUS", lang);

                    }
                }
                else
                {
                    loginUserObject = new UserObject();

                    USERS tmpUsers = tdmapEntities.USERS.Where(
                        user =>
                            (user.USR_USERNAME.Equals(this.UserName) || user.USR_USERNAME.Equals(upperUserName) || user.USR_USERNAME.Equals(lowerUserName)) &&
                            user.USR_DELETED == 0
                            && user.USR_STATUS == iUserStatusActive
                    ).FirstOrDefault();


                    if (tmpUsers != null)
                    {
                        tmpUsers.USR_PWD_RETRY_CNT++;

                        if (tmpUsers.USR_PWD_RETRY_CNT == 6)
                        {
                            tmpUsers.USR_STATUS = Int16.Parse(UserSessionData.USER_STATUS_PWD_BLOCKED);
                            result[0] = "false";
                            result[1] = "-7";
                            result[2] = StringResources.getString(StringResources.RES_ERR, "LOGIN_PAGE_INV_USER_BLOCK_PWD", lang);
                        }
                        else
                        {
                            String invPwdMsg = StringResources.getString(StringResources.RES_ERR, "LOGIN_PAGE_INV_USER_PWD", lang);
                            invPwdMsg = string.Format(invPwdMsg, (5 - tmpUsers.USR_PWD_RETRY_CNT).ToString());
                            result[0] = "false";
                            result[1] = "-8";
                            result[2] = invPwdMsg;
                        }

                        tdmapEntities.SaveChanges();
                    }
                    else
                    {
                        result[0] = "false";
                        result[1] = "-9";
                        result[2] = StringResources.getString(StringResources.RES_ERR, "LOGIN_PAGE_INV_USER_PWD_INV", lang);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                String[] result = new String[3];
                result[0] = "false";
                result[1] = "-10";
                result[2] = ex.Message;

                loginUserObject = new UserObject();
                return result;
            }
        }

        public UserObject GetUserByUserName(String userName)
        {
            UserObject tmpObj = null;
            try
            {
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();
                String upperUserName = userName.ToUpper(culture);
                String lowerUserName = userName.ToLower(culture);

                USERS userObj = tdmapEntities.USERS.Where(
                    user => (user.USR_USERNAME.Equals(this.UserName) ||
                            user.USR_USERNAME.Equals(upperUserName) ||
                            user.USR_USERNAME.Equals(lowerUserName)) &&
                            user.USR_DELETED != 1
                        ).SingleOrDefault();

                if (userObj != null)
                {
                    tmpObj = new UserObject();
                    tmpObj.UserName = userObj.USR_USERNAME;
                    tmpObj.UserEMail = userObj.USR_EMAIL;
                    tmpObj.UserFirstName = userObj.USR_NAME;
                    tmpObj.UserMidName = userObj.USR_MID_NAME;
                    tmpObj.UserLastName = userObj.USR_SURNAME;
                    tmpObj.UserProfileImage = userObj.USR_PROFILE_IMG;
                    tmpObj.UserRetryPwdCount = short.Parse(userObj.USR_PWD_RETRY_CNT.ToString());
                    tmpObj.UserStatus = short.Parse(userObj.USR_STATUS.ToString());
                    tmpObj.UserMustChangePwd = short.Parse(userObj.USR_MUST_CHANGE_PWD.ToString());
                    tmpObj.UserDeleted = short.Parse(userObj.USR_DELETED.ToString());
                    tmpObj.UserInsertDateTime = long.Parse(userObj.USR_INSERT_DATETIME.ToString());
                    tmpObj.UserInsertUser = userObj.USR_INSERT_USER;
                    tmpObj.UserUpdateDateTime = long.Parse(userObj.USR_UPDATE_DATETIME.ToString());
                    tmpObj.UserUpdateUser = userObj.USR_UPDATE_USER;
                    return tmpObj;
                }
                else
                {
                    tmpObj = null;
                    return tmpObj;
                }
            }
            catch (Exception ex)
            {
                tmpObj = null;
                return tmpObj;
            }
        }

        public void GenerateSessionParam()
        {
            this.SessionParam = "";
            String param = SessionParamsUtil.createParameter("UserObject.UserName", this.UserName);
            this.SessionParam = SessionParamsUtil.addToParameterTable(param);
        }

        public Boolean isLoggedInUser()
        {
            UserSessionData userInfoItem = HttpContext.Current.Session["UserSessionData"] != null ? (UserSessionData)HttpContext.Current.Session["UserSessionData"] : new UserSessionData();
            if (userInfoItem.Username.Equals(this.UserName))
                return true;
            else
                return false;
        }
    }
}