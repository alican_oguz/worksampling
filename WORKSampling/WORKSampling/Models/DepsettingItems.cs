﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class DepsettingItems
    {

        public List<DepsettingItem> DepsettingItemsList { get; set; }

        public DepsettingItems()
        {
            this.DepsettingItemsList = new List<DepsettingItem>();
        }

        public String[] Load()
        {
            String[] result = { "true", "", "" };

            try
            {
                List<DEPSETTINGS> depsettings = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.DepsettingItemsList = new List<DepsettingItem>();
                String username = CommonConfiguration.SessionUserName();


                depsettings =
                    tdmapEntities.DEPSETTINGS.Where(
                        lift => lift.DEPSETTINGS_DELETED != 1 && lift.DEPSETTINGS_INSERT_USER.Equals(username)
                    ).OrderBy(lift => lift.DEPSETTINGS_ID).ToList<DEPSETTINGS>();

                if (depsettings != null)
                {
                    foreach (DEPSETTINGS depsetting in depsettings)
                    {
                        DepsettingItem item = new DepsettingItem();
                        item.Id = depsetting.DEPSETTINGS_ID;
                        item.DepartmentId = depsetting.DEPT_ID;
                        item.MinObserNumber = depsetting.MIN_OBSERVATION_NUMBER;
                        item.StartShiftTime = depsetting.START_SHIFT_TIME;
                        item.EndShiftTime = depsetting.END_SHIFT_TIME;
                        item.StartBreakTime = depsetting.START_BREAK_TIME;
                        item.EndBreakTime = depsetting.END_BREAK_TIME;
                        item.InsertUser = depsetting.DEPSETTINGS_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(depsetting.DEPSETTINGS_INSERT_DATETIME.ToString());
                        item.UpdateUser = depsetting.DEPSETTINGS_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(depsetting.DEPSETTINGS_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(depsetting.DEPSETTINGS_DELETED.ToString());
                        item.LoadDisplayData();
                        this.DepsettingItemsList.Add(item);
                    }

                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] LoadByDepartmentId(Int64 dePartmentId)
        {
            String[] result = { "true", "", "" };

            try
            {
                List<DEPSETTINGS> depsettings = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.DepsettingItemsList = new List<DepsettingItem>();

                depsettings =
                    tdmapEntities.DEPSETTINGS.Where(
                        lift => lift.DEPSETTINGS_DELETED != 1
                            && lift.DEPT_ID == dePartmentId
                    ).OrderBy(lift => lift.DEPSETTINGS_ID).ToList<DEPSETTINGS>();

                if (depsettings != null)
                {
                    foreach (DEPSETTINGS depsetting in depsettings)
                    {
                        DepsettingItem item = new DepsettingItem();
                        item.Id = depsetting.DEPSETTINGS_ID;
                        item.DepartmentId = depsetting.DEPT_ID;
                        item.MinObserNumber = depsetting.MIN_OBSERVATION_NUMBER;
                        item.StartShiftTime = depsetting.START_SHIFT_TIME;
                        item.EndShiftTime = depsetting.END_SHIFT_TIME;
                        item.StartBreakTime = depsetting.START_BREAK_TIME;
                        item.EndBreakTime = depsetting.END_BREAK_TIME;
                        item.InsertUser = depsetting.DEPSETTINGS_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(depsetting.DEPSETTINGS_INSERT_DATETIME.ToString());
                        item.UpdateUser = depsetting.DEPSETTINGS_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(depsetting.DEPSETTINGS_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(depsetting.DEPSETTINGS_DELETED.ToString());
                        item.LoadDisplayData();
                        this.DepsettingItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }


    }
}