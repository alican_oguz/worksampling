﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;

namespace _2DMap.Models
{
    public class ObservationItems
    {

        public List<ObservationItem> ObservationItemsList { get; set; }

        public ObservationItems()
        {
            this.ObservationItemsList = new List<ObservationItem>();
        }

        public String[] Load()
        {
            String[] result = { "true", "", "" };

            try
            {
                List<ACTIVITIES> observations = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.ObservationItemsList = new List<ObservationItem>();
                String username = CommonConfiguration.SessionUserName();
                observations =
                    tdmapEntities.ACTIVITIES.Where(
                        lift => lift.ACT_DELETED != 1 && lift.ACT_INSERT_USER.Equals(username)
                    ).OrderBy(lift => lift.ACT_NAME).ToList<ACTIVITIES>();

                if (observations != null)
                {
                    foreach (ACTIVITIES observation in observations)
                    {
                        ObservationItem item = new ObservationItem();
                        item.Id = observation.ACT_ID;
                        item.DepartmentId = observation.DEPT_ID;
                        item.ObclassId = observation.ACT_TYPE_ID;
                        item.WorkerId = observation.WORKERS_ID;
                        item.Name = observation.ACT_NAME;

                        item.InsertUser = observation.ACT_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(observation.ACT_INSERT_DATETIME.ToString());
                        item.UpdateUser = observation.ACT_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(observation.ACT_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(observation.ACT_DELETED.ToString());
                        item.LoadDisplayData();
                        this.ObservationItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] LoadByDepartmentId(Int64 dePartmentId)
        {
            String[] result = { "true", "", "" };

            try
            {
                List<ACTIVITIES> observations = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.ObservationItemsList = new List<ObservationItem>();

                observations =
                    tdmapEntities.ACTIVITIES.Where(
                        lift => lift.ACT_DELETED != 1
                            && lift.DEPT_ID == dePartmentId
                    ).OrderBy(lift => lift.ACT_NAME).ToList<ACTIVITIES>();

                if (observations != null)
                {
                    foreach (ACTIVITIES observation in observations)
                    {
                        ObservationItem item = new ObservationItem();
                        item.Id = observation.ACT_ID;
                        item.DepartmentId = observation.DEPT_ID;
                        item.ObclassId = observation.ACT_TYPE_ID;
                        item.WorkerId = observation.WORKERS_ID;
                        item.Name = observation.ACT_NAME;

                        item.InsertUser = observation.ACT_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(observation.ACT_INSERT_DATETIME.ToString());
                        item.UpdateUser = observation.ACT_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(observation.ACT_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(observation.ACT_DELETED.ToString());
                        item.LoadDisplayData();
                        this.ObservationItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] LoadByObclassId(Int64 obClassId)
        {
            String[] result = { "true", "", "" };

            try
            {
                List<ACTIVITIES> observations = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.ObservationItemsList = new List<ObservationItem>();

                observations =
                    tdmapEntities.ACTIVITIES.Where(
                        lift => lift.ACT_DELETED != 1
                            && lift.ACT_TYPE_ID == obClassId
                    ).OrderBy(lift => lift.ACT_NAME).ToList<ACTIVITIES>();

                if (observations != null)
                {
                    foreach (ACTIVITIES observation in observations)
                    {
                        ObservationItem item = new ObservationItem();
                        item.Id = observation.ACT_ID;
                        item.DepartmentId = observation.DEPT_ID;
                        item.ObclassId = observation.ACT_TYPE_ID;
                        item.WorkerId = observation.WORKERS_ID;
                        item.Name = observation.ACT_NAME;

                        item.InsertUser = observation.ACT_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(observation.ACT_INSERT_DATETIME.ToString());
                        item.UpdateUser = observation.ACT_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(observation.ACT_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(observation.ACT_DELETED.ToString());
                        item.LoadDisplayData();
                        this.ObservationItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

        public String[] LoadByWorkerId(Int64 worKerId)
        {
            String[] result = { "true", "", "" };

            try
            {
                List<ACTIVITIES> observations = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.ObservationItemsList = new List<ObservationItem>();

                observations =
                    tdmapEntities.ACTIVITIES.Where(
                        lift => lift.ACT_DELETED != 1
                            && lift.WORKERS_ID == worKerId
                    ).OrderBy(lift => lift.ACT_NAME).ToList<ACTIVITIES>();

                if (observations != null)
                {
                    foreach (ACTIVITIES observation in observations)
                    {
                        ObservationItem item = new ObservationItem();
                        item.Id = observation.ACT_ID;
                        item.DepartmentId = observation.DEPT_ID;
                        item.ObclassId = observation.ACT_TYPE_ID;
                        item.WorkerId = observation.WORKERS_ID;
                        item.Name = observation.ACT_NAME;

                        item.InsertUser = observation.ACT_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(observation.ACT_INSERT_DATETIME.ToString());
                        item.UpdateUser = observation.ACT_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(observation.ACT_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(observation.ACT_DELETED.ToString());
                        item.LoadDisplayData();
                        this.ObservationItemsList.Add(item);
                    }
                }
            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }


    }
}