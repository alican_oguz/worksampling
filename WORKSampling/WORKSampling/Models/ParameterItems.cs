﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using _2DMap.Utility;
namespace _2DMap.Models
{
    public class ParameterItems
    {
        public List<ParameterItem> ParameterItemsList { get; set; }

        public ParameterItems()
        {
            this.ParameterItemsList = new List<ParameterItem>();
        }

        public String[] Load()
        {
            String[] result = { "true", "", "" };

            try
            {
                List<PARAMETERS> parameters = null;
                WorkSamplingEntities tdmapEntities = new WorkSamplingEntities();

                this.ParameterItemsList = new List<ParameterItem>();

                parameters =
                    tdmapEntities.PARAMETERS.Where(
                        parameterr => parameterr.PARAMETER_DELETED != 1
                    ).OrderBy(parameterr => parameterr.PARAMETER).ToList<PARAMETERS>();

                if (parameters != null)
                {
                    foreach (PARAMETERS parameter in parameters)
                    {
                        ParameterItem item = new ParameterItem();
                        item.Id = parameter.PARAMETER_ID;
                        item.Parameter = parameter.PARAMETER;
                        item.DeptId = parameter.DEPT_ID;
                        item.InsertUser = parameter.PARAMETER_INSERT_USER;
                        item.InsertDateTime = Int64.Parse(parameter.PARAMETER_INSERT_DATETIME.ToString());
                        item.UpdateUser = parameter.PARAMETER_UPDATE_USER;
                        item.UpdateDateTime = Int64.Parse(parameter.PARAMETER_UPDATE_DATE_TIME.ToString());
                        item.Deleted = Int16.Parse(parameter.PARAMETER_DELETED.ToString());
                        this.ParameterItemsList.Add(item);
                    }
                }

            }
            catch (Exception ex)
            {
                result[0] = "false";
                result[1] = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                result[2] = ex.Message;
            }

            return result;
        }

    }
}