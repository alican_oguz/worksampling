﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _2DMap.Utility;
using _2DMap.Models;

namespace _2DMap.Controllers
{
    public class UtilityController : Controller
    {
        [NoCache]
        public ActionResult ShowChangePassword()
        {
            String lang = CommonConfiguration.SiteLanguage();
            ModalContent tempModalContent = new ModalContent();
            ErrorModalData tempErrorModal = (ErrorModalData)Session["ModalData"];

            tempModalContent.Create(tempErrorModal);
            Session.Remove("ModalData");

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("UserName", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_NAME", lang));
            FormLabels.Add("CurrentPassword", StringResources.getString(StringResources.RES_LABEL, "LBL_CURRENT_PASSWORD", lang));
            FormLabels.Add("NewPassword", StringResources.getString(StringResources.RES_LABEL, "LBL_NEW_PASSWORD", lang));
            FormLabels.Add("ComfirmPassword", StringResources.getString(StringResources.RES_LABEL, "LBL_COMFIRM_PASSWORD", lang));          
            ViewBag.FormLabels = FormLabels;

            ViewBag.sfreDegistr = StringResources.getString(StringResources.RES_LABEL, "TOP_MENU_CHANGE_PWD", lang);
            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CHANGE", lang);
            ViewBag.SubmitBtn2 = StringResources.getString(StringResources.RES_LABEL, "LBL_CANSEL", lang);
            ViewBag.SubmitBtn3 = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

            return PartialView("ShowChangePassword", tempModalContent);
        }

        [HttpPost]
        public JsonResult DoChangePassword()
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String oldPassword = Request["PswCur"];
            String newPassword = Request["PswComf"];
            String username = Request["UserName"];


            UserObject student = new UserObject();

            String [] result = new String[3];
            
            result = student.ChangePassword(oldPassword, newPassword,username);
            if (result[0].Equals("true"))
            {
                return Json(response);
            }
            else
            {
                response = new GenericJsonError(false, "", result[2], "");
                return Json(response);
            }
        }

        [NoCache]
        public ActionResult ShowInfoModal()
        {
            ModalContent tempModalContent = new ModalContent();
            ErrorModalData tempErrorModal = (ErrorModalData)Session["ModalData"];

            tempModalContent.Create(tempErrorModal);
            Session.Remove("ModalData");

            return PartialView("InfoModal", tempModalContent);
        }

        [NoCache]
        public ActionResult ShowDefaultErrorModal()
        {
            String errcode = Request["errcode"] != null ? (String)Request["errcode"] : "1";
            String errdesc = Request["errdesc"] != null ? (String)Request["errdesc"] : "";

            ModalContent tempModalContent = new ModalContent();

            tempModalContent.CreateDefaultErrorModal(errcode, errdesc);

            return PartialView("InfoModal", tempModalContent);
        }

        [NoCache]
        public ActionResult ShowInfoModalBodyMessage()
        {
            ModalContent tempModalContent = new ModalContent();
            String strModalMessage = (String)Session["ModalDataBodyMessage"];

            Session.Remove("ModalDataBodyMessage");

            ViewBag.Message = strModalMessage;

            return PartialView("InfoModalBodyMessage");
        }

        [NoCache]
        public JsonResult ConfirmAction(String actionurl, String confirmmsg, String confirmtitle)
        {
            String lang = CommonConfiguration.SiteLanguage();

            GenericJsonError response = new GenericJsonError(true, "", "0", "");

            Boolean hasError = false;
            String ConfirmModalTitle = StringResources.decodeString(confirmtitle);
            String LblCancel = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);
            String LblOk = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);
            String ConfirmErrTitle = StringResources.getString(StringResources.RES_ERR, "DEFAULT_ERR_MODAL_TITLE", lang);
            String ConfirmErrMsg = StringResources.getString(StringResources.RES_ERR, "CONFIRM_MODAL_ERROR", lang);

            if (actionurl == null || actionurl.Equals(""))
            {
                ConfirmErrMsg = String.Format(ConfirmErrMsg, "{ConfirmAction;Parameter;actionurl}");
                hasError = true;
            }
            else if (confirmmsg == null || confirmmsg.Equals(""))
            {
                ConfirmErrMsg = String.Format(ConfirmErrMsg, "{ConfirmAction;Parameter;confirmmsg}");
                hasError = true;
            }
            else if (confirmtitle == null || confirmtitle.Equals(""))
            {
                ConfirmErrMsg = String.Format(ConfirmErrMsg, "{ConfirmAction;Parameter;confirmtitle}");
                hasError = true;
            }

            if (hasError)
            {
                ErrorModalData infoModal = new ErrorModalData(
                    "infoModal", ConfirmErrTitle, "F",
                    ConfirmErrMsg,
                    LblOk, "default", "dismiss",
                    "", "", ""
                );

                Session["ModalData"] = infoModal;
                response = new GenericJsonError(false, "", "0", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                String ActionUrl = StringResources.decodeString(actionurl);
                String ConfirmMsg = StringResources.decodeString(confirmmsg);
                String LblProcessing = "...";

                ErrorModalData infoModal = new ErrorModalData(
                    "infoModal", ConfirmModalTitle, "I",
                    ConfirmMsg,
                    LblOk, "primary", "DoConfirmedAction('" + ActionUrl + "')",
                    LblCancel, "default", "dismiss"
                );

                infoModal.Btn1Loading = LblProcessing;

                Session["ModalData"] = infoModal;

                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }
    
    }
}
