﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using _2DMap.Utility;
using _2DMap.Models;

namespace _2DMap.Controllers
{
    public class TestMngController : Controller
    {

         [CustomAuthorize]
         public ActionResult ObservationList()
         {
             String lang = CommonConfiguration.SiteLanguage();
             SessionParamsUtil.ClearParameterTable();

             PageDefinitonData pData = new PageDefinitonData("ObservationList", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.SaveBreadCrumbData(Session);
             List<TestObservationItem> itemList = new List<TestObservationItem>();
             TestObservationItem item = new TestObservationItem();
             String[] opResult = item.Load(out itemList);

             List<String> TableHeaders = new List<String>();
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_NAME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS_START_DATE", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS_START_TIME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
             ViewBag.TableHeaders = TableHeaders;

             ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_ADD_NEW", lang);
             ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);

             ViewBag.DeleteConfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_OBSERVATION_DELETE", lang);
             ViewBag.HasError = "0";

             if (opResult[0].Equals("true"))
             {
                 ViewBag.TableData = itemList;
             }
             else
             {
                 String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_LIST", lang);
                 String modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), opResult[2]);
                 String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "dismiss",
                     "", "", "");

                 Session["ModalData"] = err;

                 ViewBag.HasError = "1";
                 ViewBag.TableData = new List<ObservationItem>();
             }

             return View();
         }

         [CustomAuthorize]
         public ActionResult ObservationAdd()
         {
             String lang = CommonConfiguration.SiteLanguage();

             PageDefinitonData pData = new PageDefinitonData("ObservationAdd", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.AddBreadCrumbItem("ObservationList", true);
             pData.SaveBreadCrumbData(Session);


             Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
             DepartmentItems items = new DepartmentItems();
             items.Load();
             if (items.DepartmentItemsList != null)
             {
                 foreach (DepartmentItem item in items.DepartmentItemsList)
                 {
                     if (!dePartmentsDict.ContainsKey(item.Id.ToString()))
                         dePartmentsDict.Add(item.Id.ToString(), item.Name);
                 }
             }

             Dictionary<String, String> obClassesDict = new Dictionary<string, string>();
             ObclassItems obitems = new ObclassItems();
             obitems.Load();
             if (obitems.ObclassItemsList != null)
             {
                 foreach (ObclassItem obitem in obitems.ObclassItemsList)
                 {
                     if (!obClassesDict.ContainsKey(obitem.Id.ToString()))
                         obClassesDict.Add(obitem.Id.ToString(), obitem.Name);
                 }
             }

             Dictionary<String, String> worKersDict = new Dictionary<string, string>();
             WorkerItems workeritems = new WorkerItems();
             workeritems.Load();
             if (workeritems.WorkerItemsList != null)
             {
                 foreach (WorkerItem workeritem in workeritems.WorkerItemsList)
                 {
                     if (!worKersDict.ContainsKey(workeritem.Id.ToString()))
                         worKersDict.Add(workeritem.Id.ToString(), workeritem.Name);
                 }
             }
             



             Dictionary<String, String> FormLabels = new Dictionary<String, String>();
             FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             FormLabels.Add("ObclassId", StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_NAME", lang));
             FormLabels.Add("WorkerId", StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_NAME", lang));
             FormLabels.Add("ObservationName", StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_NAME", lang));
             ViewBag.FormLabels = FormLabels;

             ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
             ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
             ViewBag.DepartmentsDict = dePartmentsDict;
             ViewBag.ObclassesDict = obClassesDict;
             ViewBag.WorkersDict = worKersDict;


             return View();
         }

         [CustomAuthorize]
         public ActionResult ObservationReportAdd(String refcode)
         {
             String lang = CommonConfiguration.SiteLanguage();

             PageDefinitonData pData = new PageDefinitonData("ObservationReportAdd", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.AddBreadCrumbItem("ObservationList", true);
             pData.SaveBreadCrumbData(Session);

             GenericJsonError response = new GenericJsonError(true, "", "", "");
             Dictionary<String, String> parameters = new Dictionary<string, string>();
             Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

             String ObservationId = parameters["TestObservationItem.Id"];
             Int64 iObservationId = -1;
             Int64.TryParse(ObservationId, out iObservationId);

              List<TestObservationDataItem>  observationDataList = new List<TestObservationDataItem>();
             TestObservationItem testObj = new TestObservationItem();
             
             testObj.LoadData(iObservationId, out observationDataList);

             if(observationDataList.Count == 0)
                 return RedirectToAction("ObservationList");

             TestObservationDataItem observationData = observationDataList.First();

             ParameterItems parameterList = new ParameterItems();
             String[] opResult = parameterList.Load();



             Dictionary<String, String> FormLabels = new Dictionary<String, String>();
             FormLabels.Add("ObservationName", StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_NAME", lang));
             ViewBag.FormLabels = FormLabels;

             ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
             ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
             ViewBag.ObservationData = observationData;
             ViewBag.ParameterItemsList = parameterList.ParameterItemsList;

            

             return View();
         }


         [NoCache]
         public JsonResult DoObservationAdd(List<FormDataNameValue> formdatapair)
         {
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_ADD_NEW", lang);
             String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             if (formdatapair == null)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoObservationAdd:SP:Error:1");
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoObservationAdd:SP:Error:1");

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "window.location='/test/observation/list'",
                     "", "", "");

                 Session["ModalData"] = err;

                 return Json(response);
             }

             try
             {
                 TestObservationItem newItem = new TestObservationItem(formdatapair);
                 String[] opResult = newItem.Create();
                 if (opResult[0].Equals("true"))
                 {
                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "S", modalBody, modalClose,
                         "default", "window.location.href='/test/observation/list'",
                         "", "", "");

                     Session["ModalData"] = err;

                     return Json(response);
                 }
                 else if (opResult[0].Equals("false"))
                 {
                     String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                     modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
                 else
                 {
                     String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
             }
             catch (Exception ex)
             {
                 String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", errCode, ex.Message);
                 return Json(response);
             }

         }


         [NoCache]
         public JsonResult DoObservationReportAdd(List<FormDataNameValue> formdatapair)
         {
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_DATA_EDIT", lang);
             String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             if (formdatapair == null)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoObservationReportAdd:SP:Error:1");
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoObservationReportAdd:SP:Error:1");

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "window.location.href = window.location.href",
                     "", "", "");

                 Session["ModalData"] = err;

                 return Json(response);
             }

             try
             {
                 TestObservationDataItem newItem = new TestObservationDataItem(formdatapair);
                 String[] opResult = newItem.Update();
                 if (opResult[0].Equals("true"))
                 {
                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "S", modalBody, modalClose,
                         "default", "window.location.href = window.location.href",
                         "", "", "");

                     Session["ModalData"] = err;

                     return Json(response);
                 }
                 else if (opResult[0].Equals("false"))
                 {
                     String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                     modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
                 else
                 {
                     String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
             }
             catch (Exception ex)
             {
                 String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", errCode, ex.Message);
                 return Json(response);
             }

         }

         private GenericJsonError GetObservationIdBySessionRefCode(String refcode, out String observationId, out String dePartmentId, out String obClassId, out String worKerId)
         {
             Dictionary<String, String> parameters = new Dictionary<string, string>();
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();
             String backLink = "window.location='/test/observation/list'";
             observationId = "";
             dePartmentId = "";
             obClassId = "";
             worKerId = "";

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_EDIT", lang);
             String modalBody = "";
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             Boolean hasError = false;
             Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

             if (!hasError && !isSessionParamsValid)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoGetObservationEdit:SP:Error:1");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoGetObservationEdit:SP:Error:1");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("TestObservationItem.Id"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.Id");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:2");
                 hasError = true;
             }


             if (!hasError)
             {
                 observationId = parameters["TestObservationItem.Id"];

                 if (observationId == null || observationId.Equals(""))
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.Id");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:3");
                     hasError = true;
                 }

             }

             if (hasError)
             {
                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", backLink,
                     "", "", "");

                 Session["ModalData"] = err;

                 return response;
             }

             return new GenericJsonError(true, "", "", "");
         }

         [NoCache]
         public JsonResult DoObservationDelete(String refcode)
         {
             String lang = CommonConfiguration.SiteLanguage();
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             Dictionary<String, String> parameters = new Dictionary<string, string>();

             Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
             if (!isUserSessionValid)
             {
                 response = new GenericJsonError(false, "", "timeout", "");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

             if (!isSessionParamsValid)
             {
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             response = new GenericJsonError(true, "", "", "");

             if (!parameters.ContainsKey("ObservationItem.Id"))
             {
                 response = new GenericJsonError(false, "", "", "DoObservationDelete:UserObj:Err:1");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             String ObservationId = parameters["ObservationItem.Id"];
             Int64 iObservationId = -1;
             Int64.TryParse(ObservationId, out iObservationId);

             if (ObservationId == null || ObservationId.Equals("") || iObservationId < 0)
             {
                 response = new GenericJsonError(false, "", "", "DoObservationDelete:UserObj:Err:2");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             ObservationItem deleteItem = new ObservationItem();
             deleteItem.Id = iObservationId;
             String[] result = deleteItem.Delete();
             if (result[0].Equals("false"))
             {
                 response = new GenericJsonError(false, "", result[1], result[2]);
                 return Json(response, JsonRequestBehavior.AllowGet);
             }
             else
             {
                 return Json(response, JsonRequestBehavior.AllowGet);
             }
         }

    }
}
