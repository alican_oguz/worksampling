﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using _2DMap.Utility;
using _2DMap.Models;

namespace _2DMap.Controllers
{
    public class ParameterController : Controller
    {
        [CustomAuthorize]
        public ActionResult ParameterList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("ParameterList", false);
            pData.AddBreadCrumbItem("ParameterMng", false);
            pData.SaveBreadCrumbData(Session);

            ParameterItems parameters = new ParameterItems();
            String[] opResult = parameters.Load();

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add("Departman Adı");
            TableHeaders.Add("Parametre");
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
            ViewBag.TableHeaders = TableHeaders;

            ViewBag.AddNew = "Yeni Parametre Ekle";
            ViewBag.EditTip = StringResources.getString(StringResources.RES_LABEL, "LBL_EDIT", lang);
            ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);

            ViewBag.DeleteConfirmMsg = "İlgili parametreyi silmek istediğinize emin misiniz?";
            ViewBag.HasError = "0";

            if (opResult[0].Equals("true"))
            {
                ViewBag.TableData = parameters.ParameterItemsList;
            }
            else
            {
                String modalHeader = "Parametre Listesi";
                String modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), opResult[2]);
                String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                ViewBag.HasError = "1";
                ViewBag.TableData = new List<ParameterItems>();
            }

            return View();
        }

        [CustomAuthorize]
        public ActionResult ParameterAdd()
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("ParameterAdd", false);
            pData.AddBreadCrumbItem("ParameterMng", false);
            pData.AddBreadCrumbItem("ParameterList", true);
            pData.SaveBreadCrumbData(Session);


            Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
            CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dePartmentsDict);

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("Parameter","Parametre");
            FormLabels.Add("Departmant", "Departman");
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
            ViewBag.DepartmentsDict = dePartmentsDict;

            return View();
        }

        [NoCache]
        public JsonResult DoParameterAdd(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_PARAMETER_ADD_NEW", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoParameterAdd:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoParameterAdd:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "window.location='/parameter/list'",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                ParameterItem newItem = new ParameterItem(formdatapair);
                String[] opResult = newItem.Create();
                if (opResult[0].Equals("true"))
                {
                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "S", modalBody, modalClose,
                        "default", "window.location.href='/parameter/list'",
                        "", "", "");

                    Session["ModalData"] = err;

                    return Json(response);
                }
                else if (opResult[0].Equals("false"))
                {
                    String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }

        }

        [NoCache]
        public JsonResult DoParameterDelete(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            Dictionary<String, String> parameters = new Dictionary<string, string>();

            Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
            if (!isUserSessionValid)
            {
                response = new GenericJsonError(false, "", "timeout", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!isSessionParamsValid)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response = new GenericJsonError(true, "", "", "");

            if (!parameters.ContainsKey("ParameterItem.Id"))
            {
                response = new GenericJsonError(false, "", "", "DoParameterDelete:ParamObj:Err:1");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String ParameterId = parameters["ParameterItem.Id"];
            Int64 iParameterId = -1;
            Int64.TryParse(ParameterId, out iParameterId);

            if (ParameterId == null || ParameterId.Equals("") || iParameterId < 0)
            {
                response = new GenericJsonError(false, "", "", "DoParameterDelete:ParamObj:Err:2");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            ParameterItem deleteItem = new ParameterItem();
            deleteItem.Id = iParameterId;
            String[] result = deleteItem.Delete();
            if (result[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", result[1], result[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }




    }
}
