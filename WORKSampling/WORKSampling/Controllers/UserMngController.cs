﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using _2DMap.Utility;
using _2DMap.Models;

namespace _2DMap.Controllers
{
    public class UserMngController : Controller
    {
        [CustomAuthorize]
        public ActionResult UserList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("UserList", false);
            pData.AddBreadCrumbItem("UserMng", false);
            pData.SaveBreadCrumbData(Session);

            UserObjectSet userObjectSet = new UserObjectSet();
            userObjectSet.Load();
            List<UserObject> userList = userObjectSet.UserObjectList;
            ViewBag.userList = userList;

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_USER_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_USER_FULL_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_USER_LAST_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EMAIL", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_STATUS", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
            ViewBag.TableHeaders = TableHeaders;

            ViewBag.DeleteMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_USER_DELETE", lang);
            ViewBag.BlockMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_USER_BLOCK", lang);
            ViewBag.UnblocMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_USER_UNBLOCK", lang);
            ViewBag.RenewPwdMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_USER_RENEW_PWD", lang);

            ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);
            ViewBag.EditTip = StringResources.getString(StringResources.RES_LABEL, "LBL_EDIT", lang);
            ViewBag.BlockTip = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_BLOCK", lang);
            ViewBag.UnBlockTip = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_UNBLOCK", lang);
            ViewBag.RenewPwdTip = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_RENEW_PWD", lang);

            ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_ADD_NEW", lang);

            return View();
        }

        [NoCache]
        public JsonResult DeleteUser(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            Dictionary<String, String> parameters = new Dictionary<string,string>();

            Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
            if (!isUserSessionValid)
            {
                response = new GenericJsonError(false, "", "timeout", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!isSessionParamsValid)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response = new GenericJsonError(true, "", "", "");

            if (!parameters.ContainsKey("UserObject.UserName"))
            { 
                response = new GenericJsonError(false, "", "", "DoDeleteUser:UserObj:Err:1"); 
                return Json(response, JsonRequestBehavior.AllowGet); 
            }

            String UserName = parameters["UserObject.UserName"];

            if ( UserName == null || UserName.Equals("") )
            {
                response = new GenericJsonError(false, "", "", "DoDeleteUser:UserObj:Err:2"); 
                return Json(response, JsonRequestBehavior.AllowGet); 
            }

            UserObject userObject = new UserObject();
            userObject = userObject.GetUserByUserName(UserName);

            if ( userObject == null || !userObject.UserName.Equals(UserName) )
            {
                response = new GenericJsonError(false, "", "", "DoDeleteUser:UserObj:Err:3");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String[] result = {"true","",""};

            result = userObject.Delete();
            if (result[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", result[1], result[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [NoCache]
        public JsonResult BlockUser(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            Dictionary<String, String> parameters = new Dictionary<string, string>();

            Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
            if (!isUserSessionValid)
            {
                response = new GenericJsonError(false, "", "timeout", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!isSessionParamsValid)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response = new GenericJsonError(true, "", "", "");

            if (!parameters.ContainsKey("UserObject.UserName"))
            {
                response = new GenericJsonError(false, "", "", "DoBlockUser:UserObj:Err:1");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String UserName = parameters["UserObject.UserName"];

            if (UserName == null || UserName.Equals(""))
            {
                response = new GenericJsonError(false, "", "", "DoBlockUser:UserObj:Err:2");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            UserObject userObject = new UserObject();
            userObject = userObject.GetUserByUserName(UserName);

            if (userObject == null || !userObject.UserName.Equals(UserName))
            {
                response = new GenericJsonError(false, "", "", "DoBlockUser:UserObj:Err:3");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String[] result = { "true", "", "" };

            result = userObject.BlockUserLogin();
            if (result[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", result[1], result[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [NoCache]
        public JsonResult UnBlockUser(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            Dictionary<String, String> parameters = new Dictionary<string, string>();

            Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
            if (!isUserSessionValid)
            {
                response = new GenericJsonError(false, "", "timeout", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!isSessionParamsValid)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response = new GenericJsonError(true, "", "", "");

            if (!parameters.ContainsKey("UserObject.UserName"))
            {
                response = new GenericJsonError(false, "", "", "DoUnBlockUser:UserObj:Err:1");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String UserName = parameters["UserObject.UserName"];

            if (UserName == null || UserName.Equals(""))
            {
                response = new GenericJsonError(false, "", "", "DoUnBlockUser:UserObj:Err:2");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            UserObject userObject = new UserObject();
            userObject = userObject.GetUserByUserName(UserName);

            if (userObject == null || !userObject.UserName.Equals(UserName))
            {
                response = new GenericJsonError(false, "", "", "DoUnBlockUser:UserObj:Err:3");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String[] result = { "true", "", "" };

            result = userObject.UnBlockUserLogin();
            if (result[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", result[1], result[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [NoCache]
        public JsonResult RenewPwd(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            Dictionary<String, String> parameters = new Dictionary<string, string>();

            Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
            if (!isUserSessionValid)
            {
                response = new GenericJsonError(false, "", "timeout", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!isSessionParamsValid)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response = new GenericJsonError(true, "", "", "");

            if (!parameters.ContainsKey("UserObject.UserName"))
            {
                response = new GenericJsonError(false, "", "", "DoRenewPwdUser:UserObj:Err:1");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String UserName = parameters["UserObject.UserName"];

            if (UserName == null || UserName.Equals(""))
            {
                response = new GenericJsonError(false, "", "", "DoRenewPwdUser:UserObj:Err:2");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            UserObject userObject = new UserObject();
            userObject = userObject.GetUserByUserName(UserName);

            if (userObject == null || !userObject.UserName.Equals(UserName))
            {
                response = new GenericJsonError(false, "", "", "DoRenewPwdUser:UserObj:Err:3");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String[] result = { "true", "", "" };

            result = userObject.RenewPwd();
            if (result[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", result[1], result[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                String respMsg = StringResources.getString(StringResources.RES_MSG, "USER_RENEW_PWD_SUCCESS", lang);
                respMsg = String.Format(respMsg, result[2]);
                response.resultmsg = "dispmsg";
                Session["ModalDataBodyMessage"] = respMsg;
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [CustomAuthorize]
        public ActionResult UserAdd()
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("UserAdd", false);
            pData.AddBreadCrumbItem("UserMng", false);
            pData.SaveBreadCrumbData(Session);

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("Username", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_NAME", lang));
            FormLabels.Add("FirstName", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_FULL_NAME", lang));
            FormLabels.Add("MidName", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_MID_NAME", lang));
            FormLabels.Add("LastName", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_LAST_NAME", lang));
            FormLabels.Add("Email", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EMAIL", lang));
            FormLabels.Add("Password", StringResources.getString(StringResources.RES_LABEL, "LBL_PWD", lang));
            FormLabels.Add("PasswordRe", StringResources.getString(StringResources.RES_LABEL, "LBL_PWD_RE", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);

            return View();
        }

        [NoCache]
        public JsonResult DoUserAdd(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_ADD_NEW", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoUserAdd:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoUserAdd:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader, 
                    "F", modalBody, modalClose, 
                    "default", "dismiss", 
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                UserObject newUser = new UserObject(formdatapair);
                String[] opResult = newUser.Create();
                if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("true"))
                {
                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader, 
                        "S", modalBody, modalClose, 
                        "default", "window.location.href='/usr/list'", 
                        "", "", "");

                    Session["ModalData"] = err;

                    return Json(response);
                }
                else if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("false"))
                {
                    String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", ""); 
                    
                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }
        }

        [CustomAuthorize]
        public ActionResult UserEdit(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("UserEdit", false);
            pData.AddBreadCrumbItem("UserMng", false);
            pData.AddBreadCrumbItem("UserList", true);
            pData.SaveBreadCrumbData(Session);

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("Username", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_NAME", lang));
            FormLabels.Add("FirstName", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_FULL_NAME", lang));
            FormLabels.Add("MidName", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_MID_NAME", lang));
            FormLabels.Add("LastName", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_LAST_NAME", lang));
            FormLabels.Add("Email", StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EMAIL", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.CancelBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
            ViewBag.Refcode = refcode;
            return View();
        }

        [NoCache]
        public JsonResult DoGetUserEdit(String refcode)
        {
            UserObject userObject = new UserObject();
            String UserName = "";
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/usr/list'";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            GenericJsonError response = GetUserNameBySessionRefCode(refcode, out UserName);

            if (response.result == true)
            {
                userObject = userObject.GetUserByUserName(UserName);
                if (userObject == null || !userObject.UserName.Equals(UserName))
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "UserObject");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetUserEdit:SP:Error:5");
                }
                else
                {
                    List<EditFormDataItem> formItems = new List<EditFormDataItem>();
                    formItems.Add(new EditFormDataItem("Username", "text", userObject.UserName, true));
                    formItems.Add(new EditFormDataItem("FirstName", "text", userObject.UserFirstName, false));
                    formItems.Add(new EditFormDataItem("MidName", "text", userObject.UserMidName, false));
                    formItems.Add(new EditFormDataItem("LastName", "text", userObject.UserLastName, false));
                    formItems.Add(new EditFormDataItem("UserEmail", "text", userObject.UserEMail, false));
                    response.data1 = JsonConvert.SerializeObject(formItems);
                }
            }
            else
            {
                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response, JsonRequestBehavior.AllowGet);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [NoCache]
        public JsonResult DoUserEdit(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EDIT", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoUserAdd:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoUserEdit:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                UserObject formUser = new UserObject(formdatapair);
                UserObject updateUser = new UserObject();
                String UserName = "";
                response = GetUserNameBySessionRefCode(formUser.SessionParam, out UserName);

                if (response.result == false)
                {
                    String errCode = response.errcode != null && !response.errcode.Equals("") ? response.errcode : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = response.errmsg != null && !response.errmsg.Equals("") ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), response.errmsg) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String[] opResult = updateUser.UpdateUserInfo(UserName, formUser);

                    if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("true"))
                    {
                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "S", modalBody, modalClose,
                            "default", "window.location.href='/usr/list'",
                            "", "", "");

                        Session["ModalData"] = err;

                        return Json(response);
                    }
                    else if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("false"))
                    {
                        String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                        modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                    else
                    {
                        String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                        modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }
        }

        private GenericJsonError GetUserNameBySessionRefCode(String refcode, out String userName)
        {
            Dictionary<String, String> parameters = new Dictionary<string, string>();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/usr/list'";
            userName = "";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            Boolean hasError = false;
            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!hasError && !isSessionParamsValid)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoGetUserEdit:SP:Error:1");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoGetUserEdit:SP:Error:1");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("UserObject.UserName"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "UserObject.UserName");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetUserEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError)
            {
                userName = parameters["UserObject.UserName"];

                if (userName == null || userName.Equals(""))
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "UserObject.UserName");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetUserEdit:SP:Error:3");
                    hasError = true;
                }
            }

            if (hasError)
            {
                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;

                return response;
            }

            return new GenericJsonError(true, "", "", "");
        }    
    }
}
