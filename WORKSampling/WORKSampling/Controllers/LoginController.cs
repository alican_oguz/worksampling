﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Web.Security;
using _2DMap.Utility;
using _2DMap.Models;

namespace _2DMap.Controllers
{
    public class LoginController : Controller
    {
        public ActionResult LoginPage()
        {
            String lang = CommonConfiguration.SiteLanguage();

            if (Request.Cookies["2DMapLoginCookie"] == null || Request.Cookies["2DMapLoginCookie"].Value == "")
            {
                Session.Abandon();
                Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
                AddRedirCookie();
                Response.Redirect(Request.Path);
            }

            try
            {
                FormsAuthenticationTicket ticket = FormsAuthentication.Decrypt(Request.Cookies["2DMapLoginCookie"].Value);
                if (ticket == null || ticket.Expired == true)
                    throw new Exception();
                RemoveRedirCookie();
            }
            catch
            {
                AddRedirCookie();
                Response.Redirect(Request.Path);
            }


            String rememberUserName = "";
            HttpCookie rememberCookie = new HttpCookie("2DMapUserSettings");
            rememberCookie = Request.Cookies["2DMapUserSettings"];

            if (rememberCookie != null && rememberCookie["UserName"] != null)
            {
                rememberUserName = (String)rememberCookie["UserName"];
            }

            Dictionary<string, string> PageLabels = new Dictionary<string, string>();
            PageLabels["LOGIN_PAGE_BOX_MSG"] = StringResources.getString(StringResources.RES_MSG, "LOGIN_PAGE_BOX_MSG", lang); ;
            PageLabels["LBL_USER_NAME"] = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_NAME", lang); ;
            PageLabels["LBL_PWD"] = StringResources.getString(StringResources.RES_LABEL, "LBL_PWD", lang); ;
            PageLabels["LBL_BTN_LOGIN"] = StringResources.getString(StringResources.RES_LABEL, "LBL_BTN_LOGIN", lang); ;
            PageLabels["LBL_REMEMBER_ME"] = StringResources.getString(StringResources.RES_LABEL, "LBL_REMEMBER_ME", lang); ;
            PageLabels["LBL_FORGOT_PWD"] = StringResources.getString(StringResources.RES_LABEL, "LBL_FORGOT_PWD", lang); ;
            PageLabels["R_USER_NAME"] = rememberUserName;
            PageLabels["R_CHECKED"] = !rememberUserName.Equals("") ? "1" : "0";
            PageLabels["R_USER_NAME"]= rememberUserName;
            PageLabels["R_CHECKED"] = !rememberUserName.Equals("")?"1":"0";

            ViewBag.PageLabels = PageLabels;
            ViewBag.Title = StringResources.getString(StringResources.RES_PAGE_TITLE, "LoginPage", lang);


            return View();
        }

        public JsonResult DoLogin()
        {
            String lang = CommonConfiguration.SiteLanguage();
            String username = (String)Request["2deditor_login_username"];
            String password = (String)Request["2deditor_login_password"];
            String remember = Request["2deditor_rememberlogin"] != null ? (String)Request["2deditor_rememberlogin"] : "off";

            JavaScriptSerializer js = new JavaScriptSerializer();
            GenericJsonError jsonResp = new GenericJsonError(false, "", "ERR:DoLogin:100", "");

            if (username == null || username.Equals("") || password == null || password.Equals(""))
            {
                String errMsg = StringResources.getString(StringResources.RES_ERR, "LOGIN_PAGE_INV_FORM_INPUT", lang);
                jsonResp = UnexpectedErrorModal.GenerateModal(lang, errMsg, "infoModal");
                return Json(jsonResp);
            }

            UserObject userObject = new UserObject();
            userObject.UserName = username;
            userObject.UserPassword = password;

            String[] result = userObject.Login(out userObject);

            if (result != null && result[0].Equals("true") && result[1].Equals("") && userObject != null)
            {

                if (remember.Equals("on"))
                {
                    HttpCookie rememberCookie = new HttpCookie("2DMapUserSettings");
                    rememberCookie["Language"] = lang;
                    rememberCookie["UserName"] = username;
                    rememberCookie.Expires = DateTime.Now.AddMonths(1);
                    rememberCookie.HttpOnly = true;
                    Response.Cookies.Add(rememberCookie);
                }
                else
                {
                    HttpCookie rememberCookie = new HttpCookie("2DMapUserSettings", "");
                    rememberCookie.HttpOnly = true;
                    Response.Cookies.Add(rememberCookie);
                }

                string guid = Guid.NewGuid().ToString();
                
                HttpCookie AuthTokenCookie = new HttpCookie("2DMapAuthToken", guid);
                AuthTokenCookie.Expires = DateTime.Now.AddMinutes(40);
                AuthTokenCookie.HttpOnly = true;

                Response.Cookies.Add(AuthTokenCookie);
                Response.Cookies.Add(new HttpCookie("2DMapLoginCookie", ""));

                UserSessionData userInfoItem = new UserSessionData();
                userInfoItem.Name = (userObject.UserFirstName + " " + userObject.UserMidName).Trim();
                userInfoItem.Surname = userObject.UserLastName;
                userInfoItem.AuthToken = guid;
                userInfoItem.Email = userObject.UserEMail;
                userInfoItem.ProfileImage = CommonConfiguration.UserDefaultProfileImg();
                userInfoItem.Username = userObject.UserName;
                userInfoItem.Status = userObject.UserStatus.ToString();
                Session["UserSessionData"] = userInfoItem;
                Session["MustChangePwd"] = userObject.UserMustChangePwd;
                Session["UserSessionRole"] = userObject.UserRole;

                jsonResp = new GenericJsonError(true, "", "", "");
                return Json(jsonResp);
            }
            else
            {
                HttpCookie rememberCookie = new HttpCookie("2DMapUserSettings");
                rememberCookie = Request.Cookies["2DMapUserSettings"];

                if (rememberCookie != null && rememberCookie["UserName"] != null)
                {
                    String rememberUserName = (String)rememberCookie["UserName"];

                    if (!rememberUserName.Equals(username))
                    {
                        HttpCookie forgetCookie = new HttpCookie("2DMapUserSettings", "");
                        forgetCookie.HttpOnly = true;
                        Response.Cookies.Add(forgetCookie);
                    }
                }

                String ModalTitle = StringResources.getString(StringResources.RES_LABEL, "LBL_LOGIN_MODAL_TITLE", lang);
                String ModalBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                ErrorModalData errData = new ErrorModalData(
                     "infoModal", ModalTitle, "F",
                     result[2],
                     ModalBtn, "default", "dismiss",
                     "", "", ""
                 );

                Session["ModalData"] = errData;

                jsonResp = new GenericJsonError(false, "", "", "");
                return Json(jsonResp);
            }
        }

        public ActionResult RememberPwdPage()
        {
            String lang = CommonConfiguration.SiteLanguage();

            Dictionary<string, string> PageLabels = new Dictionary<string, string>();
            PageLabels["REMEMBER_PWD_PAGE_BOX_MSG"] = StringResources.getString(StringResources.RES_MSG, "REMEMBER_PWD_PAGE_BOX_MSG", lang);
            PageLabels["LBL_USER_EMAIL"] = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_EMAIL", lang);
            PageLabels["LBL_BTN_SEND"] = StringResources.getString(StringResources.RES_LABEL, "LBL_BTN_SEND", lang);
            PageLabels["LBL_REDIRECT_LOGIN"] = StringResources.getString(StringResources.RES_LABEL, "LBL_REDIRECT_LOGIN", lang);

            ViewBag.PageLabels = PageLabels;
            ViewBag.Title = StringResources.getString(StringResources.RES_PAGE_TITLE, "RememberPwdPage", lang);

            return View();
        }

        public ActionResult UserMustChangePwdPage()
        {
            String lang = CommonConfiguration.SiteLanguage();
            UserSessionData dataItem = (UserSessionData)Session["UserSessionData"];

            String MustChangeConfirmMsg = String.Format(StringResources.getString(StringResources.RES_MSG, "MUST_CHANGE_PWD_PAGE_BOX_MSG", lang), dataItem.Username);
            Dictionary<string, string> PageLabels = new Dictionary<string, string>();
            PageLabels["MUST_CHANGE_PWD_PAGE_BOX_MSG"] = MustChangeConfirmMsg;
            PageLabels["LBL_USER_NAME"] = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_NAME", lang);
            PageLabels["LBL_OLDPWD"] = StringResources.getString(StringResources.RES_LABEL, "LBL_OLD_PWD", lang);
            PageLabels["LBL_PWD"] = StringResources.getString(StringResources.RES_LABEL, "LBL_PWD", lang);
            PageLabels["LBL_PWD_RE"] = StringResources.getString(StringResources.RES_LABEL, "LBL_PWD_RE", lang);
            PageLabels["LBL_BTN_SEND"] = StringResources.getString(StringResources.RES_LABEL, "LBL_BTN_SEND", lang);
            PageLabels["LBL_REDIRECT_LOGIN"] = StringResources.getString(StringResources.RES_LABEL, "LBL_REDIRECT_LOGIN", lang);

            ViewBag.PageLabels = PageLabels;
            ViewBag.Title = StringResources.getString(StringResources.RES_PAGE_TITLE, "UserMustChangePwdPage", lang);

            return View();
        }

        [NoCache]
        public JsonResult DoUserMustChangePwdPage()
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();
            String oldpwd = Request["oldpwd"];
            String newpwd = Request["newpwd"];
            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_USER_RENEW_PWD", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);          

            try
            {
                UserObject newUser = new UserObject();
                UserSessionData userSession = (UserSessionData)Session["UserSessionData"];
                newUser.UserName = userSession.Username;
                
                String[] opResult = newUser.ChangePwd(userSession.Username,oldpwd,newpwd);
                if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("true"))
                {
                    Session["MustChangePwd"] = -1;
                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "S", modalBody, modalClose,
                        "default", "window.location.href='/home'",
                        "", "", "");

                    Session["ModalData"] = err;

                    return Json(response);
                }
                else if (opResult != null && opResult.Length == 3 && opResult[0] != null && opResult[0].Equals("false"))
                {
                    String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }
           
        }

        public JsonResult DoResendPwd()
        {
            String lang = CommonConfiguration.SiteLanguage();
            String useremail = (String)Request["2deditor_remember_email"];

            JavaScriptSerializer js = new JavaScriptSerializer();
            GenericJsonError jsonResp = new GenericJsonError(false, "", "ERR:DoResendPwd:100", "");

            if (useremail == null || useremail.Equals(""))
            {
                String errMsg = StringResources.getString(StringResources.RES_ERR, "DEFAULT_PAGE_INV_FORM_INPUT", lang);
                jsonResp = UnexpectedErrorModal.GenerateModal(lang, errMsg, "infoModal");
                return Json(jsonResp);
            }
            UserObject userobj = new UserObject();
            userobj.SendPwd(useremail);
            String ModalTitle = StringResources.getString(StringResources.RES_LABEL, "LBL_REMEMBER_PWD_MODAL_TITLE", lang);
            String ModalBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            ErrorModalData errData = new ErrorModalData(
                 "infoModal", ModalTitle, "S",
                 StringResources.getString(StringResources.RES_MSG, "REMEMBER_PWG_OK_MSG", lang),
                 ModalBtn, "default", "window.location='/login'",
                 "", "", ""
             );

            Session["ModalData"] = errData;

            jsonResp = new GenericJsonError(true, "", "", "");
            return Json(jsonResp);
        }

        public JsonResult ConfirmLogout()
        {
            String lang = CommonConfiguration.SiteLanguage();
            String logoutconfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_LOGOUT", lang);
            String modalTitle = StringResources.getString(StringResources.RES_LABEL, "TOP_MENU_LOGOUT", lang);
            String btnConfirm = StringResources.getString(StringResources.RES_LABEL, "LBL_YES", lang);
            String btnCancel = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);

            ErrorModalData infoModal = new ErrorModalData(
                "infoModal", modalTitle, "I",
                logoutconfirmMsg,
                btnConfirm, "primary", "doLogout()",
                btnCancel, "default", "dismiss"
            );

            infoModal.Btn1Loading = "...";

            Session["ModalData"] = infoModal;

            JavaScriptSerializer js = new JavaScriptSerializer();
            GenericJsonError response = new GenericJsonError(true, "", "0", "");
            return Json(response, JsonRequestBehavior.AllowGet);

        }

        public JsonResult DoLogout()
        {
            JavaScriptSerializer js = new JavaScriptSerializer();
            GenericJsonError response = new GenericJsonError(true, "", "0", "");

            Session.Remove("UserSessionData");
            Session.Remove("MustChangePwd");
            PageDefinitonData.ClearBreadCrumbSessionData(Session);
            Session.Abandon();

            Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));
            Response.Cookies.Add(new HttpCookie("2DMapAuthToken", ""));
            Response.Cookies.Add(new HttpCookie("2DMapLoginCookie", ""));

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [NoCache]
        public JsonResult HasValidSession()
        {
            if (Session["UserSessionData"] == null)
            {
                String lang = CommonConfiguration.SiteLanguage();
                String LblError = StringResources.getString(StringResources.RES_LABEL, "LBL_SESSION_ERROR", lang);
                String LblClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);
                String MsgError = StringResources.getString(StringResources.RES_ERR, "USER_SESSION_IVALID", lang);

                ErrorModalData infoModal = new ErrorModalData(
                    "logoutModal", LblError, "F",
                    MsgError,
                    LblClose, "default", "window.location.href='/login'",
                    "", "", ""
                );

                Session["ModalData"] = infoModal;

                GenericJsonError response = new GenericJsonError(false, "", "", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                GenericJsonError response = new GenericJsonError(true, "", "", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        private void RemoveRedirCookie()
        {
            HttpCookie PalmarisWebLoginCookie = new HttpCookie("2DMapLoginCookie", "");
            PalmarisWebLoginCookie.HttpOnly = true;

            Response.Cookies.Add(PalmarisWebLoginCookie);
        }

        private void AddRedirCookie()
        {
            FormsAuthenticationTicket ticket =
            new FormsAuthenticationTicket(1, "2DMapLoginCookieVal", DateTime.Now, DateTime.Now.AddSeconds(15), false, "");
            string encryptedText = FormsAuthentication.Encrypt(ticket);

            HttpCookie PalmarisWebLoginCookie = new HttpCookie("2DMapLoginCookie", encryptedText);
            PalmarisWebLoginCookie.HttpOnly = true;
            Response.Cookies.Add(PalmarisWebLoginCookie);
        }
    }
}
