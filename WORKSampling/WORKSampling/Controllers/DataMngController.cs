﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using _2DMap.Utility;
using _2DMap.Models;

namespace _2DMap.Controllers
{
    public class DataMngController : Controller
    {
        [CustomAuthorize]
        public ActionResult CompaniesList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("CompaniesList", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.SaveBreadCrumbData(Session);

            CompanyItems coMpanies = new CompanyItems();
            String[] opResult = coMpanies.Load();

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_CATEGORY", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_EMAIL", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_WORKER_NUMBER", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
            ViewBag.TableHeaders = TableHeaders;

            ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_ADD_NEW", lang);
            ViewBag.EditTip = StringResources.getString(StringResources.RES_LABEL, "LBL_EDIT", lang);
            ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);

            ViewBag.DeleteConfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_COMPANY_DELETE", lang);
            ViewBag.HasError = "0";

            if (opResult[0].Equals("true"))
            {
                ViewBag.TableData = coMpanies.CompanyItemsList;
            }
            else
            {
                String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_LIST", lang);
                String modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), opResult[2]);
                String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                ViewBag.HasError = "1";
                ViewBag.TableData = new List<CompanyItem>();
            }

            return View();
        }

        [CustomAuthorize]
        public ActionResult CompaniesAdd()
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("CompaniesAdd", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.AddBreadCrumbItem("CompaniesList", true);
            pData.SaveBreadCrumbData(Session);
            UserObjectSet items = new UserObjectSet();
            items.LoadForManager();
            List<UserObject> item = new List<UserObject>();
            item = items.UserObjectList;
            ViewBag.userList = item;
            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("CompanyName", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_NAME", lang));
            FormLabels.Add("CompanyCategory", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_CATEGORY", lang));
            FormLabels.Add("CompanyEmail", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_EMAIL", lang));
            FormLabels.Add("CompanyWorkerNumber", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_WORKER_NUMBER", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);

            return View();
        }

        [NoCache]
        public JsonResult DoCompaniesAdd(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_ADD_NEW", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoCompaniesAdd:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoCompaniesAdd:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "window.location='/data/comp'",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                CompanyItem newItem = new CompanyItem(formdatapair);
                String[] opResult = newItem.Create();
                if (opResult[0].Equals("true"))
                {
                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "S", modalBody, modalClose,
                        "default", "window.location.href='/data/comp'",
                        "", "", "");

                    Session["ModalData"] = err;

                    return Json(response);
                }
                else if (opResult[0].Equals("false"))
                {
                    String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }

        }

        [CustomAuthorize]
        public ActionResult CompaniesEdit(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("CompaniesEdit", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.AddBreadCrumbItem("CompaniesList", true);
            pData.SaveBreadCrumbData(Session);

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("CompanyId", StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
            FormLabels.Add("CompanyName", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_NAME", lang));
            FormLabels.Add("CompanyCategory", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_CATEGORY", lang));
            FormLabels.Add("CompanyEmail", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_EMAIL", lang));
            FormLabels.Add("CompanyWorkerNumber", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_WORKER_NUMBER", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.CancelBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
            ViewBag.Refcode = refcode;
            return View();
        }
        [NoCache]
        public JsonResult DoGetCompaniesEdit(String refcode)
        {
            CompanyItem editItem = new CompanyItem();
            String CompanyId = "";
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/data/comp'";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            GenericJsonError response = GetCompanyIdBySessionRefCode(refcode, out CompanyId);

            if (response.result == true)
            {
                Int64 iCompanyId = -1;
                Int64.TryParse(CompanyId, out iCompanyId);

                String[] opResult = editItem.LoadByKey(iCompanyId);
                if (opResult[0].Equals("true"))
                {
                    List<EditFormDataItem> formItems = new List<EditFormDataItem>();
                    formItems.Add(new EditFormDataItem("CompanyId", "text", editItem.Id.ToString(), true));
                    formItems.Add(new EditFormDataItem("CompanyName", "text", editItem.Name, false));//name kısmı modelden gelecek
                    formItems.Add(new EditFormDataItem("CompanyCategory", "text", editItem.CompanyCategory, false));//ıntcode kısmı modelden gelecek değerle değiştirilecek
                    formItems.Add(new EditFormDataItem("CompanyEmail", "text", editItem.CompanyEmail, false));//
                    formItems.Add(new EditFormDataItem("CompanyWorkerNumber", "text", editItem.CompanyWorkerNumber.ToString(), false));
                    response.data1 = JsonConvert.SerializeObject(formItems);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                else if (opResult[0].Equals("false"))
                {
                    modalBody = opResult[2];
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetCompanyEdit:OP:Error:" + opResult[1]);

                }
                else
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "CompanyItem");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetCompanyEdit:SP:Error:5");
                }

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;
            }

            return Json(response, JsonRequestBehavior.AllowGet);

        }

        private GenericJsonError GetCompanyIdBySessionRefCode(String refcode, out String coMpanyId)
        {
            Dictionary<String, String> parameters = new Dictionary<string, string>();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/data/comp'";
            coMpanyId = "";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            Boolean hasError = false;
            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!hasError && !isSessionParamsValid)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoGetCompaniesEdit:SP:Error:1");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoGetCompaniesEdit:SP:Error:1");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("CompanyItem.Id"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "CompanyItem.Id");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetCompaniesEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("CompanyItem.Name"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "CompanyItem.Name");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetCompaniesEdit:SP:Error:2");
                hasError = true;
            }
            if (!hasError && !parameters.ContainsKey("CompanyItem.CompanyCategory"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "CompanyItem.CompanyCategory");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetCompaniesEdit:SP:Error:2");
                hasError = true;
            }
            if (!hasError && !parameters.ContainsKey("CompanyItem.CompanyEmail"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "CompanyItem.CompanyEmail");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetCompaniesEdit:SP:Error:2");
                hasError = true;
            }
            if (!hasError && !parameters.ContainsKey("CompanyItem.CompanyWorkerNumber"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "CompanyItem.CompanyWorkerNumber");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetCompaniesEdit:SP:Error:2");
                hasError = true;
            }
            if (!hasError)
            {
                coMpanyId = parameters["CompanyItem.Id"];

                if (coMpanyId == null || coMpanyId.Equals(""))
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "CompanyItem.Id");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetCompaniesEdit:SP:Error:3");
                    hasError = true;
                }
            }

            if (hasError)
            {
                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;

                return response;
            }

            return new GenericJsonError(true, "", "", "");
        }

        [NoCache]
        public JsonResult DoCompaniesEdit(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_EDIT", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoCompaniesEdit:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoCompaniesEdit:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                CompanyItem formItem = new CompanyItem(formdatapair);
                CompanyItem updateItem = new CompanyItem();
                String CompanyId = "";
                response = GetCompanyIdBySessionRefCode(formItem.SessionParam, out CompanyId);

                if (response.result == false)
                {
                    String errCode = response.errcode != null && !response.errcode.Equals("") ? response.errcode : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = response.errmsg != null && !response.errmsg.Equals("") ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), response.errmsg) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String[] opResult = updateItem.Update(CompanyId, formItem);

                    if (opResult[0].Equals("true"))
                    {
                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "S", modalBody, modalClose,
                            "default", "window.location.href='/data/comp'",
                            "", "", "");

                        Session["ModalData"] = err;

                        return Json(response);
                    }
                    else if (opResult[0].Equals("false"))
                    {
                        String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                        modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                    else
                    {
                        String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                        modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }
        }

        [NoCache]
        public JsonResult DoCompaniesDelete(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            Dictionary<String, String> parameters = new Dictionary<string, string>();

            Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
            if (!isUserSessionValid)
            {
                response = new GenericJsonError(false, "", "timeout", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!isSessionParamsValid)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response = new GenericJsonError(true, "", "", "");

            if (!parameters.ContainsKey("CompanyItem.Id"))
            {
                response = new GenericJsonError(false, "", "", "DoCompaniesDelete:UserObj:Err:1");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String CompanyId = parameters["CompanyItem.Id"];
            Int64 iCompanyId = -1;
            Int64.TryParse(CompanyId, out iCompanyId);

            if (CompanyId == null || CompanyId.Equals("") || iCompanyId < 0)
            {
                response = new GenericJsonError(false, "", "", "DoCompaniesDelete:UserObj:Err:2");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

          /*  DepartmentItems lMapItems = new DepartmentItems();
            String[] lResult = lMapItems.LoadBySkiCenterId(iCompanyId);
            if (lResult[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", lResult[1], lResult[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }*/

            CompanyItem deleteItem = new CompanyItem();
            deleteItem.Id = iCompanyId;
            String[] result = deleteItem.Delete();
            if (result[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", result[1], result[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [CustomAuthorize]
        public ActionResult DepartmentsList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("DepartmentsList", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.SaveBreadCrumbData(Session);

            DepartmentItems departmentItems = new DepartmentItems();
            String[] opResult = departmentItems.Load();

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang)); 
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
            ViewBag.TableHeaders = TableHeaders;

            ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENTS_ADD_NEW", lang);
            ViewBag.EditTip = StringResources.getString(StringResources.RES_LABEL, "LBL_EDIT", lang);
            ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);

            ViewBag.DeleteConfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_DEPARTMENT_DELETE", lang);
            ViewBag.HasError = "0";

            if (opResult[0].Equals("true"))
            {
                ViewBag.TableData = departmentItems.DepartmentItemsList;
            }
            else
            {
                String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENTS_LIST", lang);
                String modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), opResult[2]);
                String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                ViewBag.HasError = "1";
                ViewBag.TableData = new List<DepartmentItem>();
            }

            return View();
        }

        [CustomAuthorize]
        public ActionResult DepartmentsAdd()
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("DepartmentsAdd", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.AddBreadCrumbItem("DepartmentsList", true);
            pData.SaveBreadCrumbData(Session);

            Dictionary<String, String> coMpaniesDict = new Dictionary<string, string>();
            CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_COMPANIES, out coMpaniesDict);

            CompanyItems coMpanies = new CompanyItems();
            coMpanies.Load();

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("CompanyId", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY", lang));
            FormLabels.Add("DepartmentName", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
            ViewBag.CompaniesDict = coMpanies.CompanyItemsList;

            return View();
        }

        [NoCache]
        public JsonResult DoDepartmentsAdd(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENTS_ADD_NEW", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoDepartmentsAdd:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoDepartmentsAdd:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "window.location='/data/dept'",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                DepartmentItem newItem = new DepartmentItem(formdatapair);
                String[] opResult = newItem.Create();
                if (opResult[0].Equals("true"))
                {
                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "S", modalBody, modalClose,
                        "default", "window.location.href='/data/dept'",
                        "", "", "");

                    Session["ModalData"] = err;

                    return Json(response);
                }
                else if (opResult[0].Equals("false"))
                {
                    String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }

        }

        [CustomAuthorize]
        public ActionResult DepartmentsEdit(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("DepartmentsEdit", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.AddBreadCrumbItem("DepartmentsList", true);
            pData.SaveBreadCrumbData(Session);

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
            FormLabels.Add("CompanyId", StringResources.getString(StringResources.RES_LABEL, "LBL_COMPANY", lang));
            FormLabels.Add("DepartmentName", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.CancelBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
            ViewBag.Refcode = refcode;
            return View();
        }

        [NoCache]
        public JsonResult DoGetDepartmentsEdit(String refcode)
        {
            DepartmentItem editItem = new DepartmentItem();
            String DepartmentId = "";
            String CompanyId = "";
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/data/dept'";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENTS_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            GenericJsonError response = GetDepartmentIdBySessionRefCode(refcode, out DepartmentId, out CompanyId);

            if (response.result == true)
            {
                Int64 iDepartmentId = -1;
                Int64.TryParse(DepartmentId, out iDepartmentId);

                String[] opResult = editItem.LoadByKey(iDepartmentId);
                if (opResult[0].Equals("true"))
                {
                    Dictionary<String, String> coMpaniesDict = new Dictionary<string, string>();
                    CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_COMPANIES, out coMpaniesDict);


                    List<EditFormSelectItem> companiesSelectItems = EditFormDataItem.GenerateListFromDictionary(coMpaniesDict);
                   

                    List<EditFormDataItem> formItems = new List<EditFormDataItem>();
                    formItems.Add(new EditFormDataItem("DepartmentId", "text", editItem.Id.ToString(), true));
                    formItems.Add(new EditFormDataItem("CompanyId", "select", editItem.CompanyId.ToString(), companiesSelectItems, true));
                    formItems.Add(new EditFormDataItem("DepartmentName", "text", editItem.Name, false));
                 
                    response.data1 = JsonConvert.SerializeObject(formItems);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                else if (opResult[0].Equals("false"))
                {
                    modalBody = opResult[2];
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepartmentsEdit:OP:Error:" + opResult[1]);
                }
                else
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "DepartmentItem");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepartmentsEdit:SP:Error:5");
                }

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;
            }

            return Json(response, JsonRequestBehavior.AllowGet);

        }

        private GenericJsonError GetDepartmentIdBySessionRefCode(String refcode, out String departmentId, out String coMpanyId)
        {
            Dictionary<String, String> parameters = new Dictionary<string, string>();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/data/dept'";
            departmentId = "";
            coMpanyId = "";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENTS_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            Boolean hasError = false;
            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!hasError && !isSessionParamsValid)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoGetDepartmentsEdit:SP:Error:1");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoGetDepartmentsEdit:SP:Error:1");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("DepartmentItem.Id"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepartmentItem.Id");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepartmentsEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("DepartmentItem.Name"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepartmentItem.Name");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepartmentsEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("DepartmentItem.CompanyId"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepartmentItem.SkiCenterId");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepartmentsEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError)
            {
                departmentId = parameters["DepartmentItem.Id"];

                if (departmentId == null || departmentId.Equals(""))
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepartmentItem.Id");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepartmentsEdit:SP:Error:3");
                    hasError = true;
                }

                coMpanyId = parameters["DepartmentItem.CompanyId"];

                if (coMpanyId == null || coMpanyId.Equals(""))
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepartmentItem.coMpanyId");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepartmentsEdit:SP:Error:3");
                    hasError = true;
                }
            }

            if (hasError)
            {
                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;

                return response;
            }

            return new GenericJsonError(true, "", "", "");
        }

        [NoCache]
        public JsonResult DoDepartmentsEdit(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENTS_EDIT", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoDepartmentsEdit:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoDepartmentsEdit:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                DepartmentItem formItem = new DepartmentItem(formdatapair);
                DepartmentItem updateItem = new DepartmentItem();
                String DepartmentId = "";
                String CompanyId = "";
                response = GetDepartmentIdBySessionRefCode(formItem.SessionParam, out DepartmentId, out CompanyId);

                if (response.result == false)
                {
                    String errCode = response.errcode != null && !response.errcode.Equals("") ? response.errcode : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = response.errmsg != null && !response.errmsg.Equals("") ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), response.errmsg) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String[] opResult = updateItem.Update(DepartmentId, CompanyId, formItem);

                    if (opResult[0].Equals("true"))
                    {
                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "S", modalBody, modalClose,
                            "default", "window.location.href='/data/dept'",
                            "", "", "");

                        Session["ModalData"] = err;

                        return Json(response);
                    }
                    else if (opResult[0].Equals("false"))
                    {
                        String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                        modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                    else
                    {
                        String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                        modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }
        }

        [NoCache]
        public JsonResult DoDepartmentsDelete(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            Dictionary<String, String> parameters = new Dictionary<string, string>();

            Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
            if (!isUserSessionValid)
            {
                response = new GenericJsonError(false, "", "timeout", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!isSessionParamsValid)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response = new GenericJsonError(true, "", "", "");

            if (!parameters.ContainsKey("DepartmentItem.Id"))
            {
                response = new GenericJsonError(false, "", "", "DoDepartmentsDelete:UserObj:Err:1");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String DepartmentId = parameters["DepartmentItem.Id"];
            Int64 iDepartmentId = -1;
            Int64.TryParse(DepartmentId, out iDepartmentId);

            if (DepartmentId == null || DepartmentId.Equals("") || iDepartmentId < 0)
            {
                response = new GenericJsonError(false, "", "", "DoDepartmentsDelete:UserObj:Err:2");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            DepartmentItem deleteItem = new DepartmentItem();
            deleteItem.Id = iDepartmentId;
            String[] result = deleteItem.Delete();
            if (result[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", result[1], result[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [CustomAuthorize]
        public ActionResult WorkersList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("WorkersList", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.SaveBreadCrumbData(Session);

            WorkerItems workerItems = new WorkerItems();
            String[] opResult = workerItems.Load();

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_SURNAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_TC", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
            ViewBag.TableHeaders = TableHeaders;

            ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "LBL_WORKERS_ADD_NEW", lang);
            ViewBag.EditTip = StringResources.getString(StringResources.RES_LABEL, "LBL_EDIT", lang);
            ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);

            ViewBag.DeleteConfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_WORKER_DELETE", lang);
            ViewBag.HasError = "0";

            if (opResult[0].Equals("true"))
            {
                ViewBag.TableData = workerItems.WorkerItemsList;
            }
            else
            {
                String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_WORKERS_LIST", lang);
                String modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), opResult[2]);
                String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                ViewBag.HasError = "1";
                ViewBag.TableData = new List<WorkerItem>();
            }

            return View();
        }

        [CustomAuthorize]
        public ActionResult WorkersAdd()
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("WorkersAdd", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.AddBreadCrumbItem("WorkersList", true);
            pData.SaveBreadCrumbData(Session);

            Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
            CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dePartmentsDict);


            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
            FormLabels.Add("WorkerName", StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_NAME", lang));
            FormLabels.Add("WorkerSurname", StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_SURNAME", lang));
            FormLabels.Add("WorkerTc", StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_TC", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
            ViewBag.DepartmentsDict =dePartmentsDict;

            return View();
        }

        [NoCache]
        public JsonResult DoWorkersAdd(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_WORKERS_ADD_NEW", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoWorkersAdd:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoWorkersAdd:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "window.location='/data/worker'",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                WorkerItem newItem = new WorkerItem(formdatapair);
                String[] opResult = newItem.Create();
                if (opResult[0].Equals("true"))
                {
                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "S", modalBody, modalClose,
                        "default", "window.location.href='/data/worker'",
                        "", "", "");

                    Session["ModalData"] = err;

                    return Json(response);
                }
                else if (opResult[0].Equals("false"))
                {
                    String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }

        }

        [CustomAuthorize]
        public ActionResult WorkersEdit(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("WorkersEdit", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.AddBreadCrumbItem("WorkersList", true);
            pData.SaveBreadCrumbData(Session);

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("WorkerId", StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
            FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
            FormLabels.Add("WorkerName", StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_NAME", lang));
            FormLabels.Add("WorkerSurname", StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_SURNAME", lang));
            FormLabels.Add("WorkerTc", StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_TC", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.CancelBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
            ViewBag.Refcode = refcode;
            return View();
        }

        [NoCache]
        public JsonResult DoGetWorkersEdit(String refcode)
        {
            WorkerItem editItem = new WorkerItem();
            String WorkerId = "";
            String DepartmentId = "";
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/data/worker'";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_WORKERS_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            GenericJsonError response = GetWorkerIdBySessionRefCode(refcode, out WorkerId, out DepartmentId);

            if (response.result == true)
            {
                Int64 iWorkerId = -1;
                Int64.TryParse(WorkerId, out iWorkerId);

                String[] opResult = editItem.LoadByKey(iWorkerId);
                if (opResult[0].Equals("true"))
                {
                    Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
                    CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dePartmentsDict);

                  

                    List<EditFormSelectItem> dePartmentsSelectItems = EditFormDataItem.GenerateListFromDictionary(dePartmentsDict);
                    

                    List<EditFormDataItem> formItems = new List<EditFormDataItem>();
                    formItems.Add(new EditFormDataItem("WorkerId", "text", editItem.Id.ToString(), true));
                    formItems.Add(new EditFormDataItem("DepartmentId", "select", editItem.DepartmentId.ToString(), dePartmentsSelectItems, false));
                    formItems.Add(new EditFormDataItem("WorkerName", "text", editItem.Name, false));
                    formItems.Add(new EditFormDataItem("WorkerSurname", "text", editItem.Surname, false));
                    formItems.Add(new EditFormDataItem("WorkerTc", "text", editItem.Tc.ToString(), false));
                 
                    response.data1 = JsonConvert.SerializeObject(formItems);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                else if (opResult[0].Equals("false"))
                {
                    modalBody = opResult[2];
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetWorkersEdit:OP:Error:" + opResult[1]);
                }
                else
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "WorkerItem");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetWorkersEdit:SP:Error:5");
                }

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;
            }

            return Json(response, JsonRequestBehavior.AllowGet);

        }

        private GenericJsonError GetWorkerIdBySessionRefCode(String refcode, out String workerId, out String dePartmentId)
        {
            Dictionary<String, String> parameters = new Dictionary<string, string>();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/data/worker'";
            workerId = "";
            dePartmentId = "";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_WORKERS_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            Boolean hasError = false;
            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!hasError && !isSessionParamsValid)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoGetWorkersEdit:SP:Error:1");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoGetWorkersEdit:SP:Error:1");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("WorkerItem.Id"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "WorkerItem.Id");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetWorkersEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("WorkerItem.Name"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "WorkerItem.Name");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetWorkersEdit:SP:Error:2");
                hasError = true;
            }
            if (!hasError && !parameters.ContainsKey("WorkerItem.Surname"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "WorkerItem.Surname");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetWorkersEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("WorkerItem.Tc"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "WorkerItem.Tc");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetWorkersEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("WorkerItem.DepartmentId"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "WorkerItem.DepartmentId");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetWorkersEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError)
            {
                workerId = parameters["WorkerItem.Id"];

                if (workerId == null || workerId.Equals(""))
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "WorkerItem.Id");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetWorkersEdit:SP:Error:3");
                    hasError = true;
                }

                dePartmentId = parameters["WorkerItem.DepartmentId"];

                if (dePartmentId == null || dePartmentId.Equals(""))
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "WorkerItem.dePartmentId");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetWorkersEdit:SP:Error:3");
                    hasError = true;
                }
            }

            if (hasError)
            {
                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;

                return response;
            }

            return new GenericJsonError(true, "", "", "");
        }

        [NoCache]
        public JsonResult DoWorkersEdit(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_WORKERS_EDIT", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoWorkersEdit:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoWorkersEdit:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                WorkerItem formItem = new WorkerItem(formdatapair);
                WorkerItem updateItem = new WorkerItem();
                String WorkerId = "";
                String DepartmentId = "";
                response = GetWorkerIdBySessionRefCode(formItem.SessionParam, out WorkerId, out DepartmentId);

                if (response.result == false)
                {
                    String errCode = response.errcode != null && !response.errcode.Equals("") ? response.errcode : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = response.errmsg != null && !response.errmsg.Equals("") ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), response.errmsg) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String[] opResult = updateItem.Update(WorkerId, DepartmentId, formItem);

                    if (opResult[0].Equals("true"))
                    {
                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "S", modalBody, modalClose,
                            "default", "window.location.href='/data/worker'",
                            "", "", "");

                        Session["ModalData"] = err;

                        return Json(response);
                    }
                    else if (opResult[0].Equals("false"))
                    {
                        String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                        modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                    else
                    {
                        String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                        modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }
        }

        [NoCache]
        public JsonResult DoWorkersDelete(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            Dictionary<String, String> parameters = new Dictionary<string, string>();

            Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
            if (!isUserSessionValid)
            {
                response = new GenericJsonError(false, "", "timeout", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!isSessionParamsValid)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response = new GenericJsonError(true, "", "", "");

            if (!parameters.ContainsKey("WorkerItem.Id"))
            {
                response = new GenericJsonError(false, "", "", "DoWorkersDelete:UserObj:Err:1");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String WorkerId = parameters["WorkerItem.Id"];
            Int64 iWorkerId = -1;
            Int64.TryParse(WorkerId, out iWorkerId);

            if (WorkerId == null || WorkerId.Equals("") || iWorkerId < 0)
            {
                response = new GenericJsonError(false, "", "", "DoWorkersDelete:UserObj:Err:2");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            WorkerItem deleteItem = new WorkerItem();
            deleteItem.Id = iWorkerId;
            String[] result = deleteItem.Delete();
            if (result[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", result[1], result[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }

        [CustomAuthorize]
        public ActionResult OperatorsList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("OperatorsList", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.SaveBreadCrumbData(Session);

            OperatorItems oPerators = new OperatorItems();
            String[] opResult = oPerators.Load();

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_PWD", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_EMAIL", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_USERNAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
            ViewBag.TableHeaders = TableHeaders;

            ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATORS_ADD_NEW", lang);
            ViewBag.EditTip = StringResources.getString(StringResources.RES_LABEL, "LBL_EDIT", lang);
            ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);

            ViewBag.DeleteConfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_OPERATORS_DELETE", lang);
            ViewBag.HasError = "0";

            if (opResult[0].Equals("true"))
            {
                ViewBag.TableData = oPerators.OperatorItemsList;
            }
            else
            {
                String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATORS_LIST", lang);
                String modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), opResult[2]);
                String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                ViewBag.HasError = "1";
                ViewBag.TableData = new List<OperatorItem>();
            }

            return View();
        }

        [CustomAuthorize]
        public ActionResult OperatorsAdd()
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("OperatorsAdd", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.AddBreadCrumbItem("OperatorsList", true);
            pData.SaveBreadCrumbData(Session);

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("OperatorName", StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_NAME", lang));
            FormLabels.Add("OperatorPwd", StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_PWD", lang));
            FormLabels.Add("OperatorEmail", StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_EMAIL", lang));
            FormLabels.Add("OperatorUsername", StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_USERNAME", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);

            return View();
        }

        [NoCache]
        public JsonResult DoOperatorsAdd(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATORS_ADD_NEW", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoOperatorsAdd:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoOperatorsAdd:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "window.location='/data/ope'",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                OperatorItem newItem = new OperatorItem(formdatapair);
                String[] opResult = newItem.Create();
                if (opResult[0].Equals("true"))
                {
                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "S", modalBody, modalClose,
                        "default", "window.location.href='/data/ope'",
                        "", "", "");

                    Session["ModalData"] = err;

                    return Json(response);
                }
                else if (opResult[0].Equals("false"))
                {
                    String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }

        }

        [CustomAuthorize]
        public ActionResult OperatorsEdit(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();

            PageDefinitonData pData = new PageDefinitonData("OperatorsEdit", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.AddBreadCrumbItem("OperatorsList", true);
            pData.SaveBreadCrumbData(Session);

            Dictionary<String, String> FormLabels = new Dictionary<String, String>();
            FormLabels.Add("OperatorId", StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
            FormLabels.Add("OperatorName", StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_NAME", lang));
            FormLabels.Add("OperatorPwd", StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_PWD", lang));
            FormLabels.Add("OperatorEmail", StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_EMAIL", lang));
            FormLabels.Add("OperatorUsername", StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATOR_USERNAME", lang));
            ViewBag.FormLabels = FormLabels;

            ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
            ViewBag.CancelBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);
            ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
            ViewBag.Refcode = refcode;
            return View();
        }

        [NoCache]
        public JsonResult DoGetOperatorsEdit(String refcode)
        {
            OperatorItem editItem = new OperatorItem();
            String OperatorId = "";
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/data/ope'";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATORS_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            GenericJsonError response = GetOperatorIdBySessionRefCode(refcode, out OperatorId);

            if (response.result == true)
            {
                Int64 iOperatorId = -1;
                Int64.TryParse(OperatorId, out iOperatorId);

                String[] opResult = editItem.LoadByKey(iOperatorId);
                if (opResult[0].Equals("true"))
                {
                    List<EditFormDataItem> formItems = new List<EditFormDataItem>();
                    formItems.Add(new EditFormDataItem("OperatorId", "text", editItem.Id.ToString(), true));
                    formItems.Add(new EditFormDataItem("OperatorName", "text", editItem.Name, false));
                    formItems.Add(new EditFormDataItem("OperatorPwd", "text", editItem.Pwd, false));
                    formItems.Add(new EditFormDataItem("OperatorEmail", "text", editItem.Email, false));
                    formItems.Add(new EditFormDataItem("OperatorUsername", "text", editItem.Username, false));
                    response.data1 = JsonConvert.SerializeObject(formItems);
                    return Json(response, JsonRequestBehavior.AllowGet);
                }
                else if (opResult[0].Equals("false"))
                {
                    modalBody = opResult[2];
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetOperatorEdit:OP:Error:" + opResult[1]);

                }
                else
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "OperatorItem");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetOperatorEdit:SP:Error:5");
                }

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;
            }

            return Json(response, JsonRequestBehavior.AllowGet);

        }

        private GenericJsonError GetOperatorIdBySessionRefCode(String refcode, out String oPeratorId)
        {
            Dictionary<String, String> parameters = new Dictionary<string, string>();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();
            String backLink = "window.location='/data/ope'";
            oPeratorId = "";

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATORS_EDIT", lang);
            String modalBody = "";
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            Boolean hasError = false;
            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!hasError && !isSessionParamsValid)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoGetOperatorsEdit:SP:Error:1");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoGetOperatorsEdit:SP:Error:1");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("OperatorItem.Id"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "OperatorItem.Id");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetOperatorsEdit:SP:Error:2");
                hasError = true;
            }

            if (!hasError && !parameters.ContainsKey("OperatorItem.Name"))
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "OperatorItem.Name");
                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetOperatorsEdit:SP:Error:2");
                hasError = true;
            }


            if (!hasError)
            {
                oPeratorId = parameters["OperatorItem.Id"];

                if (oPeratorId == null || oPeratorId.Equals(""))
                {
                    modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "OperatorItem.Id");
                    response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetOperatorsEdit:SP:Error:3");
                    hasError = true;
                }
            }

            if (hasError)
            {
                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", backLink,
                    "", "", "");

                Session["ModalData"] = err;

                return response;
            }

            return new GenericJsonError(true, "", "", "");
        }

        [NoCache]
        public JsonResult DoOperatorsEdit(List<FormDataNameValue> formdatapair)
        {
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            String lang = CommonConfiguration.SiteLanguage();

            String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OPERATORS_EDIT", lang);
            String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
            String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

            if (formdatapair == null)
            {
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoOperatorsEdit:SP:Error:1");
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoOperatorsEdit:SP:Error:1");

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                return Json(response);
            }

            try
            {
                OperatorItem formItem = new OperatorItem(formdatapair);
                OperatorItem updateItem = new OperatorItem();
                String OperatorId = "";
                response = GetOperatorIdBySessionRefCode(formItem.SessionParam, out OperatorId);

                if (response.result == false)
                {
                    String errCode = response.errcode != null && !response.errcode.Equals("") ? response.errcode : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                    modalBody = response.errmsg != null && !response.errmsg.Equals("") ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), response.errmsg) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                    modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                    ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                        "F", modalBody, modalClose,
                        "default", "dismiss",
                        "", "", "");

                    Session["ModalData"] = err;

                    response = new GenericJsonError(false, "", errCode, "");
                    return Json(response);
                }
                else
                {
                    String[] opResult = updateItem.Update(OperatorId, formItem);

                    if (opResult[0].Equals("true"))
                    {
                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "S", modalBody, modalClose,
                            "default", "window.location.href='/data/ope'",
                            "", "", "");

                        Session["ModalData"] = err;

                        return Json(response);
                    }
                    else if (opResult[0].Equals("false"))
                    {
                        String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                        modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                    else
                    {
                        String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                        modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                        modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                        ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                            "F", modalBody, modalClose,
                            "default", "dismiss",
                            "", "", "");

                        Session["ModalData"] = err;

                        response = new GenericJsonError(false, "", errCode, "");
                        return Json(response);
                    }
                }
            }
            catch (Exception ex)
            {
                String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                response = new GenericJsonError(false, "", errCode, ex.Message);
                return Json(response);
            }
        }

        [NoCache]
        public JsonResult DoOperatorsDelete(String refcode)
        {
            String lang = CommonConfiguration.SiteLanguage();
            GenericJsonError response = new GenericJsonError(true, "", "", "");
            Dictionary<String, String> parameters = new Dictionary<string, string>();

            Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
            if (!isUserSessionValid)
            {
                response = new GenericJsonError(false, "", "timeout", "");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

            if (!isSessionParamsValid)
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            response = new GenericJsonError(true, "", "", "");

            if (!parameters.ContainsKey("OperatorItem.Id"))
            {
                response = new GenericJsonError(false, "", "", "DoOperatorsDelete:UserObj:Err:1");
                return Json(response, JsonRequestBehavior.AllowGet);
            }

            String OperatorId = parameters["OperatorItem.Id"];
            Int64 iOperatorId = -1;
            Int64.TryParse(OperatorId, out iOperatorId);

            if (OperatorId == null || OperatorId.Equals("") || iOperatorId < 0)
            {
                response = new GenericJsonError(false, "", "", "DoOperatorsDelete:UserObj:Err:2");
                return Json(response, JsonRequestBehavior.AllowGet);
            }



            OperatorItem deleteItem = new OperatorItem();
            deleteItem.Id = iOperatorId;
            String[] result = deleteItem.Delete();
            if (result[0].Equals("false"))
            {
                response = new GenericJsonError(false, "", result[1], result[2]);
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(response, JsonRequestBehavior.AllowGet);
            }
        }


         [CustomAuthorize]
        public ActionResult ObclassList()
        {
            String lang = CommonConfiguration.SiteLanguage();
            SessionParamsUtil.ClearParameterTable();

            PageDefinitonData pData = new PageDefinitonData("ObclassList", false);
            pData.AddBreadCrumbItem("DataMng", false);
            pData.SaveBreadCrumbData(Session);

            ObclassItems obclassItems = new ObclassItems();
            String[] opResult = obclassItems.Load();

            List<String> TableHeaders = new List<String>();
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
            TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
            ViewBag.TableHeaders = TableHeaders;

            ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_ADD_NEW", lang);
            ViewBag.EditTip = StringResources.getString(StringResources.RES_LABEL, "LBL_EDIT", lang);
            ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);

            ViewBag.DeleteConfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_OBCLASS_DELETE", lang);
            ViewBag.HasError = "0";

            if (opResult[0].Equals("true"))
            {
                ViewBag.TableData = obclassItems.ObclassItemsList;
            }
            else
            {
                String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_LIST", lang);
                String modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), opResult[2]);
                String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                    "F", modalBody, modalClose,
                    "default", "dismiss",
                    "", "", "");

                Session["ModalData"] = err;

                ViewBag.HasError = "1";
                ViewBag.TableData = new List<WorkerItem>();
            }

            return View();
        }


         [CustomAuthorize]
         public ActionResult ObclassAdd()
         {
             String lang = CommonConfiguration.SiteLanguage();

             PageDefinitonData pData = new PageDefinitonData("ObclassAdd", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.AddBreadCrumbItem("ObclassList", true);
             pData.SaveBreadCrumbData(Session);

             Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
             DepartmentItems items = new DepartmentItems();
             items.Load();
             if (items.DepartmentItemsList != null)
             {
                 foreach (DepartmentItem item in items.DepartmentItemsList)
                 {
                     if (!dePartmentsDict.ContainsKey(item.Id.ToString()))
                         dePartmentsDict.Add(item.Id.ToString(), item.Name);
                 }
             }

             Dictionary<String, String> FormLabels = new Dictionary<String, String>();
             FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             FormLabels.Add("ObclassName", StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_NAME", lang));
             ViewBag.FormLabels = FormLabels;

             ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
             ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
             ViewBag.DepartmentsDict = dePartmentsDict;

             return View();
         }


         [NoCache]
         public JsonResult DoObclassAdd(List<FormDataNameValue> formdatapair)
         {
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_ADD_NEW", lang);
             String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             if (formdatapair == null)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoObclassAdd:SP:Error:1");
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoObclassAdd:SP:Error:1");

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "window.location='/data/obclass'",
                     "", "", "");

                 Session["ModalData"] = err;

                 return Json(response);
             }

             try
             {
                 ObclassItem newItem = new ObclassItem(formdatapair);
                 String[] opResult = newItem.Create();
                 if (opResult[0].Equals("true"))
                 {
                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "S", modalBody, modalClose,
                         "default", "window.location.href='/data/obclass'",
                         "", "", "");

                     Session["ModalData"] = err;

                     return Json(response);
                 }
                 else if (opResult[0].Equals("false"))
                 {
                     String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                     modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
                 else
                 {
                     String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
             }
             catch (Exception ex)
             {
                 String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", errCode, ex.Message);
                 return Json(response);
             }

         }

         [CustomAuthorize]
         public ActionResult ObclassEdit(String refcode)
         {
             String lang = CommonConfiguration.SiteLanguage();

             PageDefinitonData pData = new PageDefinitonData("ObclassEdit", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.AddBreadCrumbItem("ObclassList", true);
             pData.SaveBreadCrumbData(Session);

             Dictionary<String, String> FormLabels = new Dictionary<String, String>();
             FormLabels.Add("ObclassId", StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
             FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             FormLabels.Add("ObclassName", StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_NAME", lang));
             ViewBag.FormLabels = FormLabels;

             ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
             ViewBag.CancelBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);
             ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
             ViewBag.Refcode = refcode;
             return View();
         }

         [NoCache]
         public JsonResult DoGetObclassEdit(String refcode)
         {
             ObclassItem editItem = new ObclassItem();
             String ObclassId = "";
             String DepartmentId = "";
             String lang = CommonConfiguration.SiteLanguage();
             String backLink = "window.location='/data/obclass'";

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_EDIT", lang);
             String modalBody = "";
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             GenericJsonError response = GetObclassIdBySessionRefCode(refcode, out ObclassId, out DepartmentId);

             if (response.result == true)
             {
                 Int64 iObclassId = -1;
                 Int64.TryParse(ObclassId, out iObclassId);

                 String[] opResult = editItem.LoadByKey(iObclassId);
                 if (opResult[0].Equals("true"))
                 {
                     Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
                     CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dePartmentsDict);



                     List<EditFormSelectItem> dePartmentsSelectItems = EditFormDataItem.GenerateListFromDictionary(dePartmentsDict);


                     List<EditFormDataItem> formItems = new List<EditFormDataItem>();
                     formItems.Add(new EditFormDataItem("ObclassId", "text", editItem.Id.ToString(), true));
                     formItems.Add(new EditFormDataItem("DepartmentId", "select", editItem.DepartmentId.ToString(), dePartmentsSelectItems, false));
                     formItems.Add(new EditFormDataItem("ObclassName", "text", editItem.Name, false));

                     response.data1 = JsonConvert.SerializeObject(formItems);
                     return Json(response, JsonRequestBehavior.AllowGet);
                 }
                 else if (opResult[0].Equals("false"))
                 {
                     modalBody = opResult[2];
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObclassEdit:OP:Error:" + opResult[1]);
                 }
                 else
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "ObclassItem");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObclassEdit:SP:Error:5");
                 }

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", backLink,
                     "", "", "");

                 Session["ModalData"] = err;
             }

             return Json(response, JsonRequestBehavior.AllowGet);

         }


         private GenericJsonError GetObclassIdBySessionRefCode(String refcode, out String obclassId, out String dePartmentId)
         {
             Dictionary<String, String> parameters = new Dictionary<string, string>();
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();
             String backLink = "window.location='/data/obclass'";
             obclassId = "";
             dePartmentId = "";

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_EDIT", lang);
             String modalBody = "";
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             Boolean hasError = false;
             Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

             if (!hasError && !isSessionParamsValid)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoGetObclassEdit:SP:Error:1");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoGetObclassEdit:SP:Error:1");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("ObclassItem.Id"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObclassItem.Id");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObclassEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("ObclassItem.Name"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObclassItem.Name");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObclassEdit:SP:Error:2");
                 hasError = true;
             }


             if (!hasError && !parameters.ContainsKey("ObclassItem.DepartmentId"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObclassItem.DepartmentId");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObclassEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError)
             {
                 obclassId = parameters["ObclassItem.Id"];

                 if (obclassId == null || obclassId.Equals(""))
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObclassItem.Id");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObclassEdit:SP:Error:3");
                     hasError = true;
                 }

                 dePartmentId = parameters["ObclassItem.DepartmentId"];

                 if (dePartmentId == null || dePartmentId.Equals(""))
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObclassItem.dePartmentId");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObclassEdit:SP:Error:3");
                     hasError = true;
                 }
             }

             if (hasError)
             {
                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", backLink,
                     "", "", "");

                 Session["ModalData"] = err;

                 return response;
             }

             return new GenericJsonError(true, "", "", "");
         }


         [NoCache]
         public JsonResult DoObclassEdit(List<FormDataNameValue> formdatapair)
         {
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_EDIT", lang);
             String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             if (formdatapair == null)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoObclassEdit:SP:Error:1");
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoObclassEdit:SP:Error:1");

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "dismiss",
                     "", "", "");

                 Session["ModalData"] = err;

                 return Json(response);
             }

             try
             {
                 ObclassItem formItem = new ObclassItem(formdatapair);
                 ObclassItem updateItem = new ObclassItem();
                 String ObclassId = "";
                 String DepartmentId = "";
                 response = GetObclassIdBySessionRefCode(formItem.SessionParam, out ObclassId, out DepartmentId);

                 if (response.result == false)
                 {
                     String errCode = response.errcode != null && !response.errcode.Equals("") ? response.errcode : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                     modalBody = response.errmsg != null && !response.errmsg.Equals("") ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), response.errmsg) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
                 else
                 {
                     String[] opResult = updateItem.Update(ObclassId, DepartmentId, formItem);

                     if (opResult[0].Equals("true"))
                     {
                         ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                             "S", modalBody, modalClose,
                             "default", "window.location.href='/data/obclass'",
                             "", "", "");

                         Session["ModalData"] = err;

                         return Json(response);
                     }
                     else if (opResult[0].Equals("false"))
                     {
                         String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                         modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                         modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                         ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                             "F", modalBody, modalClose,
                             "default", "dismiss",
                             "", "", "");

                         Session["ModalData"] = err;

                         response = new GenericJsonError(false, "", errCode, "");
                         return Json(response);
                     }
                     else
                     {
                         String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                         modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                         modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                         ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                             "F", modalBody, modalClose,
                             "default", "dismiss",
                             "", "", "");

                         Session["ModalData"] = err;

                         response = new GenericJsonError(false, "", errCode, "");
                         return Json(response);
                     }
                 }
             }
             catch (Exception ex)
             {
                 String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", errCode, ex.Message);
                 return Json(response);
             }
         }


         [NoCache]
         public JsonResult DoObclassDelete(String refcode)
         {
             String lang = CommonConfiguration.SiteLanguage();
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             Dictionary<String, String> parameters = new Dictionary<string, string>();

             Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
             if (!isUserSessionValid)
             {
                 response = new GenericJsonError(false, "", "timeout", "");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

             if (!isSessionParamsValid)
             {
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             response = new GenericJsonError(true, "", "", "");

             if (!parameters.ContainsKey("ObclassItem.Id"))
             {
                 response = new GenericJsonError(false, "", "", "DoObclassDelete:UserObj:Err:1");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             String ObclassId = parameters["ObclassItem.Id"];
             Int64 iObclassId = -1;
             Int64.TryParse(ObclassId, out iObclassId);

             if (ObclassId == null || ObclassId.Equals("") || iObclassId < 0)
             {
                 response = new GenericJsonError(false, "", "", "DoObclassDelete:UserObj:Err:2");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             ObclassItem deleteItem = new ObclassItem();
             deleteItem.Id = iObclassId;
             String[] result = deleteItem.Delete();
             if (result[0].Equals("false"))
             {
                 response = new GenericJsonError(false, "", result[1], result[2]);
                 return Json(response, JsonRequestBehavior.AllowGet);
             }
             else
             {
                 return Json(response, JsonRequestBehavior.AllowGet);
             }
         }

         [CustomAuthorize]
         public ActionResult ObservationList()
         {
             String lang = CommonConfiguration.SiteLanguage();
             SessionParamsUtil.ClearParameterTable();

             PageDefinitonData pData = new PageDefinitonData("ObservationList", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.SaveBreadCrumbData(Session);

             ObservationItems observationItems = new ObservationItems();
             String[] opResult = observationItems.Load();

             List<String> TableHeaders = new List<String>();
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_NAME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_NAME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_NAME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
             ViewBag.TableHeaders = TableHeaders;

             ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_ADD_NEW", lang);
             ViewBag.EditTip = StringResources.getString(StringResources.RES_LABEL, "LBL_EDIT", lang);
             ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);

             ViewBag.DeleteConfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_OBSERVATION_DELETE", lang);
             ViewBag.HasError = "0";

             if (opResult[0].Equals("true"))
             {
                 ViewBag.TableData = observationItems.ObservationItemsList;
             }
             else
             {
                 String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_LIST", lang);
                 String modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), opResult[2]);
                 String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "dismiss",
                     "", "", "");

                 Session["ModalData"] = err;

                 ViewBag.HasError = "1";
                 ViewBag.TableData = new List<ObservationItem>();
             }

             return View();
         }

         [CustomAuthorize]
         public ActionResult ObservationAdd()
         {
             String lang = CommonConfiguration.SiteLanguage();

             PageDefinitonData pData = new PageDefinitonData("ObservationAdd", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.AddBreadCrumbItem("ObservationList", true);
             pData.SaveBreadCrumbData(Session);


             Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
             DepartmentItems items = new DepartmentItems();
             items.Load();
             if (items.DepartmentItemsList != null)
             {
                 foreach (DepartmentItem item in items.DepartmentItemsList)
                 {
                     if (!dePartmentsDict.ContainsKey(item.Id.ToString()))
                         dePartmentsDict.Add(item.Id.ToString(), item.Name);
                 }
             }

             Dictionary<String, String> obClassesDict = new Dictionary<string, string>();
             ObclassItems obitems = new ObclassItems();
             obitems.Load();
             if (obitems.ObclassItemsList != null)
             {
                 foreach (ObclassItem obitem in obitems.ObclassItemsList)
                 {
                     if (!obClassesDict.ContainsKey(obitem.Id.ToString()))
                         obClassesDict.Add(obitem.Id.ToString(), obitem.Name);
                 }
             }

             Dictionary<String, String> worKersDict = new Dictionary<string, string>();
             WorkerItems workeritems = new WorkerItems();
             workeritems.Load();
             if (workeritems.WorkerItemsList != null)
             {
                 foreach (WorkerItem workeritem in workeritems.WorkerItemsList)
                 {
                     if (!worKersDict.ContainsKey(workeritem.Id.ToString()))
                         worKersDict.Add(workeritem.Id.ToString(), workeritem.Name);
                 }
             }
             



             Dictionary<String, String> FormLabels = new Dictionary<String, String>();
             FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             FormLabels.Add("ObclassId", StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_NAME", lang));
             FormLabels.Add("WorkerId", StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_NAME", lang));
             FormLabels.Add("ObservationName", StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_NAME", lang));
             ViewBag.FormLabels = FormLabels;

             ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
             ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
             ViewBag.DepartmentsDict = dePartmentsDict;
             ViewBag.ObclassesDict = obClassesDict;
             ViewBag.WorkersDict = worKersDict;


             return View();
         }


         [NoCache]
         public JsonResult DoObservationAdd(List<FormDataNameValue> formdatapair)
         {
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_ADD_NEW", lang);
             String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             if (formdatapair == null)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoObservationAdd:SP:Error:1");
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoObservationAdd:SP:Error:1");

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "window.location='/data/observation'",
                     "", "", "");

                 Session["ModalData"] = err;

                 return Json(response);
             }

             try
             {
                 ObservationItem newItem = new ObservationItem(formdatapair);
                 String[] opResult = newItem.Create();
                 if (opResult[0].Equals("true"))
                 {
                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "S", modalBody, modalClose,
                         "default", "window.location.href='/data/observation'",
                         "", "", "");

                     Session["ModalData"] = err;

                     return Json(response);
                 }
                 else if (opResult[0].Equals("false"))
                 {
                     String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                     modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
                 else
                 {
                     String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
             }
             catch (Exception ex)
             {
                 String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", errCode, ex.Message);
                 return Json(response);
             }

         }

         [CustomAuthorize]
         public ActionResult ObservationEdit(String refcode)
         {
             String lang = CommonConfiguration.SiteLanguage();

             PageDefinitonData pData = new PageDefinitonData("ObservationEdit", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.AddBreadCrumbItem("ObservationList", true);
             pData.SaveBreadCrumbData(Session);

             Dictionary<String, String> FormLabels = new Dictionary<String, String>();
             FormLabels.Add("ObservationId", StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
             FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             FormLabels.Add("ObclassId", StringResources.getString(StringResources.RES_LABEL, "LBL_OBCLASS_NAME", lang));
             FormLabels.Add("WorkerId", StringResources.getString(StringResources.RES_LABEL, "LBL_WORKER_NAME", lang));
             FormLabels.Add("ObservationName", StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_NAME", lang));

             ViewBag.FormLabels = FormLabels;


             ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
             ViewBag.CancelBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);
             ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
             ViewBag.Refcode = refcode;
             return View();
         }

         [NoCache]
         public JsonResult DoGetObservationEdit(String refcode)
         {
             ObservationItem editItem = new ObservationItem();
             String ObclassId = "";
             String DepartmentId = "";
             String ObservationId = "";
             String WorkerId = "";

             String lang = CommonConfiguration.SiteLanguage();
             String backLink = "window.location='/data/observation'";

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_EDIT", lang);
             String modalBody = "";
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             GenericJsonError response = GetObservationIdBySessionRefCode(refcode, out ObclassId, out DepartmentId, out ObservationId,out WorkerId);

             if (response.result == true)
             {
                 Int64 iObclassId = -1;
                 Int64.TryParse(ObclassId, out iObclassId);

                 String[] opResult = editItem.LoadByKey(iObclassId);
                 if (opResult[0].Equals("true"))
                 {
                     Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
                     CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dePartmentsDict);

                     Dictionary<String, String> obClassesDict = new Dictionary<string, string>();
                     CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_OBCLASS, out obClassesDict);

                     Dictionary<String, String> worKersDict = new Dictionary<string, string>();
                     CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_WORKERS, out worKersDict);


                     List<EditFormSelectItem> dePartmentsSelectItems = EditFormDataItem.GenerateListFromDictionary(dePartmentsDict);
                     List<EditFormSelectItem> obClassesSelectItems = EditFormDataItem.GenerateListFromDictionary(obClassesDict);
                     List<EditFormSelectItem> worKersSelectItems = EditFormDataItem.GenerateListFromDictionary(worKersDict);


                     List<EditFormDataItem> formItems = new List<EditFormDataItem>();
                     formItems.Add(new EditFormDataItem("ObservationId", "text", editItem.Id.ToString(), true));
                     formItems.Add(new EditFormDataItem("DepartmentId", "select", editItem.DepartmentId.ToString(), dePartmentsSelectItems, false));
                     formItems.Add(new EditFormDataItem("ObclassId", "select", editItem.ObclassId.ToString(), obClassesSelectItems, false));
                     formItems.Add(new EditFormDataItem("WorkerId", "select", editItem.WorkerId.ToString(), worKersSelectItems, false));
                     formItems.Add(new EditFormDataItem("ObservationName", "text", editItem.Name, false));

                     response.data1 = JsonConvert.SerializeObject(formItems);
                     return Json(response, JsonRequestBehavior.AllowGet);
                 }
                 else if (opResult[0].Equals("false"))
                 {
                     modalBody = opResult[2];
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:OP:Error:" + opResult[1]);
                 }
                 else
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "ObservationItem");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:5");
                 }

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", backLink,
                     "", "", "");

                 Session["ModalData"] = err;
             }

             return Json(response, JsonRequestBehavior.AllowGet);

         }

         private GenericJsonError GetObservationIdBySessionRefCode(String refcode, out String observationId, out String dePartmentId, out String obClassId, out String worKerId)
         {
             Dictionary<String, String> parameters = new Dictionary<string, string>();
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();
             String backLink = "window.location='/data/observation'";
             observationId = "";
             dePartmentId = "";
             obClassId = "";
             worKerId = "";

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_EDIT", lang);
             String modalBody = "";
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             Boolean hasError = false;
             Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

             if (!hasError && !isSessionParamsValid)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoGetObservationEdit:SP:Error:1");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoGetObservationEdit:SP:Error:1");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("ObservationItem.Id"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.Id");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("ObservationItem.Name"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.Name");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:2");
                 hasError = true;
             }


             if (!hasError && !parameters.ContainsKey("ObservationItem.DepartmentId"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.DepartmentId");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:2");
                 hasError = true;
             }
             if (!hasError && !parameters.ContainsKey("ObservationItem.ObclassId"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.ObclassId");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:2");
                 hasError = true;
             }
             if (!hasError && !parameters.ContainsKey("ObservationItem.WorkerId"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.WorkerId");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError)
             {
                 observationId = parameters["ObservationItem.Id"];

                 if (observationId == null || observationId.Equals(""))
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.Id");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:3");
                     hasError = true;
                 }

                 dePartmentId = parameters["ObservationItem.DepartmentId"];

                 if (dePartmentId == null || dePartmentId.Equals(""))
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.dePartmentId");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:3");
                     hasError = true;
                 }

                 obClassId = parameters["ObservationItem.ObclassId"];

                 if (obClassId == null || obClassId.Equals(""))
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.obClassId");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:3");
                     hasError = true;
                 }

                 worKerId = parameters["ObservationItem.WorkerId"];

                 if (worKerId == null || worKerId.Equals(""))
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "ObservationItem.worKerId");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetObservationEdit:SP:Error:3");
                     hasError = true;
                 }
             }

             if (hasError)
             {
                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", backLink,
                     "", "", "");

                 Session["ModalData"] = err;

                 return response;
             }

             return new GenericJsonError(true, "", "", "");
         }

         [NoCache]
         public JsonResult DoObservationEdit(List<FormDataNameValue> formdatapair)
         {
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_OBSERVATION_EDIT", lang);
             String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             if (formdatapair == null)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoObservationEdit:SP:Error:1");
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoObservationEdit:SP:Error:1");

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "dismiss",
                     "", "", "");

                 Session["ModalData"] = err;

                 return Json(response);
             }

             try
             {
                 ObservationItem formItem = new ObservationItem(formdatapair);
                 ObservationItem updateItem = new ObservationItem();
                 String ObservationId = "";
                 String DepartmentId = "";
                 String ObclassId = "";
                 String WorkerId = "";
                 response = GetObservationIdBySessionRefCode(formItem.SessionParam, out ObservationId, out DepartmentId, out ObclassId, out WorkerId );

                 if (response.result == false)
                 {
                     String errCode = response.errcode != null && !response.errcode.Equals("") ? response.errcode : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                     modalBody = response.errmsg != null && !response.errmsg.Equals("") ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), response.errmsg) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
                 else
                 {
                     String[] opResult = updateItem.Update(ObservationId, DepartmentId, ObclassId, WorkerId, formItem);

                     if (opResult[0].Equals("true"))
                     {
                         ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                             "S", modalBody, modalClose,
                             "default", "window.location.href='/data/observation'",
                             "", "", "");

                         Session["ModalData"] = err;

                         return Json(response);
                     }
                     else if (opResult[0].Equals("false"))
                     {
                         String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                         modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                         modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                         ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                             "F", modalBody, modalClose,
                             "default", "dismiss",
                             "", "", "");

                         Session["ModalData"] = err;

                         response = new GenericJsonError(false, "", errCode, "");
                         return Json(response);
                     }
                     else
                     {
                         String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                         modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                         modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                         ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                             "F", modalBody, modalClose,
                             "default", "dismiss",
                             "", "", "");

                         Session["ModalData"] = err;

                         response = new GenericJsonError(false, "", errCode, "");
                         return Json(response);
                     }
                 }
             }
             catch (Exception ex)
             {
                 String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", errCode, ex.Message);
                 return Json(response);
             }
         }


         [NoCache]
         public JsonResult DoObservationDelete(String refcode)
         {
             String lang = CommonConfiguration.SiteLanguage();
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             Dictionary<String, String> parameters = new Dictionary<string, string>();

             Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
             if (!isUserSessionValid)
             {
                 response = new GenericJsonError(false, "", "timeout", "");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

             if (!isSessionParamsValid)
             {
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             response = new GenericJsonError(true, "", "", "");

             if (!parameters.ContainsKey("ObservationItem.Id"))
             {
                 response = new GenericJsonError(false, "", "", "DoObservationDelete:UserObj:Err:1");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             String ObservationId = parameters["ObservationItem.Id"];
             Int64 iObservationId = -1;
             Int64.TryParse(ObservationId, out iObservationId);

             if (ObservationId == null || ObservationId.Equals("") || iObservationId < 0)
             {
                 response = new GenericJsonError(false, "", "", "DoObservationDelete:UserObj:Err:2");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             ObservationItem deleteItem = new ObservationItem();
             deleteItem.Id = iObservationId;
             String[] result = deleteItem.Delete();
             if (result[0].Equals("false"))
             {
                 response = new GenericJsonError(false, "", result[1], result[2]);
                 return Json(response, JsonRequestBehavior.AllowGet);
             }
             else
             {
                 return Json(response, JsonRequestBehavior.AllowGet);
             }
         }


         [CustomAuthorize]
         public ActionResult DepsettingsList()
         {
             String lang = CommonConfiguration.SiteLanguage();
             SessionParamsUtil.ClearParameterTable();

             PageDefinitonData pData = new PageDefinitonData("DepsettingsList", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.SaveBreadCrumbData(Session);

             DepsettingItems depsettingItems = new DepsettingItems();
             String[] opResult = depsettingItems.Load();

             List<String> TableHeaders = new List<String>();
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_START_SHIFT_TIME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_END_SHIFT_TIME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_START_BREAK_TIME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_END_BREAK_TIME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             TableHeaders.Add(StringResources.getString(StringResources.RES_LABEL, "LBL_PROCESS", lang));
             ViewBag.TableHeaders = TableHeaders;

             ViewBag.AddNew = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPSETTINGS_ADD_NEW", lang);
             ViewBag.EditTip = StringResources.getString(StringResources.RES_LABEL, "LBL_EDIT", lang);
             ViewBag.DeleteTip = StringResources.getString(StringResources.RES_LABEL, "LBL_DELETE", lang);

             ViewBag.DeleteConfirmMsg = StringResources.getString(StringResources.RES_MSG, "CONFIRM_DEPSETTING_DELETE", lang);
             ViewBag.HasError = "0";

             if (opResult[0].Equals("true"))
             {
                 ViewBag.TableData = depsettingItems.DepsettingItemsList;
             }
             else
             {
                 String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPSETTINGS_LIST", lang);
                 String modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), opResult[2]);
                 String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "dismiss",
                     "", "", "");

                 Session["ModalData"] = err;

                 ViewBag.HasError = "1";
                 ViewBag.TableData = new List<DepsettingItem>();
             }

             return View();
         }


         [CustomAuthorize]
         public ActionResult DepsettingsAdd()
         {
             String lang = CommonConfiguration.SiteLanguage();

             PageDefinitonData pData = new PageDefinitonData("DepsettingsAdd", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.AddBreadCrumbItem("DepsettingsList", true);
             pData.SaveBreadCrumbData(Session);

             Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
             CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dePartmentsDict);


             Dictionary<String, String> FormLabels = new Dictionary<String, String>();
             FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             FormLabels.Add("MinObserNumber", StringResources.getString(StringResources.RES_LABEL, "LBL_MIN_OBSER_NUMBER", lang));
             FormLabels.Add("StartShiftTime", StringResources.getString(StringResources.RES_LABEL, "LBL_START_SHIFT_TIME", lang));
             FormLabels.Add("EndShiftTime", StringResources.getString(StringResources.RES_LABEL, "LBL_END_SHIFT_TIME", lang));
             FormLabels.Add("StartBreakTime", StringResources.getString(StringResources.RES_LABEL, "LBL_START_BREAK_TIME", lang));
             FormLabels.Add("EndBreakTime", StringResources.getString(StringResources.RES_LABEL, "LBL_END_BREAK_TIME", lang));
            
             ViewBag.FormLabels = FormLabels;

             ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
             ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
             ViewBag.DepartmentsDict = dePartmentsDict;

             return View();
         }


         [NoCache]
         public JsonResult DoDepsettingsAdd(List<FormDataNameValue> formdatapair)
         {
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPSETTINGS_ADD_NEW", lang);
             String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             if (formdatapair == null)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoDepsettingsAdd:SP:Error:1");
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoDepsettingsAdd:SP:Error:1");

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "window.location='/data/depsetting'",
                     "", "", "");

                 Session["ModalData"] = err;

                 return Json(response);
             }

             try
             {
                 DepsettingItem newItem = new DepsettingItem(formdatapair);
                 String[] opResult = newItem.Create();
                 if (opResult[0].Equals("true"))
                 {
                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "S", modalBody, modalClose,
                         "default", "window.location.href='/data/depsetting'",
                         "", "", "");

                     Session["ModalData"] = err;

                     return Json(response);
                 }
                 else if (opResult[0].Equals("false"))
                 {
                     String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                     modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
                 else
                 {
                     String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
             }
             catch (Exception ex)
             {
                 String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", errCode, ex.Message);
                 return Json(response);
             }

         }

         [CustomAuthorize]
         public ActionResult DepsettingsEdit(String refcode)
         {
             String lang = CommonConfiguration.SiteLanguage();

             PageDefinitonData pData = new PageDefinitonData("DepsettingsEdit", false);
             pData.AddBreadCrumbItem("DataMng", false);
             pData.AddBreadCrumbItem("DepsettingsList", true);
             pData.SaveBreadCrumbData(Session);

             Dictionary<String, String> FormLabels = new Dictionary<String, String>();
             FormLabels.Add("DepsettingId", StringResources.getString(StringResources.RES_LABEL, "LBL_ID", lang));
             FormLabels.Add("DepartmentId", StringResources.getString(StringResources.RES_LABEL, "LBL_DEPARTMENT_NAME", lang));
             FormLabels.Add("MinObserNumber", StringResources.getString(StringResources.RES_LABEL, "LBL_MIN_OBSER_NUMBER", lang));
             FormLabels.Add("StartShiftTime", StringResources.getString(StringResources.RES_LABEL, "LBL_START_SHIFT_TIME", lang));
             FormLabels.Add("EndShiftTime", StringResources.getString(StringResources.RES_LABEL, "LBL_END_SHIFT_TIME", lang));
             FormLabels.Add("StartBreakTime", StringResources.getString(StringResources.RES_LABEL, "LBL_START_BREAK_TIME", lang));
             FormLabels.Add("EndBreakTime", StringResources.getString(StringResources.RES_LABEL, "LBL_END_BREAK_TIME", lang));
             ViewBag.FormLabels = FormLabels;

             ViewBag.SubmitBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_SAVE", lang);
             ViewBag.CancelBtn = StringResources.getString(StringResources.RES_LABEL, "LBL_CANCEL", lang);
             ViewBag.LoadingMsg = StringResources.getString(StringResources.RES_MSG, "FORM_DATA_LOADING", lang);
             ViewBag.Refcode = refcode;
             return View();
         }

         [NoCache]
         public JsonResult DoGetDepsettingsEdit(String refcode)
         {
             DepsettingItem editItem = new DepsettingItem();
             String DepsettingId = "";
             String DepartmentId = "";
             String lang = CommonConfiguration.SiteLanguage();
             String backLink = "window.location='/data/depsetting'";

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPSETTINGS_EDIT", lang);
             String modalBody = "";
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             GenericJsonError response = GetDepsettingIdBySessionRefCode(refcode, out DepsettingId, out DepartmentId);

             if (response.result == true)
             {
                 Int64 iDepsettingId = -1;
                 Int64.TryParse(DepsettingId, out iDepsettingId);

                 String[] opResult = editItem.LoadByKey(iDepsettingId);
                 if (opResult[0].Equals("true"))
                 {
                     Dictionary<String, String> dePartmentsDict = new Dictionary<string, string>();
                     CachedConversions.GetCachedKeyValues(System.Web.HttpContext.Current, CachedConversions.CACHED_DICT_DEPARTMENTS, out dePartmentsDict);



                     List<EditFormSelectItem> dePartmentsSelectItems = EditFormDataItem.GenerateListFromDictionary(dePartmentsDict);


                     List<EditFormDataItem> formItems = new List<EditFormDataItem>();
                     formItems.Add(new EditFormDataItem("DepsettingId", "text", editItem.Id.ToString(), true));
                     formItems.Add(new EditFormDataItem("DepartmentId", "select", editItem.DepartmentId.ToString(), dePartmentsSelectItems, false));
                     formItems.Add(new EditFormDataItem("MinObserNumber", "text", editItem.MinObserNumber.ToString(), false));
                     formItems.Add(new EditFormDataItem("StartShiftTime", "text", editItem.StartShiftTime.ToString(), false));
                     formItems.Add(new EditFormDataItem("EndShiftTime", "text", editItem.EndShiftTime.ToString(), false));
                     formItems.Add(new EditFormDataItem("StartBreakTime", "text", editItem.StartBreakTime.ToString(), false));
                     formItems.Add(new EditFormDataItem("EndBreakTime", "text", editItem.EndBreakTime.ToString(), false));


                     response.data1 = JsonConvert.SerializeObject(formItems);
                     return Json(response, JsonRequestBehavior.AllowGet);
                 }
                 else if (opResult[0].Equals("false"))
                 {
                     modalBody = opResult[2];
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:OP:Error:" + opResult[1]);
                 }
                 else
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_DB_RECORD_NOT_FOUND", lang), "DepsettingItem");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:5");
                 }

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", backLink,
                     "", "", "");

                 Session["ModalData"] = err;
             }

             return Json(response, JsonRequestBehavior.AllowGet);

         }

         private GenericJsonError GetDepsettingIdBySessionRefCode(String refcode, out String depsettingId, out String dePartmentId)
         {
             Dictionary<String, String> parameters = new Dictionary<string, string>();
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();
             String backLink = "window.location='/data/depsetting'";
             depsettingId = "";
             dePartmentId = "";

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPSETTINGS_EDIT", lang);
             String modalBody = "";
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             Boolean hasError = false;
             Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

             if (!hasError && !isSessionParamsValid)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoGetDepsettingsEdit:SP:Error:1");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoGetDepsettingsEdit:SP:Error:1");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("DepsettingItem.Id"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepsettingItem.Id");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("DepsettingItem.MinObserNumber"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepsettingItem.MinObserNumber");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:2");
                 hasError = true;
             }
             if (!hasError && !parameters.ContainsKey("DepsettingItem.StartShiftTime"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepsettingItem.StartShiftTime");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("DepsettingItem.EndShiftTime"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepsettingItem.EndShiftTime");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("DepsettingItem.StartBreakTime"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepsettingItem.StartBreakTime");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("DepsettingItem.EndBreakTime"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepsettingItem.EndBreakTime");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError && !parameters.ContainsKey("DepsettingItem.DepartmentId"))
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepsettingItem.DepartmentId");
                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:2");
                 hasError = true;
             }

             if (!hasError)
             {
                 depsettingId = parameters["DepsettingItem.Id"];

                 if (depsettingId == null || depsettingId.Equals(""))
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepsettingItem.Id");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:3");
                     hasError = true;
                 }

                 dePartmentId = parameters["DepsettingItem.DepartmentId"];

                 if (dePartmentId == null || dePartmentId.Equals(""))
                 {
                     modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_REQUIRED_ERR", lang), "DepsettingItem.dePartmentId");
                     response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_OPERATION_FAILED, "DoGetDepsettingsEdit:SP:Error:3");
                     hasError = true;
                 }
             }

             if (hasError)
             {
                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", backLink,
                     "", "", "");

                 Session["ModalData"] = err;

                 return response;
             }

             return new GenericJsonError(true, "", "", "");
         }

         [NoCache]
         public JsonResult DoDepsettingsEdit(List<FormDataNameValue> formdatapair)
         {
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             String lang = CommonConfiguration.SiteLanguage();

             String modalHeader = StringResources.getString(StringResources.RES_LABEL, "LBL_DEPSETTINGS_EDIT", lang);
             String modalBody = StringResources.getString(StringResources.RES_MSG, "GENERIC_OPERATION_SUCCESS", lang);
             String modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_OK", lang);

             if (formdatapair == null)
             {
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_FORM_DATA_NOT_AVAILABLE", lang), "DoDepsettingsEdit:SP:Error:1");
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", CommonConfiguration.ERR_CODE_FORM_DATA_NOT_AVAILABLE, "DoDepsettingsEdit:SP:Error:1");

                 ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                     "F", modalBody, modalClose,
                     "default", "dismiss",
                     "", "", "");

                 Session["ModalData"] = err;

                 return Json(response);
             }

             try
             {
                 DepsettingItem formItem = new DepsettingItem(formdatapair);
                 DepsettingItem updateItem = new DepsettingItem();
                 String DepsettingId = "";
                 String DepartmentId = "";
                 response = GetDepsettingIdBySessionRefCode(formItem.SessionParam, out DepsettingId, out DepartmentId);

                 if (response.result == false)
                 {
                     String errCode = response.errcode != null && !response.errcode.Equals("") ? response.errcode : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                     modalBody = response.errmsg != null && !response.errmsg.Equals("") ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), response.errmsg) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                     modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                     ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                         "F", modalBody, modalClose,
                         "default", "dismiss",
                         "", "", "");

                     Session["ModalData"] = err;

                     response = new GenericJsonError(false, "", errCode, "");
                     return Json(response);
                 }
                 else
                 {
                     String[] opResult = updateItem.Update(DepsettingId, DepartmentId, formItem);

                     if (opResult[0].Equals("true"))
                     {
                         ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                             "S", modalBody, modalClose,
                             "default", "window.location.href='/data/depsetting'",
                             "", "", "");

                         Session["ModalData"] = err;

                         return Json(response);
                     }
                     else if (opResult[0].Equals("false"))
                     {
                         String errCode = opResult[1] != null ? opResult[1] : CommonConfiguration.ERR_CODE_OPERATION_FAILED;
                         modalBody = opResult[2] != null ? String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED", lang), opResult[2]) : String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED);
                         modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                         ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                             "F", modalBody, modalClose,
                             "default", "dismiss",
                             "", "", "");

                         Session["ModalData"] = err;

                         response = new GenericJsonError(false, "", errCode, "");
                         return Json(response);
                     }
                     else
                     {
                         String errCode = CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC;
                         modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_FAILED_NO_ERR_DESC", lang), CommonConfiguration.ERR_CODE_OPERATION_FAILED_NO_ERR_DESC);
                         modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                         ErrorModalData err = new ErrorModalData("infoModal", modalHeader,
                             "F", modalBody, modalClose,
                             "default", "dismiss",
                             "", "", "");

                         Session["ModalData"] = err;

                         response = new GenericJsonError(false, "", errCode, "");
                         return Json(response);
                     }
                 }
             }
             catch (Exception ex)
             {
                 String errCode = CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION;
                 modalBody = String.Format(StringResources.getString(StringResources.RES_ERR, "GENERIC_OPERATION_EXCEPTION", lang), CommonConfiguration.ERR_CODE_OPERATION_EXCEPTION);
                 modalClose = StringResources.getString(StringResources.RES_LABEL, "LBL_CLOSE", lang);

                 response = new GenericJsonError(false, "", errCode, ex.Message);
                 return Json(response);
             }
         }

         [NoCache]
         public JsonResult DoDepsettingsDelete(String refcode)
         {
             String lang = CommonConfiguration.SiteLanguage();
             GenericJsonError response = new GenericJsonError(true, "", "", "");
             Dictionary<String, String> parameters = new Dictionary<string, string>();

             Boolean isUserSessionValid = SessionParamsUtil.HasValidSession();
             if (!isUserSessionValid)
             {
                 response = new GenericJsonError(false, "", "timeout", "");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             Boolean isSessionParamsValid = SessionParamsUtil.ValidateSessionParam(refcode, out response, out parameters);

             if (!isSessionParamsValid)
             {
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             response = new GenericJsonError(true, "", "", "");

             if (!parameters.ContainsKey("DepsettingItem.Id"))
             {
                 response = new GenericJsonError(false, "", "", "DoDepsettingsDelete:UserObj:Err:1");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             String DepsettingId = parameters["DepsettingItem.Id"];
             Int64 iDepsettingId = -1;
             Int64.TryParse(DepsettingId, out iDepsettingId);

             if (DepsettingId == null || DepsettingId.Equals("") || iDepsettingId < 0)
             {
                 response = new GenericJsonError(false, "", "", "DoDepsettingsDelete:UserObj:Err:2");
                 return Json(response, JsonRequestBehavior.AllowGet);
             }

             DepsettingItem deleteItem = new DepsettingItem();
             deleteItem.Id = iDepsettingId;
             String[] result = deleteItem.Delete();
             if (result[0].Equals("false"))
             {
                 response = new GenericJsonError(false, "", result[1], result[2]);
                 return Json(response, JsonRequestBehavior.AllowGet);
             }
             else
             {
                 return Json(response, JsonRequestBehavior.AllowGet);
             }
         }

    }
}
