﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using _2DMap.Utility;
using _2DMap.Models;

namespace _2DMap.Common
{
    public class HomeController : Controller
    {
        [CustomAuthorize]
        public ActionResult Index()
        {
            PageDefinitonData pData = new PageDefinitonData("TestPage2",true);
            pData.AddBreadCrumbItem("TestPage2", true);
            pData.AddBreadCrumbItem("TestPage1", true);
            pData.SaveBreadCrumbData(Session);

            return View();
        }
    }
}
