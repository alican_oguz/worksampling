﻿$(document).ready(function () {
    var dataId = $("#skiCenterItem").find(':first-child').attr("data-id");
    GetItemsById(dataId);
});
function ElementEnable() {
    $('.form-loading').hide();
    $('input[type="checkbox"]').each(function () {
        $(this).prop("disabled", false);
    });
}

function ElementDisable() {
    $('input[type="checkbox"]').each(function () {
        $(this).prop("disabled", true);
    });
}
function DoJob(count) {
    for (var i = 0; i < count; i++) {

       $("#check" + i).change(function () {
            ElementDisable();
            var dataType = $(this).attr("data-type");
            var dataId = $(this).attr("data-id");
            var dataSId = $(this).attr("data-sid");
            var dataStatus = 0;
            if ($(this).is(":checked")) {
                dataStatus = 1;
            }
            $.ajax({
                method: "POST",
                url: "../DataMng/DoUpdateSkiCenterItem",
                data: { dataType: dataType, dataId: dataId, dataSId: dataSId, dataStatus: dataStatus },

                success: function (data) {
                    var jresult = data;
                    if (jresult.hasOwnProperty('result') && jresult.result == true) {
                        ElementEnable();
                    }
                    else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                        showInfoModal("infoModal");
                        ElementEnable();
                    }
                    else {
                        console.log(data.d);
                        ElementEnable();
                        showDefaultErrorModal('infoModal', '1', 'UpdateStatus;Do;Success;1');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    showDefaultErrorModal('infoModal', '1', 'UpdateStatus;Do;Error;1');
                    ElementEnable();
                }
            });

        });
    }
}

function GetItemsById(id) {
    
    $('#skiCenterItem').find('i').removeClass('fa fa-check-circle');
    $('#item' + id).find('i').addClass('fa fa-check-circle');

    var title = $('#item' + id).find('a').text();
    $('.box-tools').text(title);
    $('.box-tools').css("top", "10px");

    $.ajax({
        method: "POST",
        url: "../DataMng/GetItemsBySkiCenterId",
        data: { id: id },
        beforeSend: function () {
            $('#slopeItem').empty();
            $('#liftItem').empty();
            $('#funItem').empty();
            $('#gastItem').empty();
            $('#infItem').empty();
            $('.form-loading').show();
        }
    }).done(function (msg) {

        var data = jQuery.parseJSON(msg.data1);
        var count = 0;
        var countSlope = 0;
        var countLift = 0;
        var countFun = 0;
        var countGast = 0;
        var countInf = 0;

        for (var key in data) {
            if (data.hasOwnProperty(key)) {
                var obj = eval('(' + data[key] + ')');
                if (key.indexOf("slope") >= 0) {
                    FillScreenFromData(obj, count, "#slopeItem", "slope");
                    count++;
                    countSlope++;
                }
                else if (key.indexOf("lift") >= 0) {
                    FillScreenFromData(obj, count, "#liftItem", "lift");
                    count++;
                    countLift++;
                }
                else if (key.indexOf("gast") >= 0) {
                    FillScreenFromData(obj, count, "#gastItem", "gast");
                    count++;
                    countGast++;
                }
                else if (key.indexOf("fun") >= 0) {
                    FillScreenFromData(obj, count, "#funItem", "fun");
                    count++;
                    countFun++;
                }
                else if (key.indexOf("inf") >= 0) {
                    FillScreenFromData(obj, count, "#infItem", "inf");
                    count++;
                    countInf++;
                }
            }
        }

        if (countSlope == 0) {
            FillEmptyScreen("#slopeItem");
        }
        if (countLift == 0) {
            FillEmptyScreen("#liftItem");
        }
        if (countFun == 0) {
            FillEmptyScreen("#funItem");
        }
        if (countGast == 0) {
            FillEmptyScreen("#gastItem");
        }
        if (countInf == 0) {
            FillEmptyScreen("#infItem");
        }

        ElementEnable();
        DoJob(count);
    });
}

function FillScreenFromData(item, count, id, type) {
    var checked = "";
    if (item.Status == 1)
        checked = "checked";
    $(id).append("<li><div style=\"float:left;width:70%;\">" + item.Name + "</div><div style=\"float:left;width:30%;\"><label><input type=\"checkbox\" id=\"check" + count + "\" name=\"check" + count + "\" " + checked + "></label></div></li>");
    $(id).find("li").addClass("tab-item-element");
    $('#check' + count).attr("data-sid", item.SkiCenterId);
    $('#check' + count).attr("data-id", item.Id);
    $('#check' + count).attr("data-type", type);
}

function FillEmptyScreen(id) {
    $(id).append("<li><div> Listelenecek öğe bulunamadı. </div></li>");
    $(id).find("li").addClass("tab-item-element");
    $(id).find("div").addClass("item-text");
}