﻿$(document).ready(function () {
        $('#FormCheckPassword').submit(function (e){
            e.preventDefault();
           }).validate({
            rules: { ComfirmPassword: { equalTo: "#NewPassword"} },
            ignore: ":hidden:not(.multiselect)",
            highlight: function (element, errorClass) {
            $(element).parent().parent().addClass("has-error");
                if (!$(element).hasClass('multiselect'))
                $(element).parent().append($('<span>').addClass('glyphicon glyphicon-remove form-control-feedback'));
                                                        },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).parent().parent().removeClass("has-error");
                    if (!$(element).hasClass('multiselect'))
                        $(element).parent().find("span").remove()
                    },
                    errorClass: "text-danger",
                    errorElement: "span",
                    onkeyup: function (element, event) {
                    this.element(element);
                                                        },
                    onfocusout: function () {
                                            },
                    errorPlacement: function (error, element) {
                    error.insertAfter($(element).parent());},
                    submitHandler: function (form) {
                    var PswCurrent = $('#CurrentPassword').val();
                    var PswComfirm = $('#ComfirmPassword').val();
                    var UsrName=$('#UserName').val();
                    $.ajax({
                        type: "POST",
                        url: "/Utility/DoChangePassword",
                        content: "application/html; charset=utf-8",
                        data: { PswCur: PswCurrent, PswComf: PswComfirm, UserName: UsrName }, 
                        success: function (data) {
                            if (data.result === false) { 
                                $('#Msg').html(data.errcode);
                            }
                            else {
                                   $("#Cancel").text("Kapat");
                                   $("#Msg").html("Şifreniz Değiştirildi");
                                   $("#PasswordModalSave").hide();
                            }
                        },
                           error: function (xhr, ajaxOptions, thrownError) {
                               console.log(xhr);
                           }
                    });
                }
            });
    });
