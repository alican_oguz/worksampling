﻿$(document).ready(function () {
    var buttonsArr = ['UserAddFormSubmit'];

    $('#UserAddForm').submit(function (e) {
        e.preventDefault();
    })
    .validate({
        rules: { PasswordRe: { equalTo: "#Password"} },
        ignore: ":hidden:not(.multiselect)",
        highlight: function (element, errorClass) {
            $(element).parent().parent().addClass("has-error");
            if (!$(element).hasClass('multiselect'))
                $(element).parent().append($('<span>').addClass('glyphicon glyphicon-remove form-control-feedback'));
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().parent().removeClass("has-error");
            if (!$(element).hasClass('multiselect'))
                $(element).parent().find("span").remove()
        },
        errorClass: "text-danger",
        errorElement: "span",
        onkeyup: function (element, event) {
            this.element(element);
        },
        onfocusout: function () {
        },
        errorPlacement: function (error, element) {
            error.insertAfter($(element).parent());
        },
        submitHandler: function (form) {
            $.ajax({
                type: 'GET',
                url: '/Login/HasValidSession',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    AnyFormLoading(buttonsArr);
                },
                success: function (data) {
                    var jresult = data;
                    if (jresult.hasOwnProperty('result') && jresult.result == true) {
                        $.ajax({
                            type: 'POST',
                            url: '/UserMng/DoUserAdd',
                            data: JSON.stringify({ 'formdatapair': $(form).serializeArray() }),
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            success: function (data) {
                                var jresult = data;
                                if (jresult.hasOwnProperty('result') && jresult.result == true) {
                                    showInfoModal("infoModal");
                                }
                                else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                                    AnyFormRemoveLoading(buttonsArr);
                                    showInfoModal("infoModal");
                                }
                                else {
                                    console.log(data.d);
                                    AnyFormRemoveLoading(buttonsArr);
                                    showDefaultErrorModal('infoModal', '1', 'UserAddForm;Do;Success;1');
                                }
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                console.log(xhr);
                                AnyFormRemoveLoading(buttonsArr);
                                showDefaultErrorModal('infoModal', '1', 'UserAddForm;Do;Error;1');
                            }
                        });
                    }
                    else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                        AnyFormRemoveLoading(buttonsArr);
                        showInfoModal('infoModal');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    AnyFormRemoveLoading(buttonsArr);
                    showDefaultErrorModal('infoModal', '1', 'UserAddForm;ValidSession;Error;1');
                }
            });

            return false;
        }
    });
});
