$(document).ready(function () {
    $("form input").keypress(function (e) {
        if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
            $('#changePwdForm').submit()
            return false;
        } else {
            return true;
        }
    });

    $("input[type=text]:first").focus();

    $('#changePwdForm').submit(function (e) {
        e.preventDefault();
    })
    .validate({
        rules: {},
        lang: 'tr',
        errorClass: "err_in",
        errorElement: "span",
        onfocusout: function () {
        },
        errorPlacement: function (error, element) {
        },
        highlight: function (element, errorClass) {
            $(element).parent().addClass("has-error");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parent().removeClass("has-error");
        },
        submitHandler: function (form) {
            $.ajax({
                url: 'Login/DoResendPwd',
                type: 'POST',
                dataType: 'html',
                data: $('form').serialize(),
                beforeSend: function () {
                    $('#pwdResendSubmitBtn').button('loading');
                },
                success: function (data) {

                    var jresult = $.parseJSON(data);
                    if (jresult.hasOwnProperty('result') && jresult.result == true) {
                        showInfoModal('infoModal');
                    }
                    else if (jresult.hasOwnProperty('result') && jresult.result == false) {
                        if (jresult.errcode == '') {
                            showInfoModal('infoModal');
                        }
                        else {
                            showDefaultErrorModal('infoModal', '1', jresult.errcode);
                        }

                        $('form').find("input[type=text]").val("");
                        $('#pwdResendSubmitBtn').button('reset');
                    }
                    else {
                        console.log("JSON ERR:" + data.d);
                        showDefaultErrorModal('infoModal', '1', 'ResendPwd;Validate;Success;2');
                        $('form').find("input[type=text]").val("");
                        $('#pwdResendSubmitBtn').button('reset');
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    console.log(xhr);
                    showDefaultErrorModal('infoModal', '1', 'ResendPwd;Validate;Error;3');
                    $('form').find("input[type=text]").val("");
                    $('#pwdResendSubmitBtn').button('reset');
                }
            });
            return false;
        }
    });
})